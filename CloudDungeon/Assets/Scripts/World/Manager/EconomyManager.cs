﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using HeroEditor.Common;
using HeroEditor.Common.Enums;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Assets.HeroEditor.Common.CharacterScripts;


public class UnitGoods
{
    public int _Num;
    public string _Name;
    public string _IMGName; //스프라이트 이름이다.  
    public int _Group; // 어떤 속성의 물품인지. 각각의 넘버들로 의미지어진다. 
    public int _Grade;  //몇등급의 물품인지. 고급 등급일수록 나중에 나온다. 
    
    public int _Price;  //기준으로 쓰이는 가격    
    public float _Weight; //무게.

    public SpriteGroupEntry _GoodsSpriteGroupEntry; //구즈의 스프라이트그룹엔트리
    public SpriteGroupEntry _SubGoodsSpriteGroupEntry; //트레일이나 헬멧 마스크 같은 서브 엔트리.

    public int _EquipmentStat;  //이 장비가 가져가는 수치 값

    public enum EquipmentType
    {
        CanNotUse, //-

        MeleeWeapon1H,  //0
        MeleeWeapon2H,
        Bow,

        Shield,  //1

        Armor,  //2


        Helmet,  //3

        Mask,  //4

        Glasses,  //5

        Cape,   //6

        Back,  //7
        
    }

    public EquipmentType _EquipmentType; //아이템 타입

    public UnitGoods() { }
    public UnitGoods(int _Num, int _Group, int _Grade, string _Name, string _IMGName, int _Price, float _Weight, int _EquipmentStat )
    {
        this._Num = _Num;
        this._Name = _Name;
        this._IMGName = _IMGName;
        this._Group = _Group;
        this._Grade = _Grade;
        this._Price = _Price;
        this._Weight = _Weight;
        this._EquipmentStat = _EquipmentStat;

    }
}

public class EconomyManager : MonoBehaviour
{
    
    //외부 참조
    WorldManager _WorldManager;
    
    //시스템

    //속성
    public List<UnitGoods> _ProtoGoodsList;  // 게임에 존재할 수 있는 모든 상품 데이터를 가지고 있는 리스트. 
    public int _totalGoodsGroupCount;
    public List<float> _averagePriceRateList;  // 게임에 존재하는 모든 물품들의 평균 가격 리스트

    public float _BuyPriceDiscountAdjustRate;  //구입시 시세 조정율

    void Start()
    {
        _WorldManager = GetComponent<WorldManager>();
        InputProtoGoodsList();
        StartCoroutine(Init());
        _BuyPriceDiscountAdjustRate = 0.15f;  //구입시 시세 조정율

    }
    

    IEnumerator Init()
    {
        yield return new WaitForSeconds(0.3f);

        ShareProtoGoodsToTowns();

        StartCoroutine(ControlGeneratingGoods());
    }

    void InputProtoGoodsList()
    {
        _ProtoGoodsList = new List<UnitGoods>();
        _ProtoGoodsList.Add(new UnitGoods(0, 0, 1, "어쌔신 대거", "AssassinDagger [Paint]", 330, 25, 2));
        _ProtoGoodsList.Add(new UnitGoods(1, 0, 1, "어쌔신 후드", "AssassinHood [Paint]", 440, 24, 2));
        _ProtoGoodsList.Add(new UnitGoods(2, 0, 1, "밸런스드 스워드", "BalancedSword [Paint]", 60, 5, 1));
        _ProtoGoodsList.Add(new UnitGoods(3, 0, 1, "밴드 마스크", "BanditMask [Paint]", 380, 20, 2));
        _ProtoGoodsList.Add(new UnitGoods(4, 0, 1, "버서커 액스", "BerserkAxe", 400, 28, 2));
        _ProtoGoodsList.Add(new UnitGoods(5, 0, 2, "버서커 헬름", "BerserkHelm", 720, 38, 5));
        _ProtoGoodsList.Add(new UnitGoods(6, 0, 2, "비숍 햇", "BishopHat [Paint]", 380, 34, 5));
        _ProtoGoodsList.Add(new UnitGoods(7, 0, 2, "비숏 완드", "BishopWand", 730, 46, 6));
        _ProtoGoodsList.Add(new UnitGoods(8, 0, 3, "브로드 소드", "BroadSword", 1640, 90, 16));
        _ProtoGoodsList.Add(new UnitGoods(9, 0, 3, "카디날 북", "CardinalBook", 2800, 57, 11));
        _ProtoGoodsList.Add(new UnitGoods(10, 0, 4, "카디날 햇", "CardinalHat [Paint]", 2480, 84, 20));
        _ProtoGoodsList.Add(new UnitGoods(11, 1, 1, "카디날 완드", "CardinalWand", 130, 20, 2));
        _ProtoGoodsList.Add(new UnitGoods(12, 1, 1, "카니발 마스크", "CarnivalMask", 320, 17, 1));
        _ProtoGoodsList.Add(new UnitGoods(13, 1, 1, "카타 프랙 헬름", "CataphractHelm [Paint]", 50, 7, 1));
        _ProtoGoodsList.Add(new UnitGoods(14, 1, 1, "카타프랙 스피어", "CataphractSpear", 510, 27, 2));
        _ProtoGoodsList.Add(new UnitGoods(15, 1, 2, "챔피언 헬름", "ChampionHelm", 420, 42, 6));
        _ProtoGoodsList.Add(new UnitGoods(16, 1, 2, "클러릭 완드", "ClericWand1", 360, 32, 5));
        _ProtoGoodsList.Add(new UnitGoods(17, 1, 2, "특이한 클러릭 완드", "ClericWand2", 1420, 48, 6));
        _ProtoGoodsList.Add(new UnitGoods(18, 1, 3, "크루세이더 쉴드", "CrusaderShield", 740, 21, 6));
        _ProtoGoodsList.Add(new UnitGoods(19, 1, 3, "다크 마운틴", "DarkMountain [Paint]", 820, 24, 6));
        _ProtoGoodsList.Add(new UnitGoods(20, 1, 4, "디저트 밴디지", "DeserterBandage [Paint]", 2000, 76, 19));
        _ProtoGoodsList.Add(new UnitGoods(21, 2, 1, "디저트 대거", "DeserterDagger [Paint]", 310, 29, 2));
        _ProtoGoodsList.Add(new UnitGoods(22, 2, 1, "디스트로이어 액스", "DestroyerAxe", 410, 23, 2));
        _ProtoGoodsList.Add(new UnitGoods(23, 2, 1, "디스트로이어 헬름", "DestroyerHelm [Paint]", 50, 11, 1));
        _ProtoGoodsList.Add(new UnitGoods(24, 2, 1, "드루이드 햇", "DruidHat", 240, 15, 1));
        _ProtoGoodsList.Add(new UnitGoods(25, 2, 2, "드루이드 완드", "DruidWand", 1470, 42, 6));
        _ProtoGoodsList.Add(new UnitGoods(26, 2, 2, "엘더 스태프", "ElderStaff", 600, 28, 4));
        _ProtoGoodsList.Add(new UnitGoods(27, 2, 2, "엘리트가드 스워드", "EliteGuardSword [Paint]", 1530, 58, 7));
        _ProtoGoodsList.Add(new UnitGoods(28, 2, 3, "엘리트나이트 스워드", "EliteKnightSword [Paint]", 1980, 66, 12));
        _ProtoGoodsList.Add(new UnitGoods(29, 2, 3, "패밀리스워드", "FamilySword", 2260, 45, 9));
        _ProtoGoodsList.Add(new UnitGoods(30, 2, 4, "파이어완드", "FireAdeptWand1", 2500, 36, 11));
        _ProtoGoodsList.Add(new UnitGoods(31, 3, 1, "특이한 파이어완드", "FireAdeptWand2", 140, 15, 1));
        _ProtoGoodsList.Add(new UnitGoods(32, 3, 1, "파이어나이트 스워드", "FireKnightSword [Paint]", 110, 7, 1));
        _ProtoGoodsList.Add(new UnitGoods(33, 3, 1, "파이어워리어 쉴드", "FireWarriorShield", 150, 28, 2));
        _ProtoGoodsList.Add(new UnitGoods(34, 3, 1, "파이어워리어 스워드", "FireWarriorSword1", 120, 14, 1));
        _ProtoGoodsList.Add(new UnitGoods(35, 3, 2, "강화된 파이어워리어 스워드", "FireWarriorSword2", 1450, 56, 7));
        _ProtoGoodsList.Add(new UnitGoods(36, 3, 2, "파이어위저드 후드", "FireWizardHood", 270, 20, 4));
        _ProtoGoodsList.Add(new UnitGoods(37, 3, 2, "파이어위저드 완드", "FireWizardWand", 780, 34, 5));
        _ProtoGoodsList.Add(new UnitGoods(38, 3, 3, "프리포크 액스", "FreeFolkAxe", 1720, 51, 10));
        _ProtoGoodsList.Add(new UnitGoods(39, 3, 3, "글래디에이터 헬름", "GladiatorHelm1", 950, 57, 11));
        _ProtoGoodsList.Add(new UnitGoods(40, 3, 4, "강화된 글래디에이터 헬름", "GladiatorHelm2", 7650, 104, 24));
        _ProtoGoodsList.Add(new UnitGoods(41, 4, 1, "글래디에이터 쉴드", "GladiatorShield", 230, 19, 1));
        _ProtoGoodsList.Add(new UnitGoods(42, 4, 1, "가드헬름", "GuardHelm1 [Paint]", 120, 8, 1));
        _ProtoGoodsList.Add(new UnitGoods(43, 4, 1, "가드헬름", "GuardHelm2 [Paint]", 210, 14, 1));
        _ProtoGoodsList.Add(new UnitGoods(44, 4, 1, "가드헬름", "GuardHelm3 [Paint]", 120, 13, 1));
        _ProtoGoodsList.Add(new UnitGoods(45, 4, 2, "가드쉴드", "GuardShield1", 290, 10, 3));
        _ProtoGoodsList.Add(new UnitGoods(46, 4, 2, "가드쉴드", "GuardShield2", 990, 58, 7));
        _ProtoGoodsList.Add(new UnitGoods(47, 4, 2, "가드스워드", "GuardSword1 [Paint]", 960, 26, 4));
        _ProtoGoodsList.Add(new UnitGoods(48, 4, 3, "가드스워드", "GuardSword2 [Paint]", 4420, 81, 15));
        _ProtoGoodsList.Add(new UnitGoods(49, 4, 3, "할버드", "Halberd1", 3100, 75, 14));
        _ProtoGoodsList.Add(new UnitGoods(50, 5, 4, "할버드", "Halberd2", 3450, 92, 22));
        _ProtoGoodsList.Add(new UnitGoods(51, 5, 1, "할버디어 헬름", "HalberdierHelm [Paint]", 180, 18, 1));
        _ProtoGoodsList.Add(new UnitGoods(52, 5, 1, "하드우드 완드", "HardwoodWand", 410, 30, 2));
        _ProtoGoodsList.Add(new UnitGoods(53, 5, 1, "헤비 헬름", "HeavyHelm [Paint]", 140, 23, 2));
        _ProtoGoodsList.Add(new UnitGoods(54, 5, 1, "헤비스워드", "HeavySword [Paint]", 340, 19, 1));
        _ProtoGoodsList.Add(new UnitGoods(55, 5, 2, "헤르밋 완드", "Hermit2Wand", 580, 58, 7));
        _ProtoGoodsList.Add(new UnitGoods(56, 5, 2, "헤르밋 후드", "HermitHood [Paint]", 390, 10, 3));
        _ProtoGoodsList.Add(new UnitGoods(57, 5, 2, "헤르밋 램프", "HermitLamp", 440, 28, 4));
        _ProtoGoodsList.Add(new UnitGoods(58, 5, 3, "헤르밋 완드", "HermitWand", 2320, 45, 9));
        _ProtoGoodsList.Add(new UnitGoods(59, 5, 3, "홀스로드 시클", "HorseLordSickle", 1480, 42, 9));
        _ProtoGoodsList.Add(new UnitGoods(60, 6, 4, "헌터 햇", "HunterHat", 4530, 60, 16));
        _ProtoGoodsList.Add(new UnitGoods(61, 6, 1, "헌터 나이프", "HunterKnife [Paint]", 460, 30, 2));
        _ProtoGoodsList.Add(new UnitGoods(62, 6, 1, "아이스위자드 후드", "IceWizardHood", 140, 16, 1));
        _ProtoGoodsList.Add(new UnitGoods(63, 6, 1, "아이스위자드 완드", "IceWizardWand", 130, 13, 1));
        _ProtoGoodsList.Add(new UnitGoods(64, 6, 1, "윈퀴지터햇", "InquisitorHat1 [Paint]", 60, 9, 1));
        _ProtoGoodsList.Add(new UnitGoods(65, 6, 2, "윈퀴지터햇", "InquisitorHat2 [Paint]", 600, 16, 3));
        _ProtoGoodsList.Add(new UnitGoods(66, 6, 2, "인퀴지터 스워드", "InquisitorSword1 [Paint]", 880, 40, 6));
        _ProtoGoodsList.Add(new UnitGoods(67, 6, 2, "인퀴지터 스워드", "InquisitorSword2 [Paint]", 720, 42, 6));
        _ProtoGoodsList.Add(new UnitGoods(68, 6, 3, "인퀴지터 스워드", "InquisitorSword3 [Paint]", 1280, 51, 10));
        _ProtoGoodsList.Add(new UnitGoods(69, 6, 3, "인퀴지터 스워드", "InquisitorSword4 [Paint]", 2270, 51, 10));
        _ProtoGoodsList.Add(new UnitGoods(70, 7, 4, "아이언섬 스워드", "IronIslandsSword [Paint]", 2560, 60, 16));
        _ProtoGoodsList.Add(new UnitGoods(71, 7, 1, "아이언쉴드", "IronShield1", 20, 5, 1));
        _ProtoGoodsList.Add(new UnitGoods(72, 7, 1, "아이언 쉴드", "IronShield2", 210, 28, 2));
        _ProtoGoodsList.Add(new UnitGoods(73, 7, 1, "아이언 쉴드", "IronShield3", 90, 5, 1));
        _ProtoGoodsList.Add(new UnitGoods(74, 7, 1, "카타나", "Katana1 [Paint]", 340, 20, 2));
        _ProtoGoodsList.Add(new UnitGoods(75, 7, 2, "카타나", "Katana2 [Paint]", 440, 42, 6));
        _ProtoGoodsList.Add(new UnitGoods(76, 7, 2, "카타나", "Katana3 [Paint]", 1790, 54, 7));
        _ProtoGoodsList.Add(new UnitGoods(77, 7, 2, "나이트헬름", "KnightHelm [Paint]", 490, 24, 4));
        _ProtoGoodsList.Add(new UnitGoods(78, 7, 3, "나이트쉴드", "KnightShield", 1730, 78, 14));
        _ProtoGoodsList.Add(new UnitGoods(79, 7, 3, "나이트스워드", "KnightSword [Paint]", 790, 39, 8));
        _ProtoGoodsList.Add(new UnitGoods(80, 8, 4, "쿠나이", "Kunai", 3300, 52, 14));
        _ProtoGoodsList.Add(new UnitGoods(81, 8, 1, "랜드네츠헬름", "LandsknechtHelm [Paint]", 110, 16, 1));
        _ProtoGoodsList.Add(new UnitGoods(82, 8, 1, "라이온스워드", "LionSword [Paint]", 40, 6, 1));
        _ProtoGoodsList.Add(new UnitGoods(83, 8, 1, "리틀라이온스워드", "LittleLionSword [Paint]", 250, 19, 1));
        _ProtoGoodsList.Add(new UnitGoods(84, 8, 1, "마체테", "Machete [Paint]", 180, 18, 1));
        _ProtoGoodsList.Add(new UnitGoods(85, 8, 2, "매직스워드", "MagicSword [Paint]", 660, 28, 4));
        _ProtoGoodsList.Add(new UnitGoods(86, 8, 2, "마라우드후드", "MarauderHood", 770, 28, 4));
        _ProtoGoodsList.Add(new UnitGoods(87, 8, 2, "마스크", "Mask1 [Paint]", 290, 10, 3));
        _ProtoGoodsList.Add(new UnitGoods(88, 8, 3, "마스크", "Mask2 [Paint]", 1360, 30, 7));
        _ProtoGoodsList.Add(new UnitGoods(89, 8, 3, "용병의 투구", "MercenaryHelm1 [Paint]", 1030, 48, 10));
        _ProtoGoodsList.Add(new UnitGoods(90, 9, 4, "용병의 투구", "MercenaryHelm2 [Paint]", 3150, 56, 15));
        _ProtoGoodsList.Add(new UnitGoods(91, 9, 1, "용병의 검", "MercenarySword1 [Paint]", 240, 14, 1));
        _ProtoGoodsList.Add(new UnitGoods(92, 9, 1, "용병의 검", "MercenarySword2 [Paint]", 520, 29, 2));
        _ProtoGoodsList.Add(new UnitGoods(93, 9, 1, "민병대 방패", "MilitiamanShield", 40, 6, 1));
        _ProtoGoodsList.Add(new UnitGoods(94, 9, 1, "군인의 숏소드", "MilitiamanShortSword [Paint]", 140, 8, 1));
        _ProtoGoodsList.Add(new UnitGoods(95, 9, 2, "마운틴", "Mountain [Paint]", 1310, 50, 7));
        _ProtoGoodsList.Add(new UnitGoods(96, 9, 2, "마운틴 소드", "MountainSword [Paint]", 490, 36, 5));
        _ProtoGoodsList.Add(new UnitGoods(97, 9, 2, "네크로맨서 후드", "NecromancerHood", 550, 36, 5));
        _ProtoGoodsList.Add(new UnitGoods(98, 9, 3, "닌자토", "Ninjato [Paint]", 630, 21, 6));
        _ProtoGoodsList.Add(new UnitGoods(99, 9, 3, "노스와든 스워드", "NorthWardenSword [Paint]", 1580, 42, 9));
        _ProtoGoodsList.Add(new UnitGoods(100, 10, 4, "올드 폴랙스", "OldPoleaxe", 2430, 52, 14));
        _ProtoGoodsList.Add(new UnitGoods(101, 10, 1, "팔라딘 헬름", "PaladinHelm [Paint]", 220, 28, 2));
        _ProtoGoodsList.Add(new UnitGoods(102, 10, 1, "팔라딘 스워드", "PaladinSword [Paint]", 230, 28, 2));
        _ProtoGoodsList.Add(new UnitGoods(103, 10, 1, "패스 파인더 햇", "PathfinderHat [Paint]", 90, 11, 1));
        _ProtoGoodsList.Add(new UnitGoods(104, 10, 1, "패스 파인더 후드", "PathfinderHood", 230, 13, 1));
        _ProtoGoodsList.Add(new UnitGoods(105, 10, 2, "파이 로맨 서 완드", "PyromancerWand", 880, 32, 5));
        _ProtoGoodsList.Add(new UnitGoods(106, 10, 2, "레인저 해트", "RangerHat [Paint]", 900, 44, 6));
        _ProtoGoodsList.Add(new UnitGoods(107, 10, 2, "로얄 아처 해트트", "RoyalArcherHat", 1140, 56, 7));
        _ProtoGoodsList.Add(new UnitGoods(108, 10, 3, "녹슨 방패", "RustyShield1", 760, 33, 7));
        _ProtoGoodsList.Add(new UnitGoods(109, 10, 3, "러스티 워드", "RustySword", 1280, 51, 10));
        _ProtoGoodsList.Add(new UnitGoods(110, 11, 4, "사이", "Sai [Paint]", 7080, 100, 24));
        _ProtoGoodsList.Add(new UnitGoods(111, 11, 1, "사무라이 엘더 소드", "SamuraiElderSword", 80, 6, 1));
        _ProtoGoodsList.Add(new UnitGoods(112, 11, 1, "사무라이 쉴드", "SamuraiShield1", 100, 13, 1));
        _ProtoGoodsList.Add(new UnitGoods(113, 11, 1, "사무라이 쉴드", "SamuraiShield2", 200, 14, 1));
        _ProtoGoodsList.Add(new UnitGoods(114, 11, 1, "사무라이 쉴드", "SamuraiShield3", 170, 17, 1));
        _ProtoGoodsList.Add(new UnitGoods(115, 11, 2, "사무라이 스워드", "SamuraiSword1", 880, 50, 7));
        _ProtoGoodsList.Add(new UnitGoods(116, 11, 2, "사무라이 스워드", "SamuraiSword2 [Paint]", 2210, 56, 7));
        _ProtoGoodsList.Add(new UnitGoods(117, 11, 2, "사무라이 스워드", "SamuraiSword3 [Paint]", 820, 58, 7));
        _ProtoGoodsList.Add(new UnitGoods(118, 11, 3, "SandElder Staff", "SandElderStaff", 3930, 72, 13));
        _ProtoGoodsList.Add(new UnitGoods(119, 11, 3, "SandElder Turban", "SandElderTurban", 410, 27, 7));
        _ProtoGoodsList.Add(new UnitGoods(120, 12, 4, "SandHealer Hood", "SandHealerHood", 1040, 20, 8));
        _ProtoGoodsList.Add(new UnitGoods(121, 12, 1, "SandHealerStaff", "SandHealerStaff1", 410, 27, 2));
        _ProtoGoodsList.Add(new UnitGoods(122, 12, 1, "SandHealerStaff", "SandHealerStaff2", 360, 25, 2));
        _ProtoGoodsList.Add(new UnitGoods(123, 12, 1, "SandMageStaff", "SandMageStaff1", 220, 24, 2));
        _ProtoGoodsList.Add(new UnitGoods(124, 12, 1, "SandMageStaff", "SandMageStaff2", 70, 14, 1));
        _ProtoGoodsList.Add(new UnitGoods(125, 12, 2, "SandShooterTurban", "SandShooterTurban", 1260, 44, 6));
        _ProtoGoodsList.Add(new UnitGoods(126, 12, 2, "SandWarrior Halberd", "SandWarriorHalberd", 1890, 56, 7));
        _ProtoGoodsList.Add(new UnitGoods(127, 12, 2, "샌드 와이어 투구", "SandWarriorHelm1", 990, 46, 6));
        _ProtoGoodsList.Add(new UnitGoods(128, 12, 3, "샌드 와이어 투구", "SandWarriorHelm2", 990, 48, 10));
        _ProtoGoodsList.Add(new UnitGoods(129, 12, 3, "샌드와 리어 도검", "SandWarriorSword1", 540, 30, 7));
        _ProtoGoodsList.Add(new UnitGoods(130, 13, 4, "샌드와 리어 도검", "SandWarriorSword2", 2660, 48, 13));
        _ProtoGoodsList.Add(new UnitGoods(131, 13, 1, "큰 낫", "Scythe", 190, 20, 2));
        _ProtoGoodsList.Add(new UnitGoods(132, 13, 1, "무당", "ShamanHelm [Paint]", 260, 26, 2));
        _ProtoGoodsList.Add(new UnitGoods(133, 13, 1, "샤먼 마스크", "ShamanMask [Paint]", 330, 24, 2));
        _ProtoGoodsList.Add(new UnitGoods(134, 13, 1, "샤먼 마스크", "ShamanMask", 290, 16, 1));
        _ProtoGoodsList.Add(new UnitGoods(135, 13, 2, "샤먼 마스크", "ShamanMask2", 2250, 60, 8));
        _ProtoGoodsList.Add(new UnitGoods(136, 13, 2, "샤먼 마스크", "ShamanMask3", 1400, 44, 6));
        _ProtoGoodsList.Add(new UnitGoods(137, 13, 2, "샤먼 마스크", "ShamanMask4", 1340, 34, 5));
        _ProtoGoodsList.Add(new UnitGoods(138, 13, 3, "무당", "ShamanWand", 1090, 42, 9));
        _ProtoGoodsList.Add(new UnitGoods(139, 13, 3, "단검", "ShortDagger [Paint]", 560, 24, 6));
        _ProtoGoodsList.Add(new UnitGoods(140, 14, 4, "쇼트 아이론 카타나", "ShortIronKatana [Paint]", 1240, 60, 16));
        _ProtoGoodsList.Add(new UnitGoods(141, 14, 1, "단검", "ShortSword [Paint]", 380, 20, 2));
        _ProtoGoodsList.Add(new UnitGoods(142, 14, 1, "시즈 아처 헬름", "SiegeArcherHelm [Paint]", 140, 13, 1));
        _ProtoGoodsList.Add(new UnitGoods(143, 14, 1, "스켈레톤 헬름", "SkeletonHelm", 80, 13, 1));
        _ProtoGoodsList.Add(new UnitGoods(144, 14, 1, "노예 검", "SlaveSword [Paint]", 390, 29, 2));
        _ProtoGoodsList.Add(new UnitGoods(145, 14, 2, "작은 도끼", "SmallAxe", 540, 28, 4));
        _ProtoGoodsList.Add(new UnitGoods(146, 14, 2, "Snper Hood", "SnperHood", 540, 16, 3));
        _ProtoGoodsList.Add(new UnitGoods(147, 14, 2, "창", "Spear", 1310, 50, 7));
        _ProtoGoodsList.Add(new UnitGoods(148, 14, 3, "스피어 먼 어머", "SpearmanArmor", 3170, 63, 12));
        _ProtoGoodsList.Add(new UnitGoods(149, 14, 3, "스피어 맨", "SpearmanHelm1", 3120, 84, 15));
        _ProtoGoodsList.Add(new UnitGoods(150, 15, 4, "스피어 맨", "SpearmanHelm2", 1430, 32, 10));
        _ProtoGoodsList.Add(new UnitGoods(151, 15, 1, "스틸 액스", "SteelAxe", 310, 27, 2));
        _ProtoGoodsList.Add(new UnitGoods(152, 15, 1, "강철 방패", "SteelShield", 150, 23, 2));
        _ProtoGoodsList.Add(new UnitGoods(153, 15, 1, "스틸 타워 쉴드", "SteelTowerShield", 270, 20, 2));
        _ProtoGoodsList.Add(new UnitGoods(154, 15, 1, "폭풍 염소 갑옷", "StormBringerArmor", 70, 6, 1));
        _ProtoGoodsList.Add(new UnitGoods(155, 15, 2, "폭풍 염맹의 마법봉", "StormBringerWand", 660, 50, 7));
        _ProtoGoodsList.Add(new UnitGoods(156, 15, 2, "SwampHealerStaff", "SwampHealer1Staff", 330, 10, 3));
        _ProtoGoodsList.Add(new UnitGoods(157, 15, 2, "SwampHealerStaff", "SwampHealer2Staff", 960, 36, 5));
        _ProtoGoodsList.Add(new UnitGoods(158, 15, 3, "늪지대 마스크", "SwampLordMask1", 4630, 78, 14));
        _ProtoGoodsList.Add(new UnitGoods(159, 15, 3, "늪지대 마스크", "SwampLordMask2", 1130, 69, 13));
        _ProtoGoodsList.Add(new UnitGoods(160, 16, 4, "SwampMageStaff", "SwampMage1Staff", 1350, 44, 12));
        _ProtoGoodsList.Add(new UnitGoods(161, 16, 1, "SwampMageStaff", "SwampMage2Staff", 70, 12, 1));
        _ProtoGoodsList.Add(new UnitGoods(162, 16, 1, "늪 워리어 스피어", "SwampWarriorSpear", 300, 19, 1));
        _ProtoGoodsList.Add(new UnitGoods(163, 16, 1, "구타", "SWAT", 70, 11, 1));
        _ProtoGoodsList.Add(new UnitGoods(164, 16, 1, "ThiefArmor", "ThiefArmor", 40, 8, 1));
        _ProtoGoodsList.Add(new UnitGoods(165, 16, 2, "도둑 투구", "ThiefHelm [Paint]", 380, 14, 3));
        _ProtoGoodsList.Add(new UnitGoods(166, 16, 2, "훈련 용 검", "TrainingSword [Paint]", 940, 50, 7));
        _ProtoGoodsList.Add(new UnitGoods(167, 16, 2, "양손 검", "TwoHandedSword [Paint]", 340, 34, 5));
        _ProtoGoodsList.Add(new UnitGoods(168, 16, 3, "바이킹 도끼", "VikingAxe1", 2980, 72, 13));
        _ProtoGoodsList.Add(new UnitGoods(169, 16, 3, "바이킹 도끼", "VikingAxe2", 690, 30, 7));
        _ProtoGoodsList.Add(new UnitGoods(170, 17, 4, "바이킹 도끼", "VikingAxe3", 7290, 120, 28));
        _ProtoGoodsList.Add(new UnitGoods(171, 17, 1, "바이킹 아이언 투구", "VikingIronHelm1", 90, 5, 1));
        _ProtoGoodsList.Add(new UnitGoods(172, 17, 1, "바이킹 아이언 투구", "VikingIronHelm2", 110, 15, 1));
        _ProtoGoodsList.Add(new UnitGoods(173, 17, 1, "바이킹 아이언 투구", "VikingIronHelm3", 120, 11, 1));
        _ProtoGoodsList.Add(new UnitGoods(174, 17, 1, "바이킹 가죽 투구", "VikingLeatherHelm", 410, 23, 2));
        _ProtoGoodsList.Add(new UnitGoods(175, 17, 2, "바이킹 소드", "VikingSword1", 1270, 44, 6));
        _ProtoGoodsList.Add(new UnitGoods(176, 17, 2, "바이킹 소드", "VikingSword1", 630, 32, 5));
        _ProtoGoodsList.Add(new UnitGoods(177, 17, 2, "바이킹 소드", "VikingSword2 [Paint]", 750, 22, 4));
        _ProtoGoodsList.Add(new UnitGoods(178, 17, 3, "바이킹 소드", "VikingSword3 [Paint]", 530, 24, 6));
        _ProtoGoodsList.Add(new UnitGoods(179, 17, 3, "전쟁 해머", "WarHammer", 840, 18, 5));
        _ProtoGoodsList.Add(new UnitGoods(180, 18, 4, "워락 어머", "WarlockArmor", 7560, 116, 27));
        _ProtoGoodsList.Add(new UnitGoods(181, 18, 1, "워락 두건", "WarlockHood", 120, 20, 2));
        _ProtoGoodsList.Add(new UnitGoods(182, 18, 1, "흑 마법사", "WarlockWand", 120, 7, 1));
        _ProtoGoodsList.Add(new UnitGoods(183, 18, 1, "워리어 쉴드", "WarriorShield", 140, 25, 2));
        _ProtoGoodsList.Add(new UnitGoods(184, 18, 1, "워터 어드밴드 완드", "WaterAdeptWand1", 270, 25, 2));
        _ProtoGoodsList.Add(new UnitGoods(185, 18, 2, "워터 어드밴드 완드", "WaterAdeptWand2", 830, 32, 5));
        _ProtoGoodsList.Add(new UnitGoods(186, 18, 2, "워터 위저드 갑옷", "WaterWizardArmor", 330, 28, 4));
        _ProtoGoodsList.Add(new UnitGoods(187, 18, 2, "워터 위저드 투구", "WaterWizardHelm", 420, 16, 3));
        _ProtoGoodsList.Add(new UnitGoods(188, 18, 3, "워터 위저드 완드", "WaterWizardWand", 3550, 84, 15));
        _ProtoGoodsList.Add(new UnitGoods(189, 18, 3, "마녀 갑옷", "WitchArmor", 1570, 27, 7));
        _ProtoGoodsList.Add(new UnitGoods(190, 19, 4, "마녀 모자", "WitchHat [Paint]", 3830, 88, 21));
        _ProtoGoodsList.Add(new UnitGoods(191, 19, 1, "마녀 사냥꾼", "WitchHunterArmor [Paint]", 390, 23, 2));
        _ProtoGoodsList.Add(new UnitGoods(192, 19, 1, "마녀 헌터 하트", "WitchHunterHat [Paint]", 240, 28, 2));
        _ProtoGoodsList.Add(new UnitGoods(193, 19, 1, "마녀봉", "WitchWand", 260, 16, 1));
        _ProtoGoodsList.Add(new UnitGoods(194, 19, 1, "늑대 검", "WolfSword [Paint]", 200, 23, 2));
        _ProtoGoodsList.Add(new UnitGoods(195, 19, 2, "우드 커터", "WoodcutterAxe", 800, 26, 4));
        _ProtoGoodsList.Add(new UnitGoods(196, 19, 2, "우드 커터 폴락 세", "WoodcutterPoleaxe", 1890, 60, 8));
        _ProtoGoodsList.Add(new UnitGoods(197, 19, 2, "나무 버클러", "WoodenBuckler", 520, 50, 7));
        _ProtoGoodsList.Add(new UnitGoods(198, 19, 3, "나무 메이스", "WoodenMace", 3470, 84, 15));
        _ProtoGoodsList.Add(new UnitGoods(199, 19, 3, "나무 방패", "WoodenShield", 1050, 27, 7));
        _ProtoGoodsList.Add(new UnitGoods(200, 20, 4, "나무 방패", "WoodenShield1", 1530, 48, 13));
        _ProtoGoodsList.Add(new UnitGoods(201, 20, 1, "나무 방패", "WoodenShield2", 170, 9, 1));
        _ProtoGoodsList.Add(new UnitGoods(202, 20, 1, "나무 방패", "WoodenShield3", 70, 5, 1));
        _ProtoGoodsList.Add(new UnitGoods(203, 20, 1, "나무 스파이크 클럽", "WoodenSpikedClub", 520, 30, 2));
        _ProtoGoodsList.Add(new UnitGoods(204, 20, 2, "나무 말뚝", "WoodenStake", 1220, 34, 5));
        _ProtoGoodsList.Add(new UnitGoods(205, 20, 2, "Zwei 핸드러", "Zweihander [Paint]", 970, 42, 6));



        _totalGoodsGroupCount = 21; //일단 수동.. ;
    }


    public void ShareProtoGoodsToTowns()  //원형의 상품들을 각 타운에 배분해주는 메소드
    {
        int _totalTownCount = _WorldManager._CurrTownList.Count;
        int _sharedItemCountPerTown = (int)(Random.Range(3, 7));

        for (int i = 0; i < _totalTownCount; i++)
        {
            for (int j = 0; j < _sharedItemCountPerTown; j++)
            {
                int randNum = Random.Range(0, _ProtoGoodsList.Count);
                _WorldManager._CurrTownList[i]._TownRegularlyGoods.Add(_ProtoGoodsList[randNum]);
            }
        }
    }

    UnitGoods _chosenGoods;
    public void GenerateRegularyGoodsInAllTowns()  //각 마을마다 물건을 발생시킨다.
    {
        ResetPriceRateAllTown();//물가 초기화는 밖으로 빼도 되긴 하다.

        int _supplyGoodsCount;

        for (int i = 0; i < _WorldManager._CurrTownList.Count; i++)
        {
            for (int j = 0; j < _WorldManager._CurrTownList[i]._TownRegularlyGoods.Count; j++)
            {  //각 마을별로 현재 가지고 있는 상품 개수를 체크하여, 공급하는 량의 2배를 한도로 새로 채워준다. 
               // 단, 타운의 상업도와 공급해주려는 상품 가격을 기준점으로 삼는다. 우선은, 랜덤 범위 안주고, 모두 일정량만큼 지급.

                if (_WorldManager._CurrTownList[i]._CurrTownCommerceGrade >= _WorldManager._CurrTownList[i]._TownRegularlyGoods[j]._Grade) //현재 타운 레벨이 해당 배분된 상품보다 높은 경우만 등장
                {
                    _supplyGoodsCount =
                        Mathf.Min(Mathf.Max(1,
                        (int)(Random.Range(0.8f, 1.2f) * _WorldManager._CurrTownList[i]._CurrTownCommerceGrade * 30000 / (_WorldManager._CurrTownList[i]._TownRegularlyGoods[j]._Price * _WorldManager._CurrTownList[i]._TownRegularlyGoods[j]._Weight))
                        ), _WorldManager._CurrTownList[i]._CurrTownCommerceGrade * (int)Random.Range(7, 11));

                    _chosenGoods = _WorldManager._CurrTownList[i]._TownRegularlyGoods[j];  //현재 공급하려는 물품

                    if (_WorldManager._CurrTownList[i]._CurrGoodsDic.ContainsKey(_chosenGoods))  //해당 물품종류가 이미 있을 경우
                    {
                        if (_WorldManager._CurrTownList[i]._CurrGoodsDic[_chosenGoods] + _supplyGoodsCount >= _supplyGoodsCount)
                        {
                            _supplyGoodsCount = _supplyGoodsCount - _WorldManager._CurrTownList[i]._CurrGoodsDic[_chosenGoods];
                        }
                        _WorldManager._CurrTownList[i]._CurrGoodsDic[_chosenGoods] += _supplyGoodsCount; //개수만 더해준다.
                    }
                    else  //해당 물품이 없을 경우
                    {
                        _WorldManager._CurrTownList[i]._CurrGoodsDic.Add(_chosenGoods, _supplyGoodsCount);  //아예 새롭게 디셔너리에 추가해준다.
                    }

                    _chosenGoods = null;
                }
            }

        }

    }

    public void ResetPriceRateAllTown()
    {
        for (int i = 0; i < _WorldManager._CurrTownList.Count; i++)
        {
            for (int j = 0; j < _WorldManager._CurrTownList[i]._CurrPriceRateList.Count; j++)
            {
                _WorldManager._CurrTownList[i]._CurrPriceRateList[j] = _WorldManager._CurrTownList[i]._InitPriceRateList[j] + Random.Range(-0.1f, 0.1f);
            }
        }
    }

    IEnumerator ControlGeneratingGoods()   //물건들을 주기적으로 컨트롤해주는 함수
    {
        yield return new WaitForSeconds(0.1f);
        GenerateRegularyGoodsInAllTowns();

    }





    void InitiationOfAveragePriceList()
    {
        _averagePriceRateList = new List<float>();
        //여기에 프라이스 초기화해줘야한다. 즉, 0번부터 n번 인덱스까지만 약 1.0을 추가해주는것이 필요하다.
        for (int i = 0; i < _totalGoodsGroupCount; i++)  //일단 수동.. ㅋㅋ 굿즈 개수 수량만큼 
        {
            _averagePriceRateList.Add(Random.Range(0.9f, 1.1f));
        }
    }


    public List<float> CalculateAveragePriceList()  //계산해서 반환하는 것으로..
    {
        List<float> _return = new List<float>();

        float tempSums = 0;
        float tempAverage = 0;
        int tempCount = 0;

        for (int h = 0; h < _totalGoodsGroupCount; h++)
        {
            tempCount = 0;
            tempAverage = 0;
            tempSums = 0;

            for (int i = 0; i < _WorldManager._CurrTownList.Count; i++)
            {
                tempSums += _WorldManager._CurrTownList[i]._CurrPriceRateList[h] + _WorldManager._CurrTownList[i]._TaxRate;
                tempCount++;
            }
            tempAverage = tempSums / tempCount;
            _return.Add(tempAverage);
        }

        return _return;

    }




}



