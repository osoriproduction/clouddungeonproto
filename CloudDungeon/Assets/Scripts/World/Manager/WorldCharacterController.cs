﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using HeroEditor.Common;
using HeroEditor.Common.Enums;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Assets.HeroEditor.Common.CharacterScripts;


public class WorldCharacterController : MonoBehaviour {

    //참조
    public WorldManager _WorldManager;
    public Character _Character;      

    public UI6WaysTile[] _UI6WaysTiles; //0부터 시계방향으로 돌린다.
    public GameObject _moveMarker;  //움직일때 켜진다.

    public Text _PlayerGoldText;
    public Text _PlayerTileInfoText;


    //시스템
    public bool _IsMoving;   //메인 캐 움직이는 중 표시

    void Start () {
        _moveMarker.SetActive(false);  //처음에 꺼주고
    }

    private void Update()
    {
        _PlayerGoldText.text = _WorldManager._PlayerUnitStarter._Gold + "";

        if (_WorldManager._PlayerUnitStarter._CurrOnTheTileObject != null){
            if (_WorldManager._PlayerUnitStarter._CurrUnitTown != null)
            {
                _PlayerTileInfoText.text = _WorldManager._PlayerUnitStarter._CurrOnTheTileObject._CurrUnitTile._currUnitTown._name;

            }
            else
            {
                _PlayerTileInfoText.text = _WorldManager._PlayerUnitStarter._CurrOnTheTileObject._CurrUnitTile._TileName;
            }
        }
    }

    public void ToggleMoveCommandTiles()
    {
        if (_WorldManager._IsCommandTileOn)
        {
            _WorldManager.DrawCommandTile(false, false);

        }
        else
        {
            if (!_IsMoving && !_WorldManager._UIMenuBtnController._IsWindowOn)
            {
                _WorldManager.DrawCommandTile(false, true);
            }
        }
    }


    //public void MoveCharacterToHere(float _PosX,  float _PosY)
    //{
    //    StartCoroutine(MoveCharacterToHereIEnum(_PosX, _PosY));
    //}

    //IEnumerator MoveCharacterToHereIEnum(float _PosX, float _PosY)
    //{
    //    _Character.Animator.SetBool("Run", true);
    //    _IsMoving = true;
    //    if (_PosX>= _WorldManager._CurrPositionOfMainChar.x)
    //    {
    //       _Character.transform.localScale = new Vector3(1, 1, 1);

    //    }
    //    else
    //    {
    //        _Character.transform.localScale = new Vector3(-1 ,1, 1);

    //    }

    //    float _MoveSPD = 0.1f;
    //    for (int i = 0; i < 10000; i++)
    //    {
    //        yield return new WaitForEndOfFrame();

    //        _WorldManager._CurrPositionOfMainChar += (new Vector3(_PosX, _PosY, 0) - _WorldManager._CurrPositionOfMainChar).normalized* _MoveSPD;
    //        if (DistBetweenPointR(new Vector3(_PosX, _PosY, 0), _WorldManager._CurrPositionOfMainChar) < 0.01f)
    //        {
    //            break;
    //        }
    //    }
    //    ToggleMoveCommandTiles();

    //    _Character.Animator.SetBool("Run", false);
    //    _Character.Animator.SetBool("Ready", true);
    //    _Character.Animator.SetBool("Angry", false);
    //    _IsMoving = false;


    //    _WorldManager._PlayerUnitStarter.FindMyLocatedTileObject();
    //}

    //public float DistBetweenPointR(Vector3 _target, Vector3 _refer)
    //{
    //    return (_target.x - _refer.x) * (_target.x - _refer.x) + (_target.y - _refer.y) * (_target.y - _refer.y);

    //}
}
