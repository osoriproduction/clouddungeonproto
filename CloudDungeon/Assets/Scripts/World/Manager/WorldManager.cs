﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

using UnityEngine.UI;

public class UnitTile  //이건 타일이자 곧 씬이다??? 커뮤니케이터에서 관리해야 되지 않나. 아니지. 월드매니져에서 관리 후, 필요한 정보만 커뮤니케이터에 넣는건 어때?
{
    public int _Num; //타일 넘버  +100 00250: 100번 라인의 x: 2.5     -100 00250: 100번 라인의 x:-2.5
    public float _PosX;
    public float _PosY;  //\라인 번호로 대체
    public string _TileName;  //타일의 이름 (무슨무슨 마을 )
    public bool _IsMoveUnAvailable; //유저가 이동할 수 없는 타일. (산이나 장애물 같은 지형은 이동 못하니까)

    public int _TileGrade; //타일의 그레이드. 자연타일은 모두 0. 가공타일은 모두 1~15까지 갖는다.

    public int _TileType;
    /* 타일의 타입은 랜덤 생성 시, 허용될 수 있는 이미지 범주를 의미한다.
     *  
     * 0은 마을     
     * 1000은 숲 
     * 1100은 럼버밀 
     * 2000은 산 
     * 2100은 금광 
     * 3000은 빈땅
     * 3100은 논밭
     * 4000은 물
     * 
     * 0번부터 마을 산 등 특수 지역을 리스트로 저장. 0은 시작 마을
    */
    public int _TileUniqueNum; // 해당 타일이 가지고 있는 타입 내 넘버. 한번 정해지면 계속 저장된다.

    public UnitTown _currUnitTown;  //이 타일이 마을이라면, 그 정보를 가지게 된다. 서로 대응되게 소유한다.
    public UnitTileObject _unitTileObject;  //서로 소유한다.

    public bool _IsDiscovered; // 구입된 영지내의 땅이지만, 타일을 합치고 빼서 다시 정찰이 필요한 미확인 지역.
    public bool _IsBoughtLand; //한번이라도 돈 주고 구입된 영지 내의 땅을 의미
    public bool _IsGenesisTile;//제너시스 타일임

    public int _TilePrice; //이 타일의 가격. 아직 구입하지 못한, 어두울 때만 사용한다.
    public int _TilePriceType; //이 타일의 가격 타입. 1000이면 나무, 2000이면 골드, 3000이면 식량을 의미한다. 

    public UnitFogTile _UnitFogTileObject;//안개를 의미



    public UnitTile() { }
    public UnitTile( float _PosX, float _PosY,  string _TileName, int _TileType)
    {
        this._PosX = _PosX;
        this._PosY = _PosY;
        if (_PosX >= 0)
        {
            this._Num = (int)( _PosY * 100000 + _PosX);
        }
        else
        {
            this._Num =(int)( _PosY * -100000 - _PosX);

        }

        this._TileName = _TileName;
        this._TileType = _TileType;
    }
}

public class UnitTown  // 마을 같은 곳
{

    public int _num; //마을 번호
    public string _name; //마을 네임
    public UnitTile _currUnitTile; //타운이 위치한 곳의 유닛타일.


    //개편

    public List<UnitGoods> _TownRegularlyGoods;  //정기적으로 공급되는 물건 종류들.    각각의 물품들은 그레이드에 맞는 금액 별로 배분된다. 
    public List<UnitGoods> _TownSpecialGoods;   //특별하게 공급되는 타운 특산품 종류들.

    public List<float> _CurrPriceRateList;  //현재 시세. 품목별로 존재하고, 유동적으로 변한다. 
    public List<float> _InitPriceRateList;  //최초로 부여되는, 시세를 저장해서 리셋해준다.

    public int _CurrTownCommerceGrade;  //마을의 현재 상업도. 이게 높아질수록 그에 맞는 물건들이 풀린다.  ex) Lv.1,  2, 3, 4, 5, 6, 7, 8, 9, 10
                                        //투자와 거래 금액이 많을수록 이 그레이드가 올라간다고 가정할 경우, 이것을 올릴만한 ...상대적 차이가 있는 계수가 하나쯤 있어도 될듯하지만 나중에 하고 지금은 우선, 그냥 다 동일한 거래 금액별 기준으로 하자.

    public Dictionary<UnitGoods, int> _CurrGoodsDic;   //현재 보유하고 있는 물건들

    public List<UnitStarter> _NativedStarters; //여기 출신인 스타터들.

    public int _TotalInvestmentAmount; //타운에 투자된 총 투자 금액

    public Dictionary<UnitCompanay, int> _TownStockInvestmentDic;  //타운에 투자한 컴퍼니별 비율
    public UnitCompanay _MonopolyCompany; //독점중인 컴퍼니

    public float _TaxRate; //이 택스 레이트는 모노폴리한 컴퍼니의 리더가 설정할 수 있다. ( 결국, 유저가 리더인 경우에만 설정 가능하고, 나머지는 랜덤하게 조정되는것으로 표현 )

    public int _TotalDividendAmountInTown; //현재 타운이 가지고 있는 배당 적립액

    public UnitTown() { }
    public UnitTown(int _num, string _name)
    {
        this._num = _num;
        this._name = _name;

        _TownRegularlyGoods = new List<UnitGoods>();
        _TownSpecialGoods = new List<UnitGoods>();
        _CurrPriceRateList = new List<float>();
        _InitPriceRateList = new List<float>();

        //여기에 프라이스 초기화해줘야한다. 즉, 0번부터 n번 인덱스까지만 약 1.0을 추가해주는것이 필요하다.
        for (int i=0; i<21 ;i++)  //일단 수동.. ㅋㅋ 굿즈 개수 수량만큼 
        {
            float tempRate = Random.Range(0.87f, 1.3f);
            _CurrPriceRateList.Add(tempRate);
            _InitPriceRateList.Add(tempRate);
        }
        

        _CurrTownCommerceGrade = 1;
        _TotalInvestmentAmount = 0;
        _CurrGoodsDic = new Dictionary<UnitGoods, int>();
        _NativedStarters = new List<UnitStarter>();
        _TownStockInvestmentDic = new Dictionary<UnitCompanay, int>();
        _TaxRate = 0.0f;//기본 세율
    }


}

public class WorldManager : MonoBehaviour {

    public Communicator _Communicator;    //커뮤니케이터를 인지한다. 
    public void GetCommunicator()  //커뮤니케이터를 가져온다.
    {
        if (GameObject.Find("Communicator") != null)
        {
            _Communicator = GameObject.Find("Communicator").GetComponent<Communicator>();
        }
    }

    //오브젝트 외부입력
    public StarterManager _StarterManager;
    public EconomyManager _EconomyManager;
    public UITradeWindow _UITradeWindow;
    public UIWorldMapWindow _UIWorldMapWindow;
    public UITownInfoWindow _UITownInfoWindow;
    public UICompanyInfoWindow _UICompanyInfoWindow;
    public UIPubInfoWindow _UIPubInfoWindow;
    public UICharWindow _UICharWindow;
    public UIPlayerInfoWindow _UIPlayerInfoWindow;
    public ItemManager _ItemManager;


    public GameObject _CurrCharacter;  //현재 선택된 캐릭터 오브젝트
    public GameObject _TileInfoRoot;  // 타일들의 루트 폴더
    public GameObject _FogTileInfoRoot; //안개 타일
    public GameObject _BgTileRoot;  // 타일들의 루트 폴더
    public UnitTileObject _UnitTileObject;  //타일 생성 객체
    public UnitCommandTile _UnitCommandTileObject;   //커맨드 타일 생성 객체
    public UnitFogTile _UnitFogTileObject;
    public WorldCharacterController _WorldCharacterController; //캐릭터 움직임 제어체
    public UIMenuBtnController _UIMenuBtnController; //메뉴버튼 컨트롤러
    public CompanyManager _CompanyManager;

    public TouchInputManager _TouchInputManager;

    public List<Sprite> _TileType0000;  //일반 마을
    public List<Sprite> _TileType1000; //숲
    public List<Sprite> _TileType1100; //럼버밀(나무 생산소)

    public List<Sprite> _TileType2000; //산
    public List<Sprite> _TileType2100; //금광(골드 생산소)

    public List<Sprite> _TileType3000; //빈땅
    public List<Sprite> _TileType3100; //논밭(식량 생산소)

    public List<Sprite> _TileType4000; //물(이어져있으면 물 생산)


    //특수 건물 오브젝트. 던젼 정보들을 담고 있다. 

    //   * 
    //* 0은 마을     
    //* 1000은 숲 
    //* 1100은 럼버밀
    //* 
    //* 2000은 산 
    //* 2100은 금광
    //* 
    //* 
    //* 3000은 빈땅
    //* 3100은 논밭
    //* 
    //* 
    //* 4000은 물



    //시스템 
    public bool _IsGlobalPause;//전체 포즈

    public UnitStarter _PlayerUnitStarter; // 이건 플레이어 스타터를 지칭하는 매핑.
    public Vector3 _CurrPositionOfMainChar;  //현재 캐릭터의 메인 포지션. 이 값은 타일 스케일(60)에 대응되는 개념으로 작용
    public List<UnitTile> _CurrWorldMap; //인스턴시에이트 된 현재의 월드맵
    public List<UnitTileObject> _CurrTileObjectList; //draw된 타일 오브젝트 리스트    
    public List<UnitTown> _CurrTownList;  //현재 생성된 타운 리스트    
    public List<UnitTown> _DiscoveredTownList;  //발견한 타운 리스트
    public List<string> _townNamePool; //마을 이름들 풀

    public List<UnitTile> _SelectedTileList;  //현재 선택된 타일들


    //지역 및 시간 정보
    public Text _TimeText;  //타임 정보 

    public int _SecFromStart;       //시작하고 나서의 초
    public int _TimeSec;             //0부터 9까지 돌고 10이 되면 0으로 초기화되면서 시간 올림
    public int _TimeHour;           //10초에 1시간. 
    public bool _IsNight;            //현재 낮인지. 아침 6시부터 저녁 18시까지 대낮, 아니면 나이트
    public int _Day;                  //24시간은 하루



    //타운 매니져. ( 별도로 파야되는데 귀찮아서 여기에 모아놓자 )
    public int[] _TownVarExpTable; //누적 경험치 테이블 차액분
    public int[] _TownExpTable; //누적 경험치 테이블


    //타일 정보
    public List<UnitTile> _PlayerTileList; //플레이어의 타일 리스트 . 생산소든 아니든 전부 포함. 어두운 곳도 포함
    public List<UnitTile> _PlayerProductingTileList; //현재 생산중인 타일 리스트들. 생산이 가능한 타일만 포함. (그레이드 값이 있는..)

    public UnitTile _TheGenesisTile; //맨 처음 시작하는 마을이 있는 타일

    //유저 정보
    public int _PlayerGold;
    public int _PlayerWood;
    public int _PlayerFood;

    public int _UnitProductGold;
    public int _UnitProductWood;
    public int _UnitProductFood;

    public Text _PlayerGoldText;
    public Text _PlayerWoodText;
    public Text _PlayerFoodText;


    private void Update()
    {
        _PlayerGoldText.text = _PlayerGold + " (+" +_UnitProductGold+")" ;
        _PlayerWoodText.text = _PlayerWood + " (+" + _UnitProductWood + ")";
        _PlayerFoodText.text = _PlayerFood + " (+" + _UnitProductFood + ")";

    }

    void Start () {

        Application.targetFrameRate = 60;
        _EconomyManager = GetComponent<EconomyManager>();
        _CompanyManager = GetComponent<CompanyManager>();
        _ItemManager = GetComponent<ItemManager>();

        _CurrTownList = new List<UnitTown>();
        _DiscoveredTownList  = new List<UnitTown>();
        _PlayerTileList = new List<UnitTile>();
        _PlayerProductingTileList = new List<UnitTile>();
        _TheGenesisTile = new UnitTile();
        _SelectedTileList = new List<UnitTile>();


        InputTownNames();
        InputTownExpTable();

        StartCoroutine(Init());
       // StartCoroutine(SecCounter());
    }

    IEnumerator SecCounter()
    {
        var _OneSec = new WaitForSeconds(1);
        var _UnitSec = new WaitForSeconds(5);

        yield return new WaitForSeconds(0.01f);

        _TimeHour = 6;
        _IsNight = false;


        for (; ; )
        {
            yield return _UnitSec;
                 
        }


    }

    IEnumerator Init()
    {
        yield return new WaitForSeconds(0.02f);
        InstantiateCurrentWorldMapData(10);
        DrawCurrentWorldMap();

        DrawCommandTile(true, true);
        DrawCommandTile(false, false);
        DrawFogTile(true);

        GiveTheseTilesToPlayer(_TheGenesisTile, 1200);
        DrawFogTile(false);//암흑 타일 표시
        //SetFirstInvestmentByIndian();  //타운이 생성된 후 해줘야 한다.

    }



    void InputTownNames()
    {
        _townNamePool = new List<string>();

        _townNamePool.Add("이스탄불");
        _townNamePool.Add("리스본");
        _townNamePool.Add("마드리드");
        _townNamePool.Add("바르셀로나");
        _townNamePool.Add("마르세유");
        _townNamePool.Add("낭트");
        _townNamePool.Add("평양");
        _townNamePool.Add("서울");
        _townNamePool.Add("뉴욕");
        _townNamePool.Add("로스앤젤레스");
        _townNamePool.Add("하노이");
        _townNamePool.Add("도쿄");
        _townNamePool.Add("교토");
        _townNamePool.Add("나가사키");
        _townNamePool.Add("빈");
        _townNamePool.Add("인천");
        _townNamePool.Add("부산"); 
        _townNamePool.Add("울산");
        _townNamePool.Add("전주");
        _townNamePool.Add("송도");
        _townNamePool.Add("해남");
        _townNamePool.Add("베이징");
        _townNamePool.Add("오사카");
        _townNamePool.Add("파리");
        _townNamePool.Add("런던");


    }


    void InstantiateCurrentWorldMapData(int _MaxTownCount)   //맵 데이터를 생성한다. 나중에는  랜덤생성도 가능할수도 있다.
    {
        int tempTileType = 0; //임시로 타일타입은 랜덤 생성
        UnitTile tempUnitTile = null;
        //int _ResidualTownCount = _MaxTownCount;  //아직 더 생성할 수 있는 타운 수
        int _ResidualTownCount = 8;  //아직 더 생성할 수 있는 타운 수

        _CurrWorldMap = new List<UnitTile>();
        int tempTownCount = 0;

        bool _IsTempDiscovered ;
        bool _IsTempBought ;

        for (int i = 90; i < 110; i++)   //행별 생성
        {
            for (int j = 0; j < 20; j++)   //열별 생성
            {

                if (i == 100 && j == 10) //스타팅 포인트이다.
                {
                    tempTileType = 0;
                }
                else if (Random.Range(0, 100) < 30)
                {  //숲 
                    tempTileType = 1000;

                }
                else if (Random.Range(0, 100) < 47)  //산
                {
                    tempTileType = 2000;

                }
                else if (Random.Range(0, 100) < 90)  //평지
                {
                    tempTileType = 3000;
                }
                else   //물 이동 불가
                {
                    tempTileType = 4000;

                }

                if( i==100 && j==10 )  //시작 마을만 일단 좀 켜주지.
                {
                    _IsTempDiscovered = true;
                    _IsTempBought = true;

                }
                else
                {
                    _IsTempDiscovered = false;
                    _IsTempBought = false;

                }


                if (i % 2 == 0) //타일 인스턴스 생성
                {
                    tempUnitTile = new UnitTile(-50 + j * 2.5f, i, "지역명", tempTileType);
                    
                }
                else
                {
                    tempUnitTile = new UnitTile(-48.75f + j * 2.5f, i, "지역명", tempTileType);
                }

                if (i == 100 && j == 10) //스타팅 포인트이다.
                {
                    tempUnitTile._TileUniqueNum = 0;  //이러고 0인덱스에 주성같은거 넣어줄 수도 있긴 하다.
                    tempUnitTile._IsGenesisTile = true;
                    _TheGenesisTile = tempUnitTile;
                    

                }
                else if (tempTileType == 1000) //숲
                {
                    tempUnitTile._TileUniqueNum = Random.Range(0, _TileType1000.Count);

                }
                else if (tempTileType == 1100) //럼버밀
                {
                    tempUnitTile._TileUniqueNum = Random.Range(0, _TileType1100.Count);

                }
                else if (tempTileType == 2000) //산
                {
                    tempUnitTile._TileUniqueNum = Random.Range(0, _TileType2000.Count);

                }
                else if (tempTileType == 2100) //금광
                {
                    tempUnitTile._TileUniqueNum = Random.Range(0, _TileType2100.Count);

                }
                else if (tempTileType == 3000)  //평지
                {
                    tempUnitTile._TileUniqueNum = Random.Range(0, _TileType3000.Count);

                }
                else if (tempTileType == 3100)  //논밭
                {
                    tempUnitTile._TileUniqueNum = Random.Range(0, _TileType3100.Count);


                }
                else if (tempTileType == 4000)  //물
                {
                    tempUnitTile._TileUniqueNum = Random.Range(0, _TileType4000.Count);

                }
                else if (tempTileType == 0)
                {
                    tempUnitTile._TileUniqueNum = Random.Range(0, _TileType0000.Count);
                 

                }

                tempUnitTile._IsBoughtLand = _IsTempBought;
                tempUnitTile._IsDiscovered = _IsTempDiscovered;

                _CurrWorldMap.Add(tempUnitTile);

            }
        }
    }

  
    public void DrawCurrentWorldMap()
    {
        _CurrTileObjectList = new List<UnitTileObject>();
        

        for (int i = 0; i < _CurrWorldMap.Count; i++)
        {
            UnitTileObject tempTileObject = Instantiate(_UnitTileObject, _BgTileRoot.transform);
            tempTileObject._CurrUnitTile = _CurrWorldMap[i];
            tempTileObject._CurrUnitTile._unitTileObject = tempTileObject;
            tempTileObject.name = _CurrWorldMap[i]._Num+"";
            tempTileObject.transform.localPosition = new Vector3(tempTileObject._CurrUnitTile._PosX, (tempTileObject._CurrUnitTile._PosY-100)*-2);

            tempTileObject.gameObject.AddComponent<SpriteRenderer>();
            tempTileObject.GetComponent<SpriteRenderer>().sortingOrder = (int)(-1*tempTileObject.transform.localPosition.y);

            if (_CurrWorldMap[i]._TileType == 1000)
            {           
                tempTileObject.GetComponent<SpriteRenderer>().sprite = _TileType1000[tempTileObject._CurrUnitTile._TileUniqueNum];
            }
            else if (_CurrWorldMap[i]._TileType == 1100)
            {
                tempTileObject.GetComponent<SpriteRenderer>().sprite = _TileType1100[tempTileObject._CurrUnitTile._TileUniqueNum];
            }
            else if (_CurrWorldMap[i]._TileType == 2000)
            {
                tempTileObject.GetComponent<SpriteRenderer>().sprite = _TileType2000[tempTileObject._CurrUnitTile._TileUniqueNum];
            }
            else if (_CurrWorldMap[i]._TileType == 2100)
            {
                tempTileObject.GetComponent<SpriteRenderer>().sprite = _TileType2100[tempTileObject._CurrUnitTile._TileUniqueNum];
            }
            else if (_CurrWorldMap[i]._TileType == 3000)
            {
                tempTileObject.GetComponent<SpriteRenderer>().sprite = _TileType3000[tempTileObject._CurrUnitTile._TileUniqueNum];
            }
            else if (_CurrWorldMap[i]._TileType == 3100)
            {
                tempTileObject.GetComponent<SpriteRenderer>().sprite = _TileType3100[tempTileObject._CurrUnitTile._TileUniqueNum];
            }
            else if (_CurrWorldMap[i]._TileType == 4000)
            {
                tempTileObject.GetComponent<SpriteRenderer>().sprite = _TileType4000[tempTileObject._CurrUnitTile._TileUniqueNum];
            }
            else if (_CurrWorldMap[i]._TileType == 0)
            {
                if (tempTileObject._CurrUnitTile._PosX == 0 && tempTileObject._CurrUnitTile._PosY == 100)
                {
                    tempTileObject.GetComponent<SpriteRenderer>().sprite = _TileType0000[0];
                }
                else
                {
                    tempTileObject.GetComponent<SpriteRenderer>().sprite = _TileType0000[tempTileObject._CurrUnitTile._TileUniqueNum];
                }
           
            }
          

            _CurrTileObjectList.Add(tempTileObject);
            
        }
    
    }

    public bool _IsCommandTileOn;
    public List<UnitCommandTile> _CommandTiles;
    public void DrawCommandTile(bool _init, bool _isOn)  // 이동할 때 뜨는 이동 가능 타일 리스트
    {
        if (_init)  //게임 최초 시작시 한번 그려주고 저장한다.
        {
            _CommandTiles = new List<UnitCommandTile>();
            for (int i = 0; i < _CurrWorldMap.Count; i++)
            {
                UnitCommandTile tempTileObject = Instantiate(_UnitCommandTileObject, _TileInfoRoot.transform);
                tempTileObject._CurrUnitTile = _CurrWorldMap[i];
                tempTileObject.name = _CurrWorldMap[i]._Num + "";
                tempTileObject.transform.localPosition = new Vector3(tempTileObject._CurrUnitTile._PosX, (tempTileObject._CurrUnitTile._PosY - 100) * -2);
                            
                tempTileObject._CanNotMoveBtnObject.gameObject.SetActive(false);
                tempTileObject._MoveSelectBtnObject.gameObject.SetActive(false);
                              
                _CommandTiles.Add(tempTileObject);
            }
        }
        else
        {
            if (_isOn)  //킬 때 쓰는것.
            {
                _WorldCharacterController._moveMarker.SetActive(true);

                for (int i = 0; i < 6; i++) // 6웨이 버튼들을 모두 초기화해준다.
                {
                    _WorldCharacterController._UI6WaysTiles[i]._currCommandTile = null;
                }

                _IsCommandTileOn = true;
                for (int i = 0; i < _CommandTiles.Count; i++)
                {
                    if (!_CommandTiles[i]._CurrUnitTile._IsMoveUnAvailable && DistBetweenPointR(_PlayerUnitStarter.transform.position, _CommandTiles[i].transform.position) < 10 * 50)   // 이동 범주 안에 드는 타일들 체크
                    {
                        _CommandTiles[i]._CanNotMoveBtnObject.gameObject.SetActive(false);
                        _CommandTiles[i]._MoveSelectBtnObject.gameObject.SetActive(true);

                        if (DistBetweenPointR(_PlayerUnitStarter.transform.position, _CommandTiles[i].transform.position) < 1 * 60)
                        {
                            _CommandTiles[i]._CanNotMoveBtnObject.gameObject.SetActive(false);
                            _CommandTiles[i]._MoveSelectBtnObject.gameObject.SetActive(false);
                        }
                        else
                        {
                            float dx = _CommandTiles[i].transform.position.x - _PlayerUnitStarter.transform.position.x;
                            float dy = _CommandTiles[i].transform.position.y - _PlayerUnitStarter.transform.position.y;

                            if (dx >= 0)
                            {
                                if (dy > 1)
                                {
                                    _WorldCharacterController._UI6WaysTiles[1]._currCommandTile = _CommandTiles[i];

                                }                              
                                else if( dy<-1 )
                                {
                                    _WorldCharacterController._UI6WaysTiles[3]._currCommandTile = _CommandTiles[i];

                                }
                                else
                                {
                                    _WorldCharacterController._UI6WaysTiles[2]._currCommandTile = _CommandTiles[i];

                                }
                            }
                            else
                            {
                                if (dy > 1)
                                {
                                    _WorldCharacterController._UI6WaysTiles[0]._currCommandTile = _CommandTiles[i];

                                }                             
                                else if (dy < -1)
                                {
                                    _WorldCharacterController._UI6WaysTiles[4]._currCommandTile = _CommandTiles[i];                                    
                                }
                                else
                                {
                                    _WorldCharacterController._UI6WaysTiles[5]._currCommandTile = _CommandTiles[i];

                                }
                            }
                        }
                    }
                    else if(DistBetweenPointR(_PlayerUnitStarter.transform.position, _CommandTiles[i].transform.position) < 500 * 60)
                    {
                        _CommandTiles[i]._CanNotMoveBtnObject.gameObject.SetActive(true);
                        _CommandTiles[i]._MoveSelectBtnObject.gameObject.SetActive(false);

                    }                  
                }
            }
            else
            {
                //_WorldCharacterController._moveMarker.SetActive(false);


                _IsCommandTileOn = false;
                for (int i = 0; i < _CommandTiles.Count; i++)
                {
                    if (_CommandTiles[i]._CurrUnitTile._IsMoveUnAvailable)
                    {
                        _CommandTiles[i]._CanNotMoveBtnObject.gameObject.SetActive(false);
                        _CommandTiles[i]._MoveSelectBtnObject.gameObject.SetActive(false);
                    }
                    else
                    {
                        _CommandTiles[i]._CanNotMoveBtnObject.gameObject.SetActive(false);
                        _CommandTiles[i]._MoveSelectBtnObject.gameObject.SetActive(false);

                    }
                }
            }
        }
    }

    public UnitTile[] GetNearTiles(UnitTile _UnitTile,UnitStarter _ReferStarter)
    {
        UnitTile[] _returns = new UnitTile[] { null, null, null, null, null, null };

        for (int i = 0; i < _CurrWorldMap.Count; i++)
        {
            if (!_CurrWorldMap[i]._IsMoveUnAvailable && DistBetweenPointR(_ReferStarter.transform.position, _CurrWorldMap[i]._unitTileObject.transform.position) < 10 * 50)   // 이동 범주 안에 드는 타일들 체크
            {

                if (DistBetweenPointR(_ReferStarter.transform.position, _CurrWorldMap[i]._unitTileObject.transform.position) < 1 * 60)
                {

                }
                else
                {
                    float dx = _CurrWorldMap[i]._unitTileObject.transform.position.x - _ReferStarter.transform.position.x;
                    float dy = _CurrWorldMap[i]._unitTileObject.transform.position.y - _ReferStarter.transform.position.y;

                    if (dx >= 0)
                    {
                        if (dy > 1)
                        {
                            _returns[1] = _CurrWorldMap[i];

                        }
                        else if (dy < -1)
                        {
                            _returns[3] = _CurrWorldMap[i];

                        }
                        else
                        {
                            _returns[2] = _CurrWorldMap[i];

                        }
                    }
                    else
                    {
                        if (dy > 1)
                        {
                            _returns[0] = _CurrWorldMap[i];

                        }
                        else if (dy < -1)
                        {
                            _returns[4] = _CurrWorldMap[i];
                        }
                        else
                        {
                            _returns[5] = _CurrWorldMap[i];

                        }
                    }
                }
            }
        }
        return _returns;
    }

    public UnitTile GetNearTileToApproachTile(UnitTile _UnitTile, UnitStarter _ReferStarter)
    {
        UnitTile _returnTile = new UnitTile();
        float _nearestDistR = 999999;
        float _tempDistR;
        for (int i = 0; i < _CurrWorldMap.Count; i++)
        {
            if (!_CurrWorldMap[i]._IsMoveUnAvailable && DistBetweenPointR(_ReferStarter.transform.position, _CurrWorldMap[i]._unitTileObject.transform.position) < 10 * 45)   // 이동 범주 안에 드는 타일들 체크
            {
                _tempDistR = DistBetweenPointR(_CurrWorldMap[i]._unitTileObject.transform.position, _UnitTile._unitTileObject.transform.position);
                if (_tempDistR < DistBetweenPointR(_ReferStarter.transform.position, _UnitTile._unitTileObject.transform.position)   )  //바로 가는 것보다 더 거리가 짧은 타일일 경우에 인정.
                {
                    if(_tempDistR<_nearestDistR)
                    {
                        _nearestDistR = _tempDistR;
                        _returnTile = _CurrWorldMap[i];  
                    }                    
                }                            
                
            }
        }

        return _returnTile;
    }





    public List<UnitFogTile> _FogTiles;
    public void DrawFogTile(bool _IsInit)  // 이동할 때 뜨는 이동 가능 타일 리스트
    {
        if (_IsInit)  //게임 최초 시작시 한번 그려주고 저장한다.
        {
            _FogTiles = new List<UnitFogTile>();
            for (int i = 0; i < _CurrWorldMap.Count; i++)
            {
                UnitFogTile tempTileObject = Instantiate(_UnitFogTileObject, _FogTileInfoRoot.transform);
                tempTileObject._CurrUnitTile = _CurrWorldMap[i];
                tempTileObject.name = _CurrWorldMap[i]._Num + "";
                tempTileObject.transform.localPosition = new Vector3(tempTileObject._CurrUnitTile._PosX, (tempTileObject._CurrUnitTile._PosY - 100) * -2);

                tempTileObject._DarkObject.gameObject.SetActive(true);
                tempTileObject._CanNotSeeObject.gameObject.SetActive(false);
                tempTileObject._CurrUnitTile._UnitFogTileObject = tempTileObject;
                _FogTiles.Add(tempTileObject);
                SetDarkTilePrice(tempTileObject._CurrUnitTile);
            }
        }
        else  //체크에 쓴다.
        {
            for (int i = 0; i < _FogTiles.Count; i++)
            {
                SetDarkTilePrice(_FogTiles[i]._CurrUnitTile);


                if (_FogTiles[i]._CurrUnitTile._IsDiscovered) //발견된 "땅
                {
                    _FogTiles[i]._CanNotSeeObject.SetActive(false);

                }
                else
                {
                    _FogTiles[i]._CanNotSeeObject.SetActive(true);
                    
                }
                if (_FogTiles[i]._CurrUnitTile._IsBoughtLand)  //한번이라도 구입한 땅
                {
                    _FogTiles[i]._DarkObject.SetActive(false);

                }
                else
                {
                    _FogTiles[i]._DarkObject.SetActive(true);

                }


            }    
        }
    }

    public void SetDarkTilePrice(UnitTile _UnitTile) //개별적인 유닛 타일의 가격을 매겨주고, 관련된 해당 디스플레이를 처리해준다.
    {
        if (!_UnitTile._IsBoughtLand) //사지 못한 땅만 처리해준다.
        {
            if (CheckNearBoughtTile(_UnitTile)) //근처에 산 땅이 있다. 즉 접한경우이다.
            {
                if (_UnitTile._TileType == 1000)
                {

                    if (_UnitTile._Num % 2 == 0)
                    {
                        _UnitTile._TilePriceType = 2000;

                    }
                    else
                    {
                        _UnitTile._TilePriceType = 3000;

                    }

                }
                else if (_UnitTile._TileType == 2000)
                {
   
                    if (_UnitTile._Num % 2 == 0)
                    {

                        _UnitTile._TilePriceType = 1000;
                    }
                    else
                    {
                        _UnitTile._TilePriceType = 3000;

                    }
                }
                else if (_UnitTile._TileType == 3000)
                {


                    if (_UnitTile._Num % 2 == 0)
                    {
                        _UnitTile._TilePriceType = 1000;

                    }
                    else
                    {
                        _UnitTile._TilePriceType = 2000;

                    }
                }
                else if (_UnitTile._TileType == 4000)
                {
                    if (_UnitTile._Num % 3 == -1)
                    {
                        _UnitTile._TilePriceType = 1000;

                    }
                    else if (_UnitTile._Num % 3 == -2)
                    {
                        _UnitTile._TilePriceType = 2000;

                    }
                    else
                    {
                        _UnitTile._TilePriceType = 3000;

                    }
                }
             
                _UnitTile._TilePrice = _PlayerTileList.Count-18;  // 플레이어 타일 개수만큼 차징한다.

                _UnitTile._UnitFogTileObject._TilePriceText.gameObject.SetActive(true);                
                _UnitTile._UnitFogTileObject._TilePriceText.text = _UnitTile._TilePrice + "";

                if(_UnitTile._TilePriceType == 1000)
                {
                    _UnitTile._UnitFogTileObject._ResWood.SetActive(true);
                    _UnitTile._UnitFogTileObject._ResGold.SetActive(false);
                    _UnitTile._UnitFogTileObject._ResFood.SetActive(false);

                }
                else if (_UnitTile._TilePriceType == 2000)
                {
                    _UnitTile._UnitFogTileObject._ResWood.SetActive(false);
                    _UnitTile._UnitFogTileObject._ResGold.SetActive(true);
                    _UnitTile._UnitFogTileObject._ResFood.SetActive(false);
                }
                else if (_UnitTile._TilePriceType == 3000)
                {
                    _UnitTile._UnitFogTileObject._ResWood.SetActive(false);
                    _UnitTile._UnitFogTileObject._ResGold.SetActive(false);
                    _UnitTile._UnitFogTileObject._ResFood.SetActive(true);
                }

            }
            else
            {
                _UnitTile._UnitFogTileObject._TilePriceText.gameObject.SetActive(false);
                _UnitTile._UnitFogTileObject._TilePriceText.text = "";

                _UnitTile._UnitFogTileObject._ResWood.SetActive(false);
                _UnitTile._UnitFogTileObject._ResGold.SetActive(false);
                _UnitTile._UnitFogTileObject._ResFood.SetActive(false);
            }
        }
        else
        {
            _UnitTile._UnitFogTileObject._TilePriceText.gameObject.SetActive(false);
            _UnitTile._TilePrice = 0;
            _UnitTile._UnitFogTileObject._TilePriceText.text = "";

            _UnitTile._UnitFogTileObject._ResWood.SetActive(false);
            _UnitTile._UnitFogTileObject._ResGold.SetActive(false);
            _UnitTile._UnitFogTileObject._ResFood.SetActive(false);
        }
    }

    public bool CheckNearBoughtTile(UnitTile _UnitTile)
    {
        bool _return = false;
        
        for (int i = 0; i < _FogTiles.Count; i++)
        {
            if (DistBetweenPointR(_UnitTile._UnitFogTileObject.transform.position, _FogTiles[i].transform.position) < 500)   //  1칸 이내만 검색한다.
            {
                if (_FogTiles[i]._CurrUnitTile._IsBoughtLand)  //근처 타일 중에 산 땅이 있다면...
                {
                    _return = true;
                    break;
                }
            }
        }
        return _return;
    }



    public void GiveTheseTilesToPlayer(UnitTile _UnitTile, float _CheckRange) //_CheckRange=500 1칸, 1200 2칸
    {
        for (int i = 0; i < _FogTiles.Count; i++)
        {
            if (DistBetweenPointR(_UnitTile._UnitFogTileObject.transform.position, _FogTiles[i].transform.position) < _CheckRange)   //  체크 범주 안의 타일들을 모두 귀속시켜준다.
            {
                _FogTiles[i]._CurrUnitTile ._IsDiscovered = true;
                _FogTiles[i]._CurrUnitTile._IsBoughtLand = true;
                _PlayerTileList.Add(_FogTiles[i]._CurrUnitTile);
            }
            else
            {
                _FogTiles[i]._CurrUnitTile._IsDiscovered = false;
                _FogTiles[i]._CurrUnitTile._IsBoughtLand = false;
            }
           
        }
    }


    
    //타운 매니져


    void InputTownExpTable()  //타운 경험치 테이블 추가
    {
        _TownVarExpTable = new int[30];
        _TownVarExpTable[0] = 0;
        _TownVarExpTable[1] = 5000;

        _TownExpTable = new int[30];
        _TownExpTable[0] = 0;
        _TownExpTable[1] = 7000;

        for (int i = 2; i < 30; i++)
        {
            _TownVarExpTable[i] = (int)(_TownVarExpTable[i - 1] * 1.2f);
            _TownExpTable[i] = (int)(_TownExpTable[i - 1] + _TownVarExpTable[i]);

        }
    }


    public void SetFirstInvestmentByIndian()   //초반에 원주민들에 의한 투자 금액을 가상으로 설정해준다.
    {
        for (int i = 0; i < _CurrTownList.Count; i++)
        {
            if (Random.Range(0, 100) < 20)
            {
                _CurrTownList[i]._TownStockInvestmentDic.Add(_CompanyManager._CurrCompanyList[0], 1500 * (int)Random.Range(4, 8));

            }
            else
            {
                _CurrTownList[i]._TownStockInvestmentDic.Add(_CompanyManager._CurrCompanyList[0], 1000 * (int)Random.Range(2, 4));

            }
            CalculateTotalInvestmentAmount(_CurrTownList[i]);
            LevelDeciderForTown(_CurrTownList[i]);
            _CurrTownList[i]._MonopolyCompany = _CompanyManager._CurrCompanyList[0];
        }
    }

    public void LevelDeciderForTown(UnitTown _UnitTown)   //총 투자금액과 레벨 테이블을 가지고 타운의 레벨을 결정해주는 메소드.
    {

        CalculateTotalInvestmentAmount(_UnitTown);  //레벨 업 체크 전 총 금액을 계산해준다.

        int tempLevel = 1;

        for (int i = 1; i < _TownExpTable.Length; i++)
        {
            if (_UnitTown._TotalInvestmentAmount >= _TownExpTable[i])
            {
                tempLevel = i + 1;
            }
        }

        if (_UnitTown._CurrTownCommerceGrade < tempLevel)
        {
            for (int i = 0; i < tempLevel - _UnitTown._CurrTownCommerceGrade; i++)
            {
                //레벨업 시 처리
                
            }
        }
        _UnitTown._CurrTownCommerceGrade = tempLevel;
    }




    public void InvestmentToTown(UnitTown _UnitTown, UnitCompanay _InvestmentCompany, int _InvestmentAmount, UnitStarter _UnitStarter)  //타운에 투자금액 넣는 메소드
    {
        if (_UnitTown._TownStockInvestmentDic.ContainsKey(_InvestmentCompany))  //현재 타운에 동일한 투자자가 존재했다면 여기로
        {
            _UnitTown._TownStockInvestmentDic[_InvestmentCompany] += _InvestmentAmount;

        }
        else
        {
            _UnitTown._TownStockInvestmentDic.Add(_InvestmentCompany, _InvestmentAmount);
        }

        if(_UnitTown._TownStockInvestmentDic[_InvestmentCompany] > _UnitTown._TownStockInvestmentDic[_UnitTown._MonopolyCompany])  //최대 주주를 변경해준다.
        {
            _UnitTown._MonopolyCompany = _InvestmentCompany;
            _UnitTown._currUnitTile._unitTileObject._MonoPolyCompanyText.gameObject.SetActive(true);
            _UnitTown._currUnitTile._unitTileObject._MonoPolyCompanyText.text = _InvestmentCompany._Name;
                
        }

        LevelDeciderForTown(_UnitTown);  //투자 후 총 금액을 계산해서 레벨업해준다.
        _UnitStarter._Gold -= _InvestmentAmount;
        
        _CompanyManager.AddStarterToCompany(_InvestmentCompany, _UnitStarter, _InvestmentAmount);  //컴퍼니 투자금액에 스타터를 넣어준다.
        _CompanyManager.InsertInvestAmountToStockPerTownDic(_InvestmentCompany, _UnitTown, _InvestmentAmount);//컴퍼니의 지분 리스트에 현재 투자를 넣어준다.
    }




    public void CalculateTotalInvestmentAmount(UnitTown _UnitTown)  //총 투자 금액을 수동으로 계산해주는 메소드
    {
        int _TotalInvestAmount = 0;

        for (int i = 0; i < _UnitTown._TownStockInvestmentDic.Values.Count; i++)
        {
            _TotalInvestAmount += _UnitTown._TownStockInvestmentDic.Values.ToList()[i];

        }
        _UnitTown._TotalInvestmentAmount = _TotalInvestAmount;

    }



    //기능
    public float DistBetweenPointR(Vector3 _target, Vector3 _refer)
    {
        return (_target.x - _refer.x) * (_target.x - _refer.x) + (_target.y - _refer.y) * (_target.y - _refer.y);

    }


    //타일 기능
    public void ReDrawOnlyOneLand(UnitTile _UnitTile )
    {
        if (_UnitTile._TileType == 1000) //숲
        {
            _UnitTile._TileUniqueNum = Random.Range(0, _TileType1000.Count);

        }
        else if (_UnitTile._TileType == 1100) //럼버밀
        {
            _UnitTile._TileUniqueNum = Random.Range(0, _TileType1100.Count);

        }
        else if (_UnitTile._TileType == 2000) //산
        {
            _UnitTile._TileUniqueNum = Random.Range(0, _TileType2000.Count);

        }
        else if (_UnitTile._TileType == 2100) //금광
        {
            _UnitTile._TileUniqueNum = Random.Range(0, _TileType2100.Count);

        }
        else if (_UnitTile._TileType == 3000)  //평지
        {
            _UnitTile._TileUniqueNum = Random.Range(0, _TileType3000.Count);

        }
        else if (_UnitTile._TileType == 3100)  //논밭
        {
            _UnitTile._TileUniqueNum = Random.Range(0, _TileType3100.Count);


        }
        else if (_UnitTile._TileType == 4000)  //물
        {
            _UnitTile._TileUniqueNum = Random.Range(0, _TileType4000.Count);

        }
        else if (_UnitTile._TileType == 0)
        {
            _UnitTile._TileUniqueNum = Random.Range(0, _TileType0000.Count);


        }

        if (_UnitTile._TileType == 1000)
        {
            _UnitTile._unitTileObject.GetComponent<SpriteRenderer>().sprite = _TileType1000[_UnitTile._unitTileObject._CurrUnitTile._TileUniqueNum];
        }
        else if (_UnitTile._TileType == 1100)
        {
            _UnitTile._unitTileObject.GetComponent<SpriteRenderer>().sprite = _TileType1100[_UnitTile._unitTileObject._CurrUnitTile._TileUniqueNum];
        }
        else if (_UnitTile._TileType == 2000)
        {
            _UnitTile._unitTileObject.GetComponent<SpriteRenderer>().sprite = _TileType2000[_UnitTile._unitTileObject._CurrUnitTile._TileUniqueNum];
        }
        else if (_UnitTile._TileType == 2100)
        {
            _UnitTile._unitTileObject.GetComponent<SpriteRenderer>().sprite = _TileType2100[_UnitTile._unitTileObject._CurrUnitTile._TileUniqueNum];
        }
        else if (_UnitTile._TileType == 3000)
        {
            _UnitTile._unitTileObject.GetComponent<SpriteRenderer>().sprite = _TileType3000[_UnitTile._unitTileObject._CurrUnitTile._TileUniqueNum];
        }
        else if (_UnitTile._TileType == 3100)
        {
            _UnitTile._unitTileObject.GetComponent<SpriteRenderer>().sprite = _TileType3100[_UnitTile._unitTileObject._CurrUnitTile._TileUniqueNum];
        }
        else if (_UnitTile._TileType == 4000)
        {
            _UnitTile._unitTileObject.GetComponent<SpriteRenderer>().sprite = _TileType4000[_UnitTile._unitTileObject._CurrUnitTile._TileUniqueNum];
        }
        else if (_UnitTile._TileType == 0)
        {
            _UnitTile._unitTileObject.GetComponent<SpriteRenderer>().sprite = _TileType0000[_UnitTile._unitTileObject._CurrUnitTile._TileUniqueNum];

        }

    }

    public void ReDrawOnlyDarkLands()  //어두운 지역의 랜드타일 배경 스프라이트 들만 다시 그려준다.
    {

        for (int i = 0; i < _CurrWorldMap.Count; i++)
        {
            if (!_CurrWorldMap[i]._IsDiscovered && _CurrWorldMap[i]._IsBoughtLand)
            {
                //그레이드를 초기화해주고, 타일 타입을 새로 해준다.
                if (Random.Range(0, 100) < 30)
                {  //숲 
                    _CurrWorldMap[i]._TileType = 1000;

                }
                else if (Random.Range(0, 100) < 47)  //산
                {
                    _CurrWorldMap[i]._TileType = 2000;

                }
                else if (Random.Range(0, 100) < 90)  //평지
                {
                    _CurrWorldMap[i]._TileType = 3000;
                }
                else   //물 이동 불가
                {
                    _CurrWorldMap[i]._TileType = 4000;

                }
                

                //우선 유니크 넘버부터 새로 배정해준다.
                if (_CurrWorldMap[i]._TileType == 1000) //숲
                {
                    _CurrWorldMap[i]._TileUniqueNum = Random.Range(0, _TileType1000.Count);

                }
                else if (_CurrWorldMap[i]._TileType == 1100) //럼버밀
                {
                    _CurrWorldMap[i]._TileUniqueNum = Random.Range(0, _TileType1100.Count);

                }
                else if (_CurrWorldMap[i]._TileType == 2000) //산
                {
                    _CurrWorldMap[i]._TileUniqueNum = Random.Range(0, _TileType2000.Count);

                }
                else if (_CurrWorldMap[i]._TileType == 2100) //금광
                {
                    _CurrWorldMap[i]._TileUniqueNum = Random.Range(0, _TileType2100.Count);

                }
                else if (_CurrWorldMap[i]._TileType == 3000)  //평지
                {
                    _CurrWorldMap[i]._TileUniqueNum = Random.Range(0, _TileType3000.Count);

                }
                else if (_CurrWorldMap[i]._TileType == 3100)  //논밭
                {
                    _CurrWorldMap[i]._TileUniqueNum = Random.Range(0, _TileType3100.Count);


                }
                else if (_CurrWorldMap[i]._TileType == 4000)  //물
                {
                    _CurrWorldMap[i]._TileUniqueNum = Random.Range(0, _TileType4000.Count);

                }
                else if (_CurrWorldMap[i]._TileType == 0)
                {
                    _CurrWorldMap[i]._TileUniqueNum = Random.Range(0, _TileType0000.Count);


                }

                //그 다음에 배정된 유니크 넘버대로 스프라이트를 갈아껴주자.
                if (_CurrWorldMap[i]._TileType == 1000)
                {
                    _CurrWorldMap[i]._unitTileObject.GetComponent<SpriteRenderer>().sprite = _TileType1000[_CurrWorldMap[i]._unitTileObject._CurrUnitTile._TileUniqueNum];
                }
                else if (_CurrWorldMap[i]._TileType == 1100)
                {
                    _CurrWorldMap[i]._unitTileObject.GetComponent<SpriteRenderer>().sprite = _TileType1100[_CurrWorldMap[i]._unitTileObject._CurrUnitTile._TileUniqueNum];
                }
                else if (_CurrWorldMap[i]._TileType == 2000)
                {
                    _CurrWorldMap[i]._unitTileObject.GetComponent<SpriteRenderer>().sprite = _TileType2000[_CurrWorldMap[i]._unitTileObject._CurrUnitTile._TileUniqueNum];
                }
                else if (_CurrWorldMap[i]._TileType == 2100)
                {
                    _CurrWorldMap[i]._unitTileObject.GetComponent<SpriteRenderer>().sprite = _TileType2100[_CurrWorldMap[i]._unitTileObject._CurrUnitTile._TileUniqueNum];
                }
                else if (_CurrWorldMap[i]._TileType == 3000)
                {
                    _CurrWorldMap[i]._unitTileObject.GetComponent<SpriteRenderer>().sprite = _TileType3000[_CurrWorldMap[i]._unitTileObject._CurrUnitTile._TileUniqueNum];
                }
                else if (_CurrWorldMap[i]._TileType == 3100)
                {
                    _CurrWorldMap[i]._unitTileObject.GetComponent<SpriteRenderer>().sprite = _TileType3100[_CurrWorldMap[i]._unitTileObject._CurrUnitTile._TileUniqueNum];
                }
                else if (_CurrWorldMap[i]._TileType == 4000)
                {
                    _CurrWorldMap[i]._unitTileObject.GetComponent<SpriteRenderer>().sprite = _TileType4000[_CurrWorldMap[i]._unitTileObject._CurrUnitTile._TileUniqueNum];
                }
                else if (_CurrWorldMap[i]._TileType == 0)
                {
                    _CurrWorldMap[i]._unitTileObject.GetComponent<SpriteRenderer>().sprite = _TileType0000[_CurrWorldMap[i]._unitTileObject._CurrUnitTile._TileUniqueNum];

                }
            }
        }
    }


    public void CheckReleasingTiles()  // 드래그를 놓아줄 때 나오는 것.
    {
        //먼저 세개가 달성될 경우, 합쳐준다. 지금은 비용을 청구하지 말고 해보자.
        if(_SelectedTileList.Count == 3)
        {
            _SelectedTileList[0]._IsDiscovered = false;
            _SelectedTileList[1]._IsDiscovered = false;
            if(_SelectedTileList[2]._TileType == 1000)
            {
                _SelectedTileList[2]._TileType = 1100;

            }
            else if (_SelectedTileList[2]._TileType == 2000)
            {
                _SelectedTileList[2]._TileType = 2100;

            }
            else if (_SelectedTileList[2]._TileType == 3000)
            {
                _SelectedTileList[2]._TileType = 3100;

            }
            ReDrawOnlyOneLand(_SelectedTileList[2]); //마지막 타일만 새로 그려주자.
            _SelectedTileList[0]._TileGrade = 0;
            _SelectedTileList[1]._TileGrade = 0;
            _SelectedTileList[2]._TileGrade++;


            _SelectedTileList[0]._unitTileObject.DisplayTileGradeText();
            _SelectedTileList[1]._unitTileObject.DisplayTileGradeText();
            _SelectedTileList[2]._unitTileObject.DisplayTileGradeText();

            if (_PlayerProductingTileList.Contains(_SelectedTileList[0]))  //전체 생산 타일에서 이 타일을 빼준다.
            {
                _PlayerProductingTileList.Remove(_SelectedTileList[0]);
            }
            if (_PlayerProductingTileList.Contains(_SelectedTileList[1]))  //전체 생산 타일에서 이 타일을 빼준다.
            {
                _PlayerProductingTileList.Remove(_SelectedTileList[1]);
            }
            if (!_PlayerProductingTileList.Contains(_SelectedTileList[2]))  //전체 생산 타일에 이걸 넣어준다. 단, 없을 경우에만.. 
            {
                _PlayerProductingTileList.Add(_SelectedTileList[2]);
            }

            CheckTotalPoducting(); //생산량 재계산 해주자.
            GivePlayerResources(); //유저에게 현재 생산량에 맞는 자원을 지급하자.
            ReDrawOnlyDarkLands();  //어두운 부분의 타일을 다시 배정해준다.
            DrawFogTile(false);//타일 포그 상태를 다시 그려준다.
        }

        
        for(int i=0; i<_CommandTiles.Count; i++)
        {
            _CommandTiles[i]._IsSelected = false;
            _CommandTiles[i].SetColorByIsSelected();

        }
        _SelectedTileList.Clear();
        

    }

    public void CheckTotalPoducting()
    {
        int tempGoldP=0;
        int tempWoodP=0;
        int tempFoodP=0;

        for (int i = 0; i < _CurrWorldMap.Count; i++)
        {
            if (_CurrWorldMap[i]._TileGrade != 0)
            {
                if (_CurrWorldMap[i]._TileType == 1100) //나무
                {
                    tempWoodP += _CurrWorldMap[i]._TileGrade;
                }
                else if (_CurrWorldMap[i]._TileType == 2100)  //금
                {
                    tempGoldP += _CurrWorldMap[i]._TileGrade;
                }
                else if (_CurrWorldMap[i]._TileType == 3100)  //푸드
                {
                    tempFoodP += _CurrWorldMap[i]._TileGrade;

                }
            }
        }

        _UnitProductGold = tempGoldP;
        _UnitProductWood = tempWoodP;
        _UnitProductFood = tempFoodP;


    }
    
    public void GivePlayerResources()
    {
        _PlayerFood += _UnitProductFood;
        _PlayerGold += _UnitProductGold;
        _PlayerWood += _UnitProductWood;
    }
    
    //관리기능

    public void RetryGame()
    {
        Debug.Log("tabed");
        SceneManager.LoadScene(0);
    }

    public void CameraMoveRight()
    {
        StartCoroutine(MoveCameraSmoothly(new Vector3(1, 0, 0) * 0.3f));
    }

    public void CameraMoveLeft()
    {
        StartCoroutine(MoveCameraSmoothly(new Vector3(-1, 0, 0) * 0.3f));

    }

    public void CameraMoveUp()
    {
        StartCoroutine(MoveCameraSmoothly(new Vector3(0, 1, 0) * 0.3f));
    }

    public void CameraMoveDown()
    {
        StartCoroutine(MoveCameraSmoothly(new Vector3(0, -1, 0) * 0.3f));
    }

    public IEnumerator MoveCameraSmoothly(Vector3 _transVec)
    {
        for(int i=0; i< 30; i++)
        {
            yield return new WaitForEndOfFrame();
            _TouchInputManager._MainCamera.transform.Translate(_transVec);

        }
    }



}
