﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class UnitCompanay
{
    public int _Num;
    public string _Name;
    public string _IconIMGName;

    public int _Group; //동맹 여부
    public int _Level;  //컴퍼니 레벨

    public Dictionary<UnitStarter, int> _StartersInCompanyDic;  // 스타터, 투자금액
    public int _TotalStockAmount; //컴퍼니의 총 자본 금액

    public Dictionary<UnitTown, int> _StockPerTownDic; //각 마을별 투자된 금액
    public Dictionary<UnitTown, int> _TaxResidualRevenuePerTownDic; //각 소유 마을별 현재 수익

    public int _TotalDividens; //총 쌓인 배당금액
   
    public UnitCompanay()
    {

    }

    public UnitCompanay(int _Num, string _Name, string _IconIMGName)
    {
        this._Num = _Num;
        this._Name = _Name;
        this._IconIMGName = _IconIMGName;

        _StartersInCompanyDic = new Dictionary<UnitStarter, int>();
        _StockPerTownDic = new Dictionary<UnitTown, int>();
        _TaxResidualRevenuePerTownDic = new Dictionary<UnitTown, int>();

    }

}

public class CompanyManager : MonoBehaviour {

    WorldManager _WorldManager;

    public List<UnitCompanay> _ProtoCompanyList;  //원형 컴파니 리스트. 여기서 픽을 해서 현재 컴파니 리스트를 만들어준다.
    public List<UnitCompanay> _CurrCompanyList;   //현재 컴파니 리스트

    
    public int[] _CompanyVarExpTable; //누적 경험치 테이블 차액분
    public int[] _CompanyExpTable; //누적 경험치 테이블


    void Start () {
        _WorldManager = GetComponent<WorldManager>();

        InputProtoCompanyList();
        InstantiateCurrCompanyList();
        InputCompanyExpTable();

    }
	
    void InputProtoCompanyList()
    {
        _ProtoCompanyList = new List<UnitCompanay>();
        
        _ProtoCompanyList.Add(new UnitCompanay(3000001, "토착민", ""));
        _ProtoCompanyList.Add(new UnitCompanay(3000002, "스페인", ""));
        _ProtoCompanyList.Add(new UnitCompanay(3000003, "잉글랜드", ""));
        _ProtoCompanyList.Add(new UnitCompanay(3000004, "스웨덴", ""));
        _ProtoCompanyList.Add(new UnitCompanay(3000005, "한국", ""));
        _ProtoCompanyList.Add(new UnitCompanay(3000006, "미국", ""));
        _ProtoCompanyList.Add(new UnitCompanay(3000006, "베트남", ""));

    }

    void InstantiateCurrCompanyList()  //매 게임마다 판도는 달라진다. 
    {
        _CurrCompanyList = new List<UnitCompanay>();
        int tempNum;

        for (int i = 0; i < 3; i++)
        {
            if (i == 0) {
                tempNum = 0;

            }
            else
            {
                tempNum = (int)Random.Range(1, _ProtoCompanyList.Count);

            }
            UnitCompanay tempCompany= _ProtoCompanyList[tempNum];
            _ProtoCompanyList.RemoveAt(tempNum);

            tempCompany._Level = 1;
            _CurrCompanyList.Add(tempCompany);

        }
    }

    void InputCompanyExpTable()  //타운 경험치 테이블 추가
    {
        _CompanyVarExpTable = new int[30];
        _CompanyVarExpTable[0] = 0;
        _CompanyVarExpTable[1] = 5000;

        _CompanyExpTable = new int[30];
        _CompanyExpTable[0] = 0;
        _CompanyExpTable[1] = 7000;

        for (int i = 2; i < 30; i++)
        {
            _CompanyVarExpTable[i] = (int)(_CompanyVarExpTable[i - 1] * 1.2f);
            _CompanyExpTable[i] = (int)(_CompanyExpTable[i - 1] + _CompanyVarExpTable[i]);

        }
    }

    public void LevelDeciderForCompany(UnitCompanay _UnitCompany)   //총 투자금액과 레벨 테이블을 가지고 타운의 레벨을 결정해주는 메소드.
    {        
        int tempLevel = 1;

        for (int i = 1; i < _CompanyExpTable.Length; i++)
        {
            if (_UnitCompany._TotalStockAmount >= _CompanyExpTable[i])
            {
                tempLevel = i + 1;
            }
        }

        if (_UnitCompany._TotalStockAmount < tempLevel)
        {
            for (int i = 0; i < tempLevel - _UnitCompany._Level; i++)
            {
                //레벨업 시 처리

            }
        }
        _UnitCompany._Level = tempLevel;
    }


    public void AddStarterToCompany(UnitCompanay _UnitCompany, UnitStarter _UnitStarter, int _InvestAmount)  //현재 컴퍼니에 스타터의 지분을 더한다.
    {
        if (_UnitCompany._StartersInCompanyDic.Keys.Contains(_UnitStarter))
        {
            _UnitCompany._StartersInCompanyDic[_UnitStarter] += _InvestAmount;
        }
        else
        {
            _UnitCompany._StartersInCompanyDic.Add(_UnitStarter, _InvestAmount);

        }

        _UnitCompany._TotalStockAmount = 0;  //이왕 하는 김에, 총 투자금액을 계산해준다. 
        for(int i=0; i < _UnitCompany._StartersInCompanyDic.Values.Count; i++)
        {
            _UnitCompany._TotalStockAmount += _UnitCompany._StartersInCompanyDic.Values.ToList()[i];
        }

        LevelDeciderForCompany(_UnitCompany);
    }


    public void InsertInvestAmountToStockPerTownDic(UnitCompanay _UnitCompany, UnitTown _UnitTown, int _InvestAmount)
    {
        if (_UnitCompany._StockPerTownDic.Keys.Contains(_UnitTown))
        {
            _UnitCompany._StockPerTownDic[_UnitTown] += _InvestAmount;
        }
        else
        {
            _UnitCompany._StockPerTownDic.Add(_UnitTown, _InvestAmount);
        }
    }


    public void InsertTaxDividendToEachTownDic(UnitCompanay _UnitCompany, UnitTown _UnitTown, int _DividendsAmount)   //총배당
    {
        if (_UnitCompany._TaxResidualRevenuePerTownDic.Keys.Contains(_UnitTown))
        {
            _UnitCompany._TaxResidualRevenuePerTownDic[_UnitTown] += _DividendsAmount;
        }
        else
        {
            _UnitCompany._TaxResidualRevenuePerTownDic.Add(_UnitTown, _DividendsAmount);
        }

        _UnitCompany._TotalDividens = 0;  //이왕 하는 김에, 총 쌓인 배당액을 계산해준다. 
        for (int i = 0; i < _UnitCompany._TaxResidualRevenuePerTownDic.Values.Count; i++)
        {
            _UnitCompany._TotalDividens += _UnitCompany._TaxResidualRevenuePerTownDic.Values.ToList()[i];
        }

        //이왕 하는 김에 해당 마을에 쌓인 배당액도 계산해준다.
        _UnitTown._TotalDividendAmountInTown = _UnitCompany._TaxResidualRevenuePerTownDic[_UnitTown];
        

    }

    public void DoDivideInPlayerCompany()
    {
        DoDivide(_WorldManager._PlayerUnitStarter._CurrCompany);

        _WorldManager._UICompanyInfoWindow.RefreshDisplay();

    }

    public void DoDivide(UnitCompanay _UnitCompany )
    {
        for (int i = 0; i < _UnitCompany._StartersInCompanyDic.Keys.Count; i++)  //순위 표기해준다.
        {
            _UnitCompany._StartersInCompanyDic.Keys.ToList()[i]._Gold += _UnitCompany._TotalDividens * _UnitCompany._StartersInCompanyDic.Values.ToList()[i] / _UnitCompany._TotalStockAmount;
            
            
        }
        _UnitCompany._TotalDividens = 0;
        _UnitCompany._TaxResidualRevenuePerTownDic.Clear();

    }



}
