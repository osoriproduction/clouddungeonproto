﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using HeroEditor.Common;
using HeroEditor.Common.Enums;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Assets.HeroEditor.Common.CharacterScripts;



public class UnitPerson   //성별, 클래스, 등급, 외모, 고유 스킬 등에 대한 정보를 들고 있고, 이게 먼저 교배가 된 후에, gene 이 교배가 됩니다.
{
    public int _num;
    public int _gender;
    public string _name;
    public string _prefabName;
    public int _classType;
    public int _grade;

    public int _StatSTR;
    public int _StatCON;
    public int _StatDEX;
    public int _StatINT;
    public int _StatWIS;
    public int _StatSENSE;

    public UnitPerson()
    {
    }

    public UnitPerson(int _num, int _gender, int _grade, int _classType, int _StatSTR, int _StatCON, int _StatDEX, int _StatINT, int _StatWIS, int _StatSENSE, string _name, string _prefabName)
    {
        this._num = _num;
        this._gender = _gender;
        this._grade = _grade;
        this._classType = _classType;

        this._StatSTR = _StatSTR;
        this._StatCON = _StatCON;
        this._StatDEX = _StatDEX;
        this._StatINT = _StatINT;
        this._StatWIS = _StatWIS;
        this._StatSENSE = _StatSENSE;

        this._name = _name;
        this._prefabName = _prefabName;
    }
}



public class StarterManager : MonoBehaviour
{

    //외부 참조
    WorldManager _WorldManager;
    public GameObject _MonsterRoot;
    public UnitStarter _StarterPrefab;

    //내부 
    public int[] _VarExpTable; //누적 경험치 테이블 차액분
    public int[] _ExpTable; //누적 경험치 테이블
    public List<string> _StarterNamePool; // 이름들 풀
    public List<UnitPerson> _UnitPersonList;  // 모든 외모 정보 원형 리스트

    public List<UnitStarter> _CurrStartersList;  //스타터들 리스트

    void Start()
    {
        _WorldManager = GetComponent<WorldManager>();
        _CurrStartersList = new List<UnitStarter>();

        InputStarterNames();
        InputExpTable();

      // StartCoroutine(MonsterGenerator());
    }



    public IEnumerator MonsterGenerator()
    {
        InputUnitPersonList();
        int _Tick = 0;

        yield return new WaitForSeconds(0.1f);
        for (; ; )
        {

            for (int i = 0; i < 8; i++)
            {
               SummonMonster(31101, (int)Random.Range(1, 3+_Tick*2), _WorldManager._CurrWorldMap[Random.Range(0, _WorldManager._CurrWorldMap.Count)]);

            }
            SummonMonster(32101, (int)Random.Range(1, 3 + _Tick*2), _WorldManager._CurrWorldMap[Random.Range(0, _WorldManager._CurrWorldMap.Count)]);
            SummonMonster(32101, (int)Random.Range(1, 3 + _Tick*2), _WorldManager._CurrWorldMap[Random.Range(0, _WorldManager._CurrWorldMap.Count)]);
            SummonMonster(33101, (int)Random.Range(1, 3 + _Tick*2), _WorldManager._CurrWorldMap[Random.Range(0, _WorldManager._CurrWorldMap.Count)]);

            yield return new WaitForSeconds(60);
            if (Random.Range(0, 100) < 30)
            {
                _Tick++;
            }
        }

    }



    //이름 목록
    void InputStarterNames()
    {
        _StarterNamePool = new List<string>();

        _StarterNamePool.Add("리딘");
        _StarterNamePool.Add("로슬록");
        _StarterNamePool.Add("크라운");
        _StarterNamePool.Add("루터");
        _StarterNamePool.Add("지랄");
        _StarterNamePool.Add("감기");
        _StarterNamePool.Add("무욕");
        _StarterNamePool.Add("과욕");
        _StarterNamePool.Add("진딩이");
        _StarterNamePool.Add("스타커피");
        _StarterNamePool.Add("파리먹");
        _StarterNamePool.Add("금별이");
        _StarterNamePool.Add("드라마");
        _StarterNamePool.Add("꿈이");
        _StarterNamePool.Add("환상");
        _StarterNamePool.Add("죄악");
        _StarterNamePool.Add("개똥이");
        _StarterNamePool.Add("리욘");
        _StarterNamePool.Add("프라우");
        _StarterNamePool.Add("바리안");
        _StarterNamePool.Add("제이나");
        _StarterNamePool.Add("드라군");
        _StarterNamePool.Add("제라툴");
        _StarterNamePool.Add("스랄");
        _StarterNamePool.Add("둔가");
        _StarterNamePool.Add("포스드");
        _StarterNamePool.Add("지존");
    }


    void InputUnitPersonList()
    {
        _UnitPersonList = new List<UnitPerson>();
        _UnitPersonList.Add(new UnitPerson(11101, 1, 1, 1, 6, 9, 6, 10, 3, 12, "블라", "M1FighterBla"));
        _UnitPersonList.Add(new UnitPerson(11102, 1, 1, 1, 6, 12, 6, 12, 6, 11, "루터", "M1FighterLuther"));
        _UnitPersonList.Add(new UnitPerson(11201, 1, 1, 2, 11, 8, 13, 11, 6, 9, "딜튼", "M1ArcherDilton"));
        _UnitPersonList.Add(new UnitPerson(11401, 1, 1, 4, 12, 3, 6, 13, 13, 13, "룬", "M1HealerLune"));
        _UnitPersonList.Add(new UnitPerson(12101, 1, 2, 1, 11, 15, 6, 4, 7, 6, "가더", "M2FighterGuarder"));
        _UnitPersonList.Add(new UnitPerson(12401, 1, 2, 4, 10, 7, 9, 13, 7, 15, "도노반", "M2HealerDonoban"));
        _UnitPersonList.Add(new UnitPerson(13301, 1, 3, 3, 13, 10, 16, 17, 12, 15, "제피로스", "M3MageJepiros"));
        _UnitPersonList.Add(new UnitPerson(14501, 1, 4, 5, 13, 18, 10, 8, 14, 10, "단", "M4AssassinDan"));
        _UnitPersonList.Add(new UnitPerson(15101, 1, 5, 1, 10, 20, 20, 13, 12, 11, "산", "M5FighterSan"));
        _UnitPersonList.Add(new UnitPerson(23201, 2, 3, 2, 13, 16, 17, 12, 9, 7, "니나", "F3ArcherNina"));
        _UnitPersonList.Add(new UnitPerson(31101, 3, 1, 1, 2, 2, 11, 7, 8, 13, "도적", "M3MageHadon"));
        _UnitPersonList.Add(new UnitPerson(32101, 3, 2, 1, 13, 14, 9, 14, 7, 7, "도적", "M3MageHadon"));
        _UnitPersonList.Add(new UnitPerson(33101, 3, 3, 1, 8, 7, 10, 10, 16, 9, "도적", "M3MageHadon"));





    }


    void InputExpTable()
    {
        _VarExpTable = new int[30];
        _VarExpTable[0] = 0;
        _VarExpTable[1] = 500;

        _ExpTable = new int[30];
        _ExpTable[0] = 0;
        _ExpTable[1] = 500;

        for (int i = 2; i < 30; i++)
        {
            _VarExpTable[i] = (int)(_VarExpTable[i - 1] * 1.2f);
            _ExpTable[i] = (int)(_ExpTable[i - 1] + _VarExpTable[i]);

        }
    }

    //레벨업    ctrl + K + U 해제
    public void LevelDeciderForTrade(UnitStarter _UnitStarter)
    {
        int tempLevel = 1;

        for (int i = 1; i < _ExpTable.Length; i++)
        {
            if (_UnitStarter._CurrTradeExp >= _ExpTable[i])
            {
                tempLevel = i + 1;
            }
        }

        if (_UnitStarter._TradeLevel < tempLevel)
        {
            for (int i = 0; i < tempLevel - _UnitStarter._TradeLevel; i++)
            {
                //레벨업 시 처리
                float dx = Random.Range(1.1f, 1.15f);

                _UnitStarter._MaxCarryWeight = (int)(_UnitStarter._MaxCarryWeight * dx);
                _UnitStarter._maxMP = (int)(_UnitStarter._maxMP * dx);
                // _UnitStarter._Gold += 1000;

            }
        }
        _UnitStarter._TradeLevel = tempLevel;
        _UnitStarter.DecideBySTAT();

    }



    //레벨업    ctrl + K + U 해제
    public void LevelDeciderForFight(UnitStarter _UnitStarter)
    {
        int tempLevel = 1;

        for (int i = 1; i < _ExpTable.Length; i++)
        {
            if (_UnitStarter._CurrWarExp >= _ExpTable[i])
            {
                tempLevel = i + 1;
            }
        }

        if (_UnitStarter._WarLevel < tempLevel)
        {
            for (int i = 0; i < tempLevel - _UnitStarter._WarLevel; i++)
            {
                //레벨업 시 처리
                float dx = Random.Range(1.1f, 1.15f);

                _UnitStarter._maxMP = (int)(_UnitStarter._maxMP * dx);
                _UnitStarter._maxHP = (int)(_UnitStarter._maxHP * dx);
                if (_UnitStarter._currentMP < _UnitStarter._maxMP*0.3f)
                {
                    _UnitStarter._currentMP = _UnitStarter._maxMP * 0.3f;
                }

            }

           
        }
        _UnitStarter._WarLevel = tempLevel;
        _UnitStarter.DecideBySTAT();
    }


    //스타터나 몬스터들 소환 메소드

    UnitPerson FindUnitPerson(int _num)
    {
        UnitPerson _return = null;

        for (int i = 0; i < _UnitPersonList.Count; i++)
        {
            if (_UnitPersonList[i]._num == _num)
            {
                _return = _UnitPersonList[i];
                break;
            }
        }
        return _return;
    }

    const int _MaxSummonedStarter = 25;
    public void SummonMonster(int _num, int _WarLevel, UnitTile _TargetTile)
    {
        if (_CurrStartersList.Count <= _MaxSummonedStarter)
        {
            UnitTile _InstantiatingTile = null;
            for (int i = 0; i < _WorldManager._CurrWorldMap.Count; i++)
            {
                if (_WorldManager._CurrWorldMap[i] == _TargetTile)
                {
                    _InstantiatingTile = _WorldManager._CurrWorldMap[i];
                }
            }

            UnitPerson _person = FindUnitPerson(_num);
            UnitStarter _return = (UnitStarter)Instantiate(_StarterPrefab, _MonsterRoot.transform);

            Character _returnCharacter = (Character)Instantiate(Resources.Load("Prefabs/Characters/" + _person._prefabName, typeof(Character)), _return.transform);
            _returnCharacter.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            _returnCharacter.transform.localPosition = new Vector3(0, -50, 0);



            _return._UnitPerson = _person;
            _return._IsMonster = true;  //몬스터니까 이거 true로 틀어줘야 한다.

            _return.transform.position = _InstantiatingTile._unitTileObject.transform.position;
            _return._Name = _person._name;  //나머지 정보는 모두 unitperson이 들고 있는 걸로..

            _return._WarLevel = _WarLevel;
            _return._CurrWarExp = _ExpTable[_WarLevel - 1];

            _return._StatSTR = _person._StatSTR;
            _return._StatCON = _person._StatCON;
            _return._StatWIS = _person._StatWIS;
            _return._StatDEX = _person._StatDEX;
            _return._StatINT = _person._StatINT;
            _return._StatSENSE = _person._StatSENSE;


            _return._Gold = (int)(Random.Range(5, 8)) * 20 * _WarLevel;

            LevelDeciderForFight(_return);

            _return._ATKCoolTime = 2;

        }
        //Debug.Log("_cuirrstarerOList count is " + _CurrStartersList.Count);

    }



    //근처 적이나 스타터들 찾는 로직.
    public List<UnitStarter> FindNearStarters(UnitStarter _ReferStarter, float _ReferDistR, bool FindOnlyEnemy)  
    {
        List<UnitStarter> _returnList = new List<UnitStarter>();
        
        for (int i = 0; i < _CurrStartersList.Count; i++)
        {          
            if (DistBetweenPointR(_ReferStarter.transform.position, _CurrStartersList[i].transform.position) < _ReferDistR)
            {
                if (_CurrStartersList[i] != _ReferStarter)
                {
                    if (FindOnlyEnemy)  //적만 찾는 로직
                    {
                        if (_ReferStarter._IsMonster)  //몬스터일 경우
                        {
                            if (!_CurrStartersList[i]._IsMonster && _CurrStartersList[i]._CurrUnitTown==null)  //몬스터끼리는 안싸운다. 마을에 있는 사람은 놔준다.
                            {
                                _returnList.Add(_CurrStartersList[i]);

                            }
                        }
                        else  //일반 스타터일 경우
                        {
                            if (_CurrStartersList[i]._IsMonster || _ReferStarter._CurrHostileStarterList.Contains(_CurrStartersList[i]))   //적대하고 있는 목록에 있을 경우만, 
                            {
                                _returnList.Add(_CurrStartersList[i]);

                            }
                        }
                    }
                    else
                    {
                        _returnList.Add(_CurrStartersList[i]);

                    }
                }
            }
        }
        return _returnList;

    }



    public float DistBetweenPointR(Vector3 _target, Vector3 _refer)
    {
        return (_target.x - _refer.x) * (_target.x - _refer.x) + (_target.y - _refer.y) * (_target.y - _refer.y);

    }

}
