﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using HeroEditor.Common;
using HeroEditor.Common.Enums;
using UnityEngine;
using UnityEngine.SceneManagement;
using Assets.HeroEditor.Common.CharacterScripts;


public class UnitHex
{
    public UnitTileObject _unitTileObject;

    public UnitHex _Parent;

    public int _G;
    public int _H;
    public int _F;

}
public class WayManager : MonoBehaviour {
    
    //외부 오브젝트
     WorldManager _WorldManager;

    //시스템
    public List<List<UnitTileObject>> _AllWaysList;   //모든 길에 대한 정보. 

    void Start()
    {
        _WorldManager = GetComponent<WorldManager>();

        _AllWaysList = new List<List<UnitTileObject>>();

        StartCoroutine(Init());
    }
    

    IEnumerator Init()
    {
        yield return new WaitForSeconds(0.05f);
        CalculateAllWaysBetweenEveryTowns();

    }


    public void CalculateAllWaysBetweenEveryTowns()
    {
        Debug.Log("_WorldManager._CurrTownList.Count is " + _WorldManager._CurrTownList.Count);

        List<UnitTileObject> _tempWayList = new List<UnitTileObject>();

        for(int i=0; i < _WorldManager._CurrTownList.Count; i++)
        {
            for (int j = 0; j < _WorldManager._CurrTownList.Count; j++) {
                if (i != j)
                {
                    _tempWayList = FindWayByAStarAToB(_WorldManager._CurrTownList[i]._currUnitTile , _WorldManager._CurrTownList[j]._currUnitTile);
                    _AllWaysList.Add(_tempWayList);
                }
            }
        }

        Debug.Log("_AllWaysList.Count is " + _AllWaysList.Count);

    }



    // 상세 길찾기 관련 메소드


    public List<UnitHex> _closedHexList;
    public List<UnitHex> _openedHexList;
    public UnitHex _currHex;
    public UnitHex _lastHex;   // 도착 헥스 직전의 헥스다.
    public UnitHex _destinationHex;
    public UnitHex _initiationHex;

    bool IsClosedHexListHaveThisTileObject(UnitTileObject _targetTileObject)
    {
        bool _return = false;

        for (int i = 0; i < _closedHexList.Count; i++)
        {
            if (_closedHexList[i]._unitTileObject == _targetTileObject)
            {
                _return = true;
                break;
            }
        }
        return _return;
    }

    bool IsOpendHexListHaveThisTileObject(UnitTileObject _targetTileObject)
    {
        bool _return = false;

        for (int i = 0; i < _openedHexList.Count; i++)
        {
            if (_openedHexList[i]._unitTileObject == _targetTileObject)
            {
                _return = true;
                break;
            }
        }
        return _return;
    }

    UnitHex GetHexFromObjectTile(UnitTileObject _targetTileObject)
    {
        UnitHex _return = null;

        for (int i = 0; i < _openedHexList.Count; i++)
        {
            if (_openedHexList[i]._unitTileObject == _targetTileObject)
            {
                _return = _openedHexList[i];
                break;
            }
        }
        if (_return == null)
        {
            for (int i = 0; i < _closedHexList.Count; i++)
            {
                if (_closedHexList[i]._unitTileObject == _targetTileObject)
                {
                    _return = _closedHexList[i];
                    break;
                }
            }
        }

        return _return;
    }

    UnitHex GetHexOfCheapestGFromOpenList()
    {
        UnitHex _return = null;
        int _minG = 999999;

        for (int i = 0; i < _openedHexList.Count; i++)
        {
            if (_openedHexList[i]._G < _minG)
            {
                _return = _openedHexList[i];
                _minG = _openedHexList[i]._G;
            }
        }

        return _return;
    }


    public List<UnitTileObject> FindWayByAStarAToB(UnitTile _startUnitTile,  UnitTile _targetUnitTile)
    {
        List<UnitTileObject> _returnWayList = new List<UnitTileObject>(); ;  //이게 결국 최종 길이지 뭐.
        _returnWayList.Add(_startUnitTile._unitTileObject);
            
        List<UnitHex> _returnHexList = new List<UnitHex>();  //이건 헥스 기준.. 이걸로 받은 후, 웨이 리스트로 다시 뿌려줄까?

        _closedHexList = new List<UnitHex>();
        _openedHexList = new List<UnitHex>();
        _currHex = new UnitHex();
        _lastHex = new UnitHex();
        _initiationHex = new UnitHex();
        _destinationHex = new UnitHex();



        //도착 타일 지정
        _destinationHex._unitTileObject = _targetUnitTile._unitTileObject;
        _destinationHex._H = 0;
        _destinationHex._G = 0;
        _destinationHex._F = 0;


        _currHex._unitTileObject = _startUnitTile._unitTileObject;
        _currHex._G = 0;
        _currHex._H = DistBetweenPointHEX(_currHex._unitTileObject, _destinationHex._unitTileObject);
        _closedHexList.Add(_currHex);
        _initiationHex = _currHex;
        


        //맨 처음 한번 열어주기.   

        for (int i = 0; i < _WorldManager._CurrTileObjectList.Count; i++)
        {
            if (DistBetweenPointHEX(_WorldManager._CurrTileObjectList[i], _initiationHex._unitTileObject) == 1 && !_WorldManager._CurrTileObjectList[i]._CurrUnitTile._IsMoveUnAvailable)
            {
                UnitHex _tempHex = new UnitHex();
                _tempHex._unitTileObject = _WorldManager._CurrTileObjectList[i];
                _tempHex._G = 1;
                _tempHex._H = DistBetweenPointHEX(_WorldManager._CurrTileObjectList[i], _destinationHex._unitTileObject);
                _tempHex._F = _tempHex._G + _tempHex._H;
                _tempHex._Parent = _initiationHex;

                _openedHexList.Add(_tempHex);

            }
        }

        //무한 반복 시작
        bool _arrived = false;
        for (int h = 0; h < 99999; h++)
        {
            if (_arrived)  //길 찾음
            {
                _returnHexList.Add(_destinationHex);
                _returnHexList.Add(_lastHex);
                _currHex = _lastHex;

                for (; ; )
                {
                    if (_currHex._Parent == _initiationHex)
                    {
                        break;
                    }
                    _currHex = _currHex._Parent;
                    _returnHexList.Add(_currHex);

                }

                for (int l = _returnHexList.Count; l > 0; l--)  //거꾸로 길을 넣어주는 것.
                {
                    _returnWayList.Add(_returnHexList[l - 1]._unitTileObject);
                }


                break;
            }

            UnitHex _SearchingHex = GetHexOfCheapestGFromOpenList();
            //한 개의 제일 싼 오픈 헥스를 가져와서 주변부를 탐색해준다.
            if (_SearchingHex == null)
            {
                if(_startUnitTile._currUnitTown !=null && _targetUnitTile._currUnitTown != null)  //처음 지점이 타운이고, 도착 지점이 타운인데 길이 없는 경우. 재시작해주자 일단.
                {
                    SceneManager.LoadScene("WorldMap");
                }
                else
                {
                    Debug.Log("no way between " + _startUnitTile._Num + " ~ " + _targetUnitTile._Num);

                }
                break;
            }
            for (int j = 0; j < _WorldManager._CurrTileObjectList.Count; j++)
            {
                if (DistBetweenPointHEX(_WorldManager._CurrTileObjectList[j], _SearchingHex._unitTileObject) == 1 && !_WorldManager._CurrTileObjectList[j]._CurrUnitTile._IsMoveUnAvailable && !IsClosedHexListHaveThisTileObject(_WorldManager._CurrTileObjectList[j]))
                {

                    if (_destinationHex._unitTileObject == _WorldManager._CurrTileObjectList[j]) //도착함. 
                    {
                        _arrived = true;
                        _lastHex = _SearchingHex;
                        break;
                    }
                    else if (!IsOpendHexListHaveThisTileObject(_WorldManager._CurrTileObjectList[j]))  //해당 타일이 오픈에 있지 않은 새로 조사하는 타일이다.
                    {
                        UnitHex _tempHex = new UnitHex();
                        _tempHex._unitTileObject = _WorldManager._CurrTileObjectList[j];
                        _tempHex._Parent = _SearchingHex;
                        _tempHex._G = _SearchingHex._G + 1;
                        _tempHex._H = DistBetweenPointHEX(_WorldManager._CurrTileObjectList[j], _destinationHex._unitTileObject);
                        _tempHex._F = _tempHex._G + _tempHex._H;
                        _openedHexList.Add(_tempHex);
                    }
                    else  //이미한번 조사했던 타일이다. 여기서는 판정 해서 퍼렌트 바꿔줘야 함.
                    {
                        UnitHex _tempHex = GetHexFromObjectTile(_WorldManager._CurrTileObjectList[j]);

                        if (_SearchingHex._G + 1 < _tempHex._G)
                        {
                            _tempHex._Parent = _SearchingHex;
                            _tempHex._G = _SearchingHex._G + 1;
                            _tempHex._H = DistBetweenPointHEX(_WorldManager._CurrTileObjectList[j], _destinationHex._unitTileObject);
                            _tempHex._F = _tempHex._G + _tempHex._H;

                        }
                    }
                }
            }
            _openedHexList.Remove(_SearchingHex);
            _closedHexList.Add(_SearchingHex);

        }
        return _returnWayList;

    }

    int CountParentG(UnitHex _targetHex)
    {
        int _return = 1;
        UnitHex _currHex = _targetHex;

        for (; ; )
        {
            if (_targetHex._Parent != null && _targetHex._Parent != _initiationHex)
            {
                _currHex = _currHex._Parent;
                _return++;
            }
            else
            {
                break;
            }
        }

        return _return;
    }

    public float DistBetweenPointR(Vector3 _target, Vector3 _refer)
    {
        return (_target.x - _refer.x) * (_target.x - _refer.x) + (_target.y - _refer.y) * (_target.y - _refer.y);

    }

    public int DistBetweenPointHEX(UnitTileObject _targetTileObject, UnitTileObject _referTileObject)
    {
        int _return = 0;

        _return = Mathf.Abs((int)((_targetTileObject.transform.localPosition.x - _referTileObject.transform.localPosition.x) * 0.4f)) + Mathf.Abs((int)((_targetTileObject.transform.localPosition.y - _referTileObject.transform.localPosition.y) * 0.5f));

        return _return;

    }



}
