﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using HeroEditor.Common;
using HeroEditor.Common.Enums;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Assets.HeroEditor.Common.CharacterScripts;
using System.Xml;

public class ItemManager : MonoBehaviour {

    //외부참조
    WorldManager _WorldManager;
    public GameObject _ItemObejctRoot;
    public SpriteCollection _SpriteCollection;

    //내부참조
    public GameObject _SpriteEquipmentsRoot; //여기서 장비들을 모두 가져온다.
    public UnitItemObject _UnitItemObjectPrefab;

    //시스템

    public List<Sprite> _EquipmentSpriteList;  //장비 스프라이트 여기로 테이블에 있는것만 로드해주자.
    
    void Start () {
        _WorldManager = GetComponent<WorldManager>();
        StartCoroutine(Init());

        //GetFilesFromSpriteFolder();
        //Write();
    }

    IEnumerator Init()
    {
        yield return new WaitForSeconds(0.1f);
        GetAllSpriteFromFolder();

        /*  골드랑 아이템들을 생성해주는 로직인데, 일단 비활성화.
        for (int i=0; i < 50; i++)
        {
            int _tempRandNum = (int)Random.Range(0, _WorldManager._CurrWorldMap.Count);
            if (!_WorldManager._CurrWorldMap[_tempRandNum]._IsMoveUnAvailable)
            {
                if (Random.Range(0, 100) < 90) {
                    SummonCoin((int)Random.Range(7, 20) * 10, _WorldManager._CurrWorldMap[_tempRandNum]._unitTileObject);
                }
                else
                {
                    SummonGoods(_WorldManager._EconomyManager._ProtoGoodsList[(int)Random.Range(0, _WorldManager._EconomyManager._ProtoGoodsList.Count)], _WorldManager._CurrWorldMap[_tempRandNum]._unitTileObject);

                }
            }

        }*/

    }

    void GetAllSpriteFromFolder()
    {

       List<UnitGoods> _referedProtoGoodsList = _WorldManager._EconomyManager._ProtoGoodsList;

        for(int i=0; i < _referedProtoGoodsList.Count; i++)
        {
            //Debug.Log("_referedProtoGoodsList  " + i + " will get " + _referedProtoGoodsList[i]._IMGName);                  
            

            //_referedProtoGoodsList[i]._GoodsSprite = Resources.Load<Sprite>("Sprites/Equipment/" + _referedProtoGoodsList[i]._IMGName);
            SetSpriteGroupEntryByTypeAndName( _referedProtoGoodsList[i]);
            
            //if (_referedProtoGoodsList[i]._GoodsSprite == null)
            //{
            //    Debug.Log("Failed To Get Sprite");
            //}
            //_EquipmentSpriteList.Add( _referedProtoGoodsList[i]._GoodsSprite);
        }
        
    }


    void SetSpriteGroupEntryByTypeAndName(UnitGoods _UnitGoods)
    {
        for (int i = 0; i < _SpriteCollection.Helmet.Count; i++)
        {
            if (_UnitGoods._IMGName == _SpriteCollection.Helmet[i].Name)
            {
                _UnitGoods._GoodsSpriteGroupEntry = _SpriteCollection.Helmet[i];
                _UnitGoods._EquipmentType = UnitGoods.EquipmentType.Helmet;
               // Debug.Log(_UnitGoods._IMGName + "은 스프라이트컬렉션 검색완료 "+ _UnitGoods._EquipmentType);

            }
        }
        for (int i = 0; i < _SpriteCollection.HelmetMask.Count; i++)
        {
            if (_UnitGoods._IMGName == _SpriteCollection.HelmetMask[i].Name)
            {
                _UnitGoods._SubGoodsSpriteGroupEntry = _SpriteCollection.HelmetMask[i];
                //Debug.Log(_UnitGoods._IMGName + "은 스프라이트컬렉션 검색완료 " + _UnitGoods._EquipmentType);

                return;
            }
        }
        for (int i = 0; i < _SpriteCollection.Glasses.Count; i++)
        {
            if (_UnitGoods._IMGName == _SpriteCollection.Glasses[i].Name)
            {
                _UnitGoods._GoodsSpriteGroupEntry = _SpriteCollection.Glasses[i];
                _UnitGoods._EquipmentType = UnitGoods.EquipmentType.Glasses;
                //Debug.Log(_UnitGoods._IMGName + "은 스프라이트컬렉션 검색완료 " + _UnitGoods._EquipmentType);

                return;
            }
        }

        for (int i = 0; i < _SpriteCollection.Mask.Count; i++)
        {
            if (_UnitGoods._IMGName == _SpriteCollection.Mask[i].Name)
            {
                _UnitGoods._GoodsSpriteGroupEntry = _SpriteCollection.Mask[i];
                _UnitGoods._EquipmentType = UnitGoods.EquipmentType.Mask;
                //Debug.Log(_UnitGoods._IMGName + "은 스프라이트컬렉션 검색완료 " + _UnitGoods._EquipmentType);

                return;
            }
        }

        for (int i = 0; i < _SpriteCollection.Armor.Count; i++)
        {
            if (_UnitGoods._IMGName == _SpriteCollection.Armor[i].Name)
            {
                _UnitGoods._GoodsSpriteGroupEntry = _SpriteCollection.Armor[i];
                _UnitGoods._EquipmentType = UnitGoods.EquipmentType.Armor;
               // Debug.Log(_UnitGoods._IMGName + "은 스프라이트컬렉션 검색완료 " + _UnitGoods._EquipmentType);

                return;
            }
        }

        for (int i = 0; i < _SpriteCollection.Cape.Count; i++)
        {
            if (_UnitGoods._IMGName == _SpriteCollection.Cape[i].Name)
            {
                _UnitGoods._GoodsSpriteGroupEntry = _SpriteCollection.Cape[i];
                _UnitGoods._EquipmentType = UnitGoods.EquipmentType.Cape;
                //Debug.Log(_UnitGoods._IMGName + "은 스프라이트컬렉션 검색완료 " + _UnitGoods._EquipmentType);

                return;
            }
        }

        for (int i = 0; i < _SpriteCollection.Back.Count; i++)
        {
            if (_UnitGoods._IMGName == _SpriteCollection.Back[i].Name)
            {
                _UnitGoods._GoodsSpriteGroupEntry = _SpriteCollection.Back[i];
                _UnitGoods._EquipmentType = UnitGoods.EquipmentType.Back;
              //  Debug.Log(_UnitGoods._IMGName + "은 스프라이트컬렉션 검색완료 " + _UnitGoods._EquipmentType);

                return;
            }
        }

        for (int i = 0; i < _SpriteCollection.MeleeWeapon1H.Count; i++)
        {
            if (_UnitGoods._IMGName == _SpriteCollection.MeleeWeapon1H[i].Name)
            {
                _UnitGoods._GoodsSpriteGroupEntry = _SpriteCollection.MeleeWeapon1H[i];
                _UnitGoods._EquipmentType = UnitGoods.EquipmentType.MeleeWeapon1H;
            //  Debug.Log(_UnitGoods._IMGName + "은 스프라이트컬렉션 검색완료 " + _UnitGoods._EquipmentType);

            }
        }

        for (int i = 0; i < _SpriteCollection.MeleeWeapon2H.Count; i++)
        {
            if (_UnitGoods._IMGName == _SpriteCollection.MeleeWeapon2H[i].Name)
            {
                _UnitGoods._GoodsSpriteGroupEntry = _SpriteCollection.MeleeWeapon2H[i];
                _UnitGoods._EquipmentType = UnitGoods.EquipmentType.MeleeWeapon2H;
             //  Debug.Log(_UnitGoods._IMGName + "은 스프라이트컬렉션 검색완료 " + _UnitGoods._EquipmentType);

            }
        }

        for (int i = 0; i < _SpriteCollection.MeleeWeaponTrail1H.Count; i++)
        {
            if (_UnitGoods._IMGName == _SpriteCollection.MeleeWeaponTrail1H[i].Name)
            {
                _UnitGoods._SubGoodsSpriteGroupEntry = _SpriteCollection.MeleeWeaponTrail1H[i];
             //   Debug.Log(_UnitGoods._IMGName + "은 스프라이트컬렉션 검색완료 " + _UnitGoods._EquipmentType);

                return;
            }
        }

        for (int i = 0; i < _SpriteCollection.MeleeWeaponTrail2H.Count; i++)
        {
            if (_UnitGoods._IMGName == _SpriteCollection.MeleeWeaponTrail2H[i].Name)
            {
                _UnitGoods._SubGoodsSpriteGroupEntry = _SpriteCollection.MeleeWeaponTrail2H[i];
               // Debug.Log(_UnitGoods._IMGName + "은 스프라이트컬렉션 검색완료 " + _UnitGoods._EquipmentType);

                return;
            }
        }

        for (int i = 0; i < _SpriteCollection.Bow.Count; i++)
        {
            if (_UnitGoods._IMGName == _SpriteCollection.Bow[i].Name)
            {
                _UnitGoods._GoodsSpriteGroupEntry = _SpriteCollection.Bow[i];
                _UnitGoods._EquipmentType = UnitGoods.EquipmentType.Bow;
                //Debug.Log(_UnitGoods._IMGName + "은 스프라이트컬렉션 검색완료 " + _UnitGoods._EquipmentType);

                return;
            }
        }

        for (int i = 0; i < _SpriteCollection.Shield.Count; i++)
        {
            if (_UnitGoods._IMGName == _SpriteCollection.Shield[i].Name)
            {
                _UnitGoods._GoodsSpriteGroupEntry = _SpriteCollection.Shield[i];
                _UnitGoods._EquipmentType = UnitGoods.EquipmentType.Shield;

               // Debug.Log(_UnitGoods._IMGName + "은 스프라이트컬렉션 검색완료 " + _UnitGoods._EquipmentType);

                return;
            }
        }

        if (_UnitGoods._GoodsSpriteGroupEntry == null)
        {
            Debug.Log(_UnitGoods._IMGName + "은 스프라이트컬렉션에 없나봐요.");
        }


        //...

        return;
    }









    
    public void SummonCoin(int _CoinGold, UnitTileObject _UnitTileObject)
    {
        UnitItemObject _UnitItemObject = Instantiate(_UnitItemObjectPrefab, _ItemObejctRoot.transform);
        _UnitItemObject.transform.position = _UnitTileObject.transform.position;
        _UnitItemObject.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
        _UnitItemObject.transform.Translate(new Vector3(0, -2.5f, 0));
        _UnitItemObject._IsCoin = true;
        _UnitItemObject._CoinGold = _CoinGold;
    }


    public void SummonGoods(UnitGoods _UnitGood, UnitTileObject _UnitTileObject)
    {
        UnitItemObject _UnitItemObject = Instantiate(_UnitItemObjectPrefab, _ItemObejctRoot.transform);
        _UnitItemObject.transform.position = _UnitTileObject.transform.position;
        _UnitItemObject.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
        _UnitItemObject._SpriteReneder.transform.localScale = new Vector3(0.45f, 0.45f, 0.45f);
        _UnitItemObject.transform.Translate(new Vector3(0, -4.0f, 0));
        _UnitItemObject._IsCoin = false;
        _UnitItemObject._UnitGood = _UnitGood;
        if (_UnitGood._EquipmentType == UnitGoods.EquipmentType.MeleeWeapon1H)
        {
            _UnitItemObject._SpriteReneder.sprite = _UnitGood._GoodsSpriteGroupEntry.Sprite;
        }

        _UnitItemObject._SpriteReneder.sortingOrder = 100;  
    }



    //---------------------------번외-----------------------------------------------------

    public List<string> _EquipmentFileNames;
    public void GetFilesFromSpriteFolder()
    {
        _EquipmentFileNames = new List<string>();
        string _tempString;

        string dirPath = @"C:\Users\박상현\Documents\#OSORIpd\#AccelRPG\AccelRPG\Assets\Resources\Sprites\Equipment";
        if (System.IO.Directory.Exists(dirPath))
        {
            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(dirPath);
            foreach(var item in di.GetFiles())
            {
                _tempString = item.Name;
                if (!_tempString.Contains(".meta")  && !_tempString.Contains(".ini"))
                {
                    if (_tempString.Contains(".png"))
                    {
                       // Debug.Log("Name:" + _tempString + "  Length:" + _tempString.Length);
                        //_tempString.Remove(_tempString.Length - 3, 3);
                        _tempString=  _tempString.Replace(".png", "");
                        //Debug.Log("NameAfterRemoval:" + _tempString + "  Length:" + _tempString.Length);

                        _EquipmentFileNames.Add(_tempString);
                    }
                }
            }
        }


    }


    public void Write()
    {
        string _filePath = @"C:\Users\박상현\Documents\#OSORIpd\#AccelRPG\EquipmentNameList.Xml";

        XmlDocument _Document = new XmlDocument();
        XmlElement _ItemListElement = _Document.CreateElement("First");
        _Document.AppendChild(_ItemListElement);

        for(int i=0; i< _EquipmentFileNames.Count;i++)
        {
            XmlElement _NameElement = _Document.CreateElement("Euipment");
            _NameElement.SetAttribute("Number", i+"");
            _NameElement.SetAttribute("Name", _EquipmentFileNames[i]);
            _ItemListElement.AppendChild(_NameElement);
            _Document.AppendChild(_ItemListElement);

        }
        _Document.Save(_filePath);

    }

}
