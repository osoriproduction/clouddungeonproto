﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPubInfoWindow : MonoBehaviour {

    WorldManager _WorldManager;
    public GameObject _StartersObjectRoot;

    public UnitTown _OpendUnitTown;

    public List<StarterObjectInPubPrefab> _InPubStarterList;
    public StarterObjectInPubPrefab _StarterObjectInPubPrefab;


    public void KickStart()
    {
        _WorldManager = GetComponentInParent<WorldManager>();
        _InPubStarterList = new List<StarterObjectInPubPrefab>();

        _OpendUnitTown = _WorldManager._PlayerUnitStarter._CurrUnitTown;
        for (int i=0; i < _WorldManager._StarterManager._CurrStartersList.Count; i++)
        {
            if(_WorldManager._StarterManager._CurrStartersList[i]._CurrUnitTown == _OpendUnitTown  && _WorldManager._StarterManager._CurrStartersList[i] !=_WorldManager._PlayerUnitStarter)
            {
                StarterObjectInPubPrefab _tempObject = Instantiate(_StarterObjectInPubPrefab, _StartersObjectRoot.transform);
                _tempObject.transform.localPosition = new Vector3(0, -100, 0);
                _tempObject._Character = Instantiate(_WorldManager._StarterManager._CurrStartersList[i]._Character, _tempObject.transform);
                _tempObject._UnitStarter = _WorldManager._StarterManager._CurrStartersList[i];
                _InPubStarterList.Add(_tempObject);

            }
        }
        if (SlowUpdateRef != null)
        {
            StopCoroutine(SlowUpdateRef);
        }
        SlowUpdateRef = SlowUpdate();
        StartCoroutine(SlowUpdateRef);
    }

    public void ClearPub()
    {
        for (int i = 0; i < _InPubStarterList.Count; i++)
        {
            Destroy(_InPubStarterList[i].gameObject);

        }
        _InPubStarterList.Clear();
    }


    //루틴으로 체크해서 나간애들 지워주는 로직
    IEnumerator SlowUpdateRef;
    IEnumerator SlowUpdate()
    {
        WaitForSeconds _fWait = new WaitForSeconds(0.2f);
        StarterObjectInPubPrefab _tempPrefab;
        UnitTown _playerTown = _WorldManager._PlayerUnitStarter._CurrUnitTown;

        for (; ; )
        {
            yield return _fWait;
            for (int i = 0; i < _InPubStarterList.Count; i++)
            {
                if (_playerTown != _InPubStarterList[i]._UnitStarter._CurrUnitTown)
                {
                    _tempPrefab = _InPubStarterList[i];
                    _InPubStarterList.Remove(_InPubStarterList[i]);
                    Destroy(_tempPrefab.gameObject);
                }
            }
        }
    }
}
