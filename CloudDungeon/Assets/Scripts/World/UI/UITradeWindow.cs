﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;


public class UITradeWindow : MonoBehaviour {

    //외부 참조
    WorldManager _WorldManager;
    public UnitGoodsObject _UnitGoodsObjectPrefab; //로딩될 상품 오브젝트들

    public RectTransform _ContentRootSell;   //판매영역 컨텐츠 루트
    public RectTransform _ContentRootInventory;    //인벤영역 컨텐츠 루트
    public GameObject _ContentCheckedGoodsInfo;  //선택된 아이템 정보
    public Text _ItemNameText;
    public Text _PriceText;
    public Text _PriceSubText;
    public Text _WeightText;
    public Text _ErrorMsgText;
    public Image _CheckedItemIMG;

    public Text _TotalWeightText;
    public Text _MaxWeightText;
    public Text _GoldText;
    public Text _SellTownText; //어디서 제일 비싸게 팔리나 힌트.

    public AnimatedButton _PurchaseBTN;
    public AnimatedButton _SellBTN;
 
    //시스템
    public UnitGoodsObject _CurrCheckedGoods;
    public List<UnitGoodsObject> _DrawedGoods;
    public UnitGoods _RememberedGoods; //현재 체크된 오브젝트의 굿즈를 기억해준다.

    public bool _IsBuyingMode; //기본은 인벤토리 판매 모드이다.

    public float _CompanyDiscountRate;

    UnitTown _CheckedUnitTown;  //현재 타운을 참조해준다.
    float _PriceAdjustor;  //현재 가격 조정자


    private void Update()
    {
        if (_WorldManager._IsGlobalPause)
        {
            return;
        }
        _GoldText.text = _WorldManager._PlayerUnitStarter._Gold + "";

        if (_CurrCheckedGoods == null)
        {
            _PurchaseBTN.gameObject.SetActive(false);
            _SellBTN.gameObject.SetActive(false);
        }
        else
        {
            if (_IsBuyingMode)
            {
                _PurchaseBTN.gameObject.SetActive(true);
                _SellBTN.gameObject.SetActive(false);

            }
            else
            {
                _PurchaseBTN.gameObject.SetActive(false);
                _SellBTN.gameObject.SetActive(true);

            }
        }
    }

    public void KickStart()
    {
        if (_WorldManager == null)
            _WorldManager = GetComponentInParent<WorldManager>();
        _ContentCheckedGoodsInfo.SetActive(false);

        _WorldManager._PlayerUnitStarter.FindMyLocatedTileObject();
        ResetGoodsObjects();
        DrawSellSideGoodsList();
        DrawInventorySideGoodsList();

        _CompanyDiscountRate = 0;
        if (_WorldManager._PlayerUnitStarter._CurrCompany != null)  // 회사 독점 시 이득
        {
            if(_WorldManager._PlayerUnitStarter._CurrCompany == _WorldManager._PlayerUnitStarter._CurrUnitTown._MonopolyCompany)
            {
                _CompanyDiscountRate = _WorldManager._PlayerUnitStarter._CurrUnitTown._TaxRate;
            }
        }

        _CheckedUnitTown = _WorldManager._PlayerUnitStarter._CurrUnitTown;

    }

    public void DrawCheckedGoodsInfo()
    {
        if (_CurrCheckedGoods == null)
        {
            _ContentCheckedGoodsInfo.SetActive(false);
        }
        else
        {
            _ContentCheckedGoodsInfo.SetActive(true);

            _ItemNameText.text = _CurrCheckedGoods._UnitGoods._Name;
                
            if (!_IsBuyingMode)  //팔때. 팔때는 세금을 안붙힌다. 살때만... 부가가치세처럼 매기자.
            {
                _PriceAdjustor = 1;

                if (_CheckedUnitTown._TownRegularlyGoods.Contains(_CurrCheckedGoods._UnitGoods))
                {
                    _PriceAdjustor = 0.5f;
                }

                _PriceText.text = (int)(_PriceAdjustor* _CurrCheckedGoods._UnitGoods._Price * (_CheckedUnitTown._CurrPriceRateList[_CurrCheckedGoods._UnitGoods._Group])) + "";
                _PriceSubText.text = "시세: " + (int)((int)( (_CheckedUnitTown._CurrPriceRateList[_CurrCheckedGoods._UnitGoods._Group] - _WorldManager._EconomyManager._BuyPriceDiscountAdjustRate)* 10000) * 0.01f) + "%" +
                    "/ 구입가: " + _WorldManager._PlayerUnitStarter._currGoodsCostInCarry[_CurrCheckedGoods._UnitGoods];
                
            }
            else  //살때
            {
                _PriceText.text = (int)( _CurrCheckedGoods._UnitGoods._Price * (_CheckedUnitTown._CurrPriceRateList[_CurrCheckedGoods._UnitGoods._Group] + _CheckedUnitTown._TaxRate - _CompanyDiscountRate )) + "";
                _PriceSubText.text = "시세: " + (int)((int)(   (_CheckedUnitTown._CurrPriceRateList[_CurrCheckedGoods._UnitGoods._Group] + _CheckedUnitTown._TaxRate - _CompanyDiscountRate - _WorldManager._EconomyManager._BuyPriceDiscountAdjustRate) * 10000) * 0.01f) + "%";
            }
            _WeightText.text = _CurrCheckedGoods._UnitGoods._Weight + " Kg";
            _ErrorMsgText.text = "";
            //_CheckedItemIMG;
            _SellTownText.text = "추천 판매도시: " + FindGoodSellTown(_CurrCheckedGoods._UnitGoods);

            _CheckedItemIMG.sprite = _CurrCheckedGoods._UnitGoods._GoodsSpriteGroupEntry.Sprite;

        }
    }

    string FindGoodSellTown(UnitGoods _currGoods)  //제일 비싸게 파는 마을 찾아서 리턴
    {
        string _return = "";
        int _groupNum = _currGoods._Group;

        int townNum = 999;
        float mostExpensivePrice = 0;

        for(int i=0; i < _WorldManager._DiscoveredTownList.Count; i++)
        {
            if(_WorldManager._DiscoveredTownList[i]._CurrPriceRateList[_groupNum] > mostExpensivePrice)
            {
                mostExpensivePrice = _WorldManager._DiscoveredTownList[i]._CurrPriceRateList[_groupNum];
                townNum = i;
                _return = _WorldManager._DiscoveredTownList[i]._name;
            }
        }

        return _return;


    }



    public void DrawSellSideGoodsList()
    {
        if (_WorldManager._PlayerUnitStarter._CurrUnitTown != null)  //현재 유저가 머무르고 있는 마을이 있어야 작동하게 한다.
        {
            _ContentRootSell.sizeDelta = new Vector2(0, 713 + 230 * (int)((_WorldManager._PlayerUnitStarter._CurrUnitTown._CurrGoodsDic.Keys.Count - 5) * 0.5f));
            float AnchPosXLeft = -109.9f;
            float AnchPosXRight = 130.2f;
            float initAnchPosY = -152.9f;
            float dAnchPosY = -231.9f;
            UnitGoodsObject tempGoodsObject = null;

            for (int i = 0; i < _WorldManager._PlayerUnitStarter._CurrUnitTown._CurrGoodsDic.Keys.Count; i++)
            {
                tempGoodsObject = Instantiate(_UnitGoodsObjectPrefab, _ContentRootSell.transform);
                if (i % 2 == 0)
                {
                    tempGoodsObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(AnchPosXLeft, initAnchPosY + dAnchPosY * (int)((i * 0.5f)));
                }
                else
                {
                    tempGoodsObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(AnchPosXRight, initAnchPosY + dAnchPosY * (int)((i * 0.5f)));
                }
                tempGoodsObject._UnitGoods = _WorldManager._PlayerUnitStarter._CurrUnitTown._CurrGoodsDic.Keys.ToList()[i];
                tempGoodsObject._TextCount.text = _WorldManager._PlayerUnitStarter._CurrUnitTown._CurrGoodsDic.Values.ToList()[i] + "";
                tempGoodsObject._IsChecked = false;
                tempGoodsObject._IsBelongInventory = false;
                if(tempGoodsObject._UnitGoods == _RememberedGoods && _IsBuyingMode)
                {
                    StartCoroutine(tempGoodsObject.DelayedTouch());
                   
                }
                tempGoodsObject._GoodsImage.sprite = _WorldManager._PlayerUnitStarter._CurrUnitTown._CurrGoodsDic.Keys.ToList()[i]._GoodsSpriteGroupEntry.Sprite; ;
                _DrawedGoods.Add(tempGoodsObject);
            }
        }
    }

    public void DrawInventorySideGoodsList()
    {
        _ContentRootInventory.sizeDelta = new Vector2(0, 713 + 230 * (int)((_WorldManager._PlayerUnitStarter._currGoodsInCarry.Count - 5) * 0.5f));
        float AnchPosXLeft = -109.9f;
        float AnchPosXRight = 130.2f;
        float initAnchPosY = -152.9f;
        float dAnchPosY = -231.9f;
        UnitGoodsObject tempGoodsObject = null;

        int _totalWeight = 0;


        for(int i=0; i< _WorldManager._PlayerUnitStarter._currGoodsInCarry.Keys.Count; i++)
        {
            tempGoodsObject = Instantiate(_UnitGoodsObjectPrefab, _ContentRootInventory.transform);
            if (i % 2 == 0)
            {
                tempGoodsObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(AnchPosXLeft, initAnchPosY + dAnchPosY * (int)((i * 0.5f)));

            }
            else
            {
                tempGoodsObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(AnchPosXRight, initAnchPosY + dAnchPosY * (int)((i * 0.5f)));
            }
            tempGoodsObject._UnitGoods = _WorldManager._PlayerUnitStarter._currGoodsInCarry.Keys.ToList()[i];
            tempGoodsObject._TextCount.text = _WorldManager._PlayerUnitStarter._currGoodsInCarry.Values.ToList()[i]+"";
            tempGoodsObject._IsChecked = false;
            tempGoodsObject._IsBelongInventory = true;
            
            _totalWeight += (int)(_WorldManager._PlayerUnitStarter._currGoodsInCarry.Keys.ToList()[i]._Weight
                * _WorldManager._PlayerUnitStarter._currGoodsInCarry.Values.ToList()[i]);
            if (tempGoodsObject._UnitGoods == _RememberedGoods && !_IsBuyingMode )
            {
                StartCoroutine(tempGoodsObject.DelayedTouch());
            }
            tempGoodsObject._GoodsImage.sprite = _WorldManager._PlayerUnitStarter._currGoodsInCarry.Keys.ToList()[i]._GoodsSpriteGroupEntry.Sprite; ;

            _DrawedGoods.Add(tempGoodsObject);
        }

        _TotalWeightText.text = _totalWeight + " kg";
        _MaxWeightText.text = "(" + _WorldManager._PlayerUnitStarter._MaxCarryWeight + " kg)";
    }

    public void ResetGoodsObjects()
    {
        for(int i=0; i < _DrawedGoods.Count; i++)
        {
            Destroy(_DrawedGoods[i].gameObject);
        }
        _DrawedGoods.Clear();
    }

    public void PurchaseCheckedGoods(int _BuyCount)  //현재 선택된 상품을 상점에서 구매한다. 세금 맥인다.
    {
     
        if (_CheckedUnitTown._CurrGoodsDic.ContainsKey(_CurrCheckedGoods._UnitGoods))  //현재 상품을 가지고 있을 경우..
        {
            int _BuyingPrice = (int)(_CurrCheckedGoods._UnitGoods._Price
               * (_CheckedUnitTown._CurrPriceRateList[_CurrCheckedGoods._UnitGoods._Group] + _CheckedUnitTown._TaxRate - _CompanyDiscountRate - _WorldManager._EconomyManager._BuyPriceDiscountAdjustRate));

            if (_CurrCheckedGoods._UnitGoods._Weight * _BuyCount <= _WorldManager._PlayerUnitStarter._MaxCarryWeight - _WorldManager._PlayerUnitStarter._CurrCarryWeight)  //무게 제한에 안 걸림 
            {
                if (_WorldManager._PlayerUnitStarter._Gold >= _BuyingPrice * _BuyCount)    //조건이 충족하므로 구입 행동 시작.
                {
                    _WorldManager._PlayerUnitStarter.BuyGoodsToCarryStock(_CurrCheckedGoods._UnitGoods, _BuyCount, _BuyingPrice);  //인벤에 추가
                    _WorldManager._PlayerUnitStarter._Gold -= _BuyingPrice * _BuyCount;  //골드 차감

                    if (_CheckedUnitTown._MonopolyCompany != null)
                    {
                        if (_CheckedUnitTown._MonopolyCompany != _WorldManager._PlayerUnitStarter._CurrCompany)  //현재 독점 기업과 나의 기업이 다르다면...
                        {
                            _WorldManager._CompanyManager.InsertTaxDividendToEachTownDic(_CheckedUnitTown._MonopolyCompany, _CheckedUnitTown, (int)(_BuyingPrice * _BuyCount * _CheckedUnitTown._TaxRate));
                        }
                    }


                    if (_CheckedUnitTown._CurrGoodsDic[_CurrCheckedGoods._UnitGoods] - _BuyCount <= 0)
                    {
                        _CheckedUnitTown._CurrGoodsDic[_CurrCheckedGoods._UnitGoods] = 0;  //개수를 0으로 만들고, 
                        _CheckedUnitTown._CurrGoodsDic.Remove(_CurrCheckedGoods._UnitGoods);  //해당하는 품목을 빼준다.
                    }
                    else
                    {
                        _CheckedUnitTown._CurrGoodsDic[_CurrCheckedGoods._UnitGoods] -= _BuyCount;   //구입한 만큼 마을에서 해당 개수를 빼준다.

                    }

                    _CheckedUnitTown._CurrPriceRateList[_CurrCheckedGoods._UnitGoods._Group]   //구입한 만큼 해당 마을의 물가가 변동한다.
                        = _CheckedUnitTown._CurrPriceRateList[_CurrCheckedGoods._UnitGoods._Group]
                        * (1 + (_BuyCount * _BuyingPrice) / (_CheckedUnitTown._CurrTownCommerceGrade * 100000f));

                    KickStart();  //다시 그려준다.

                }
                else
                {
                    _ErrorMsgText.text = "돈이 부족합니다";

                }
            }
            else
            {
                _ErrorMsgText.text = "너무 무겁습니다.";

            }
        }
        else
        {
            //StartCoroutine(RefreshSlowly());
            _ErrorMsgText.text = "이미 팔려버렸네요";

        }
    }

    IEnumerator RefreshSlowly()
    {

        yield return new WaitForSeconds(0.1f);
        KickStart();

        _ErrorMsgText.text = "이미 팔려버렸네요. 아쉽습니다.";

    }


    public void PurchaseCheckedGoodsBuyMAX()  //현재 선택된 상품을 상점에서 구매한다.
    {            

        if (_CheckedUnitTown._CurrGoodsDic.ContainsKey(_CurrCheckedGoods._UnitGoods))  //현재 상품을 가지고 있을 경우..  수량 제한 통과
        {
            int _BuyingPrice = (int)(_CurrCheckedGoods._UnitGoods._Price
            * (_CheckedUnitTown._CurrPriceRateList[_CurrCheckedGoods._UnitGoods._Group] + _CheckedUnitTown._TaxRate - _CompanyDiscountRate - _WorldManager._EconomyManager._BuyPriceDiscountAdjustRate));

            int _BuyCount = Mathf.Max(1
                , Mathf.Min(_CheckedUnitTown._CurrGoodsDic[_CurrCheckedGoods._UnitGoods], (int)((_WorldManager._PlayerUnitStarter._MaxCarryWeight - _WorldManager._PlayerUnitStarter._CurrCarryWeight) / _CurrCheckedGoods._UnitGoods._Weight), (int)(_WorldManager._PlayerUnitStarter._Gold / _BuyingPrice)));

            if (_CurrCheckedGoods._UnitGoods._Weight * _BuyCount <= _WorldManager._PlayerUnitStarter._MaxCarryWeight - _WorldManager._PlayerUnitStarter._CurrCarryWeight)  //무게 제한에도 안 걸림 
            {
                if (_WorldManager._PlayerUnitStarter._Gold >= _BuyingPrice * _BuyCount)    //골드 제한도 안걸림. 조건이 충족하므로 구입 행동 시작.
                {
                    _WorldManager._PlayerUnitStarter.BuyGoodsToCarryStock(_CurrCheckedGoods._UnitGoods, _BuyCount, _BuyingPrice);  //인벤에 추가
                    _WorldManager._PlayerUnitStarter._Gold -= _BuyingPrice * _BuyCount;  //골드 차감

                    if (_CheckedUnitTown._MonopolyCompany != null)
                    {
                        if (_CheckedUnitTown._MonopolyCompany != _WorldManager._PlayerUnitStarter._CurrCompany)  //현재 독점 기업과 나의 기업이 다르다면...
                        {
                            _WorldManager._CompanyManager.InsertTaxDividendToEachTownDic(_CheckedUnitTown._MonopolyCompany, _CheckedUnitTown, (int)(_BuyingPrice * _BuyCount * _CheckedUnitTown._TaxRate));
                        }
                    }

                    if (_CheckedUnitTown._CurrGoodsDic[_CurrCheckedGoods._UnitGoods] - _BuyCount <= 0)
                    {
                        _CheckedUnitTown._CurrGoodsDic[_CurrCheckedGoods._UnitGoods] = 0;  //개수를 0으로 만들고, 
                        _CheckedUnitTown._CurrGoodsDic.Remove(_CurrCheckedGoods._UnitGoods);  //해당하는 품목을 빼준다.

                    }
                    else
                    {
                        _CheckedUnitTown._CurrGoodsDic[_CurrCheckedGoods._UnitGoods] -= _BuyCount;   //구입한 만큼 마을에서 해당 개수를 빼준다.

                    }

                    _CheckedUnitTown._CurrPriceRateList[_CurrCheckedGoods._UnitGoods._Group]   //구입한 만큼 해당 마을의 물가가 변동한다.
                        = _CheckedUnitTown._CurrPriceRateList[_CurrCheckedGoods._UnitGoods._Group]
                        * (1 + (_BuyCount * _BuyingPrice) / (_CheckedUnitTown._CurrTownCommerceGrade * 100000f));

                    KickStart();  //다시 그려준다.

                }
                else
                {
                    _ErrorMsgText.text = "돈이 부족합니다";

                }
            }
            else
            {
                _ErrorMsgText.text = "너무 무겁습니다.";

            }
        }
        else
        {
            _ErrorMsgText.text = "이미 팔려버렸네요";
            //StartCoroutine(RefreshSlowly());

        }
    }


    public void SellCheckedGoods(int _SellCount)  //현재 선택된 상품을 판매한다
    {
        _PriceAdjustor = 1;
        
        if (_CheckedUnitTown._TownRegularlyGoods.Contains(_CurrCheckedGoods._UnitGoods))
        {
            _PriceAdjustor = 0.5f;
        }

        int _SellingPrice = (int)(_CurrCheckedGoods._UnitGoods._Price
          * (_WorldManager._PlayerUnitStarter._CurrUnitTown._CurrPriceRateList[_CurrCheckedGoods._UnitGoods._Group]) * _PriceAdjustor);


        _WorldManager._PlayerUnitStarter._CurrTradeExp += (_SellingPrice - _WorldManager._PlayerUnitStarter._currGoodsCostInCarry[_CurrCheckedGoods._UnitGoods]) * _SellCount;   //경험치 획득 행위 (이득 발생시만 처리)
        _WorldManager._StarterManager.LevelDeciderForTrade(_WorldManager._PlayerUnitStarter);


        _WorldManager._PlayerUnitStarter.SellGoodsFromCarryStock(_CurrCheckedGoods._UnitGoods, _SellCount);
        _WorldManager._PlayerUnitStarter._Gold += _SellingPrice * _SellCount;  //골드 차감

        _CheckedUnitTown._CurrPriceRateList[_CurrCheckedGoods._UnitGoods._Group]   //판 만큼 해당 마을의 물가가 변동한다.
            = _CheckedUnitTown._CurrPriceRateList[_CurrCheckedGoods._UnitGoods._Group]
            * (1 - (_SellCount * _SellingPrice) / (_CheckedUnitTown._CurrTownCommerceGrade * 100000f));

        KickStart();  //다시 그려준다.
        

    }

    public void SellCheckedGoodsAll()  //현재 선택된 상품을 판매한다
    {

        _PriceAdjustor = 1;

        if (_CheckedUnitTown._TownRegularlyGoods.Contains(_CurrCheckedGoods._UnitGoods))
        {
            _PriceAdjustor = 0.5f;
        }

        int _SellCount = _WorldManager._PlayerUnitStarter._currGoodsInCarry[_CurrCheckedGoods._UnitGoods];
        int _SellingPrice = (int)(_CurrCheckedGoods._UnitGoods._Price
                * (_WorldManager._PlayerUnitStarter._CurrUnitTown._CurrPriceRateList[_CurrCheckedGoods._UnitGoods._Group] ) * _PriceAdjustor);
        
        _WorldManager._PlayerUnitStarter._CurrTradeExp += (_SellingPrice - _WorldManager._PlayerUnitStarter._currGoodsCostInCarry[_CurrCheckedGoods._UnitGoods]) * _SellCount;   //경험치 획득 행위 (이득 발생시만 처리)
        _WorldManager._StarterManager.LevelDeciderForTrade(_WorldManager._PlayerUnitStarter);

        _WorldManager._PlayerUnitStarter.SellGoodsFromCarryStock(_CurrCheckedGoods._UnitGoods, _SellCount);
        _WorldManager._PlayerUnitStarter._Gold += _SellingPrice * _SellCount;  //골드 차감


        _CheckedUnitTown._CurrPriceRateList[_CurrCheckedGoods._UnitGoods._Group]   //판 만큼 해당 마을의 물가가 변동한다.
            = _CheckedUnitTown._CurrPriceRateList[_CurrCheckedGoods._UnitGoods._Group]
            * (1 - (_SellCount * _SellingPrice) / (_CheckedUnitTown._CurrTownCommerceGrade * 100000f));




        KickStart();  //다시 그려준다.

    }
}
