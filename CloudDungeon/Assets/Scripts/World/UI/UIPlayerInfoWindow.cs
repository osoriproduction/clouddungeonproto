﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class UIPlayerInfoWindow : MonoBehaviour {

    WorldManager _WorldManager;
    public UnitStarter _CalledStarter;

    //left side
    public GameObject _PortraitRoot;
    public GameObject _HPBar;
    public GameObject _MPBar;
    public Text _textHP;
    public Text _textMP;
    public Text _textClass;
    public Text _textName;
    public Text _textLv;

    //deatil Side
    public GameObject _ContentInfoRoot;
    public UIUnitAttribute _UIUnitAttributePrefab;

    //btnSide

    public GameObject _EquipBTN;

    public void KickStart()
    {

        //캐릭정보 쪽
        _WorldManager = GetComponentInParent<WorldManager>();
        _UIUnitAttributeList = new List<UIUnitAttribute>();

        _CalledStarter = _WorldManager._PlayerUnitStarter;

        if (_CalledStarter != null)  //얘는 메소드 밖에서 사전적으로 참조해주고 들어와야 한다.
        {
            _textName.text = _CalledStarter._Name;

            _HPBar.transform.localScale = new Vector3(_CalledStarter._currentHP / _CalledStarter._maxHP, 1, 1);
            _MPBar.transform.localScale = new Vector3(_CalledStarter._currentMP / _CalledStarter._maxMP, 1, 1);
            _textHP.text = (int)_CalledStarter._currentHP + "/ " + (int)_CalledStarter._maxHP;
            _textMP.text = (int)_CalledStarter._currentMP + "/ " + (int)_CalledStarter._maxMP;
            _textLv.text = "Lv. " + _CalledStarter._TradeLevel + "  " + (_CalledStarter._CurrTradeExp - _WorldManager._StarterManager._ExpTable[_CalledStarter._TradeLevel - 1]) + "/ " + (_WorldManager._StarterManager._ExpTable[_CalledStarter._TradeLevel] - _WorldManager._StarterManager._ExpTable[_CalledStarter._TradeLevel - 1]);

            DrawDetailnfos();

        }
        else
        {
            CloseUI();
        }

        //인벤토리쪽
        
        _ContentCheckedGoodsInfo.SetActive(false);

        _WorldManager._PlayerUnitStarter.FindMyLocatedTileObject();
        ResetGoodsObjects();
        DrawInventorySideGoodsList();

        _CompanyDiscountRate = 0;
        if (_WorldManager._PlayerUnitStarter._CurrCompany != null)  // 회사 독점 시 이득
        {
            if (_WorldManager._PlayerUnitStarter._CurrCompany == _WorldManager._PlayerUnitStarter._CurrUnitTown._MonopolyCompany)
            {
                _CompanyDiscountRate = _WorldManager._PlayerUnitStarter._CurrUnitTown._TaxRate;
            }
        }

        _CheckedUnitTown = _WorldManager._PlayerUnitStarter._CurrUnitTown;



    }

    public void DrawDetailnfos()
    {
        for (int i = 0; i < _UIUnitAttributeList.Count; i++)
        {
            Destroy(_UIUnitAttributeList[i].gameObject);

        }
        _UIUnitAttributeList.Clear();

        int _posY = -100;
        DrawDetailUnitInfo("레벨", _CalledStarter._TradeLevel + "", _posY);
        _posY += -70;

        if (_CalledStarter._CurrCompany != null)
        {
            DrawDetailUnitInfo("소속길드", _CalledStarter._CurrCompany._Name + "", _posY);
            _posY += -70;

        }
        else
        {
            DrawDetailUnitInfo("소속길드", "없음", _posY);
            _posY += -70;
        }

        _posY += -30;

        DrawDetailUnitInfo("소지골드", (int)_CalledStarter._Gold + "", _posY);
        _posY += -70;

        DrawDetailUnitInfo("재고금액", (int)CalculateCurrInventoryCost() + "", _posY);
        _posY += -70;
        
        _posY += -30;
            

        DrawDetailUnitInfo("힘", _CalledStarter._StatSTR + "", _posY);
        _posY += -70;

        DrawDetailUnitInfo("맷집", _CalledStarter._StatCON + "", _posY);
        _posY += -70;

        DrawDetailUnitInfo("빠름", _CalledStarter._StatDEX + "", _posY);
        _posY += -70;

        DrawDetailUnitInfo("영리함", _CalledStarter._StatINT + "", _posY);
        _posY += -70;

        DrawDetailUnitInfo("지혜", _CalledStarter._StatWIS + "", _posY);
        _posY += -70;

        DrawDetailUnitInfo("감각", _CalledStarter._StatSENSE + "", _posY);
        _posY += -70;
        _posY += -30;

        DrawDetailUnitInfo("물리 공격력", _CalledStarter._StatPhysicsATK + "", _posY);
        _posY += -70;

        DrawDetailUnitInfo("마법 공격력", _CalledStarter._StatMagicATK + "", _posY);
        _posY += -70;

        DrawDetailUnitInfo("물리 방어력", _CalledStarter._StatPhysicsArmor+ "", _posY);
        _posY += -70;
        DrawDetailUnitInfo("마법 방어력", _CalledStarter._StatMagicArmor + "", _posY);

        _posY += -70;
        _posY += -30;
        

        DrawDetailUnitInfo("정보력", _CalledStarter._StatTradeInformationStrength + "", _posY);
        _posY += -70;

        DrawDetailUnitInfo("의지", _CalledStarter._StatTradeWillingness + "", _posY);
        _posY += -70;

        DrawDetailUnitInfo("판단력", _CalledStarter._StatTradeJudgement + "", _posY);
        _posY += -70;

        DrawDetailUnitInfo("이동 속도", (int)(_CalledStarter._MoveSPD * 100) + "", _posY);
        _posY += -70;
        
    }

    public List<UIUnitAttribute> _UIUnitAttributeList; //여기 모아두었다가 나중에 한번에 정리
    public void DrawDetailUnitInfo(string _stringheader, string _stringcontext, float _posY)
    {
        UIUnitAttribute tempAttri = Instantiate(_UIUnitAttributePrefab, _ContentInfoRoot.transform);
        tempAttri.transform.localPosition = new Vector3(150, _posY, 0);
        tempAttri.TextHead.text = _stringheader;
        tempAttri.TextContext.text = _stringcontext;
        _UIUnitAttributeList.Add(tempAttri);
    }


    public void CallUI(UnitStarter _calledStarter)
    {
        _CalledStarter = _calledStarter;
        
        /*
        if (_CalledStarter != null)
        {
            if (_CalledStarter._classType == 1)
            {
                _textClass.text = "Fighter";
            }
            else if (_CalledStarter._classType == 2)
            {
                _textClass.text = "Archer";
            }
            else if (_CalledStarter._classType == 3)
            {
                _textClass.text = "Wizard";
            }
            else if (_CalledStarter._classType == 4)
            {
                _textClass.text = "Healer";
            }
            else if (_CalledStarter._classType == 5)
            {
                _textClass.text = "Assassin";
            }
        }*/
    }

    public void CloseUI()
    {
        _CalledStarter = null;
        this.gameObject.SetActive(false);
    }
    
    public int CalculateCurrInventoryCost()
    {
        int _return = 0;
        for (int i = 0; i < _CalledStarter._currGoodsCostInCarry.Keys.Count; i++)
        {
            _return += _CalledStarter._currGoodsCostInCarry.ToList()[i].Value;

        }

        return _return;
    }















    //--------------------------------------inventory영역
    public UnitGoodsObject _UnitGoodsObjectPrefab; //로딩될 상품 오브젝트들

    public RectTransform _ContentRootInventory;    //인벤영역 컨텐츠 루트
    public GameObject _ContentCheckedGoodsInfo;  //선택된 아이템 정보
    public Text _ItemNameText;
    public Text _PriceText;
    public Text _PriceSubText;
    public Text _WeightText;
    public Text _ErrorMsgText;
    public Image _CheckedItemIMG;

    public Text _TotalWeightText;
    public Text _MaxWeightText;
    public Text _GoldText;
    public Text _SellTownText; //어디서 제일 비싸게 팔리나 힌트.


    //시스템
    public UnitGoodsObject _CurrCheckedGoods;
    public List<UnitGoodsObject> _DrawedGoods;
    public UnitGoods _RememberedGoods; //현재 체크된 오브젝트의 굿즈를 기억해준다.

    public bool _IsBuyingMode; //기본은 인벤토리 판매 모드이다.

    public float _CompanyDiscountRate;
    UnitTown _CheckedUnitTown;  //현재 타운을 참조해준다.
    float _PriceAdjustor;  //현재 가격 조정자


    public void DrawCheckedGoodsInfo()
    {
        if (_CurrCheckedGoods == null)
        {
            _ContentCheckedGoodsInfo.SetActive(false);
            _EquipBTN.gameObject.SetActive(false);
        }
        else
        {
            _ContentCheckedGoodsInfo.SetActive(true);
            _EquipBTN.gameObject.SetActive(true);
                        
            _ItemNameText.text = _CurrCheckedGoods._UnitGoods._Name;

            if (!_IsBuyingMode)  //팔때. 팔때는 세금을 안붙힌다. 살때만... 부가가치세처럼 매기자.
            {
                _PriceAdjustor = 1;


                _PriceText.text = "-";
                _PriceSubText.text = "시세: " + "-" +
                    "/ 구입가: " + _WorldManager._PlayerUnitStarter._currGoodsCostInCarry[_CurrCheckedGoods._UnitGoods];

            }
            else  //살때
            {
                _PriceText.text = (int)(_CurrCheckedGoods._UnitGoods._Price * (_CheckedUnitTown._CurrPriceRateList[_CurrCheckedGoods._UnitGoods._Group] + _CheckedUnitTown._TaxRate - _CompanyDiscountRate)) + "";
                _PriceSubText.text = "시세: " + (int)((int)((_CheckedUnitTown._CurrPriceRateList[_CurrCheckedGoods._UnitGoods._Group] + _CheckedUnitTown._TaxRate - _CompanyDiscountRate) * 10000) * 0.01f) + "%";
            }
            _WeightText.text = _CurrCheckedGoods._UnitGoods._Weight + " Kg";
            _ErrorMsgText.text = "";
            //_CheckedItemIMG;
            _SellTownText.text = "추천 판매도시: " + FindGoodSellTown(_CurrCheckedGoods._UnitGoods);

            _CheckedItemIMG.sprite = _CurrCheckedGoods._UnitGoods._GoodsSpriteGroupEntry.Sprite;

        }
    }

    string FindGoodSellTown(UnitGoods _currGoods)  //제일 비싸게 파는 마을 찾아서 리턴
    {
        string _return = "";
        int _groupNum = _currGoods._Group;

        int townNum = 999;
        float mostExpensivePrice = 0;

        for (int i = 0; i < _WorldManager._DiscoveredTownList.Count; i++)
        {
            if (_WorldManager._DiscoveredTownList[i]._CurrPriceRateList[_groupNum] > mostExpensivePrice)
            {
                mostExpensivePrice = _WorldManager._DiscoveredTownList[i]._CurrPriceRateList[_groupNum];
                townNum = i;
                _return = _WorldManager._DiscoveredTownList[i]._name;
            }
        }

        return _return;


    }


    public void DrawInventorySideGoodsList()
    {
        _ContentRootInventory.sizeDelta = new Vector2(0, 713 + 230 * (int)((_WorldManager._PlayerUnitStarter._currGoodsInCarry.Count - 5) * 0.5f));
        float AnchPosXLeft = -109.9f;
        float AnchPosXRight = 130.2f;
        float initAnchPosY = -152.9f;
        float dAnchPosY = -231.9f;
        UnitGoodsObject tempGoodsObject = null;

        int _totalWeight = 0;


        for (int i = 0; i < _WorldManager._PlayerUnitStarter._currGoodsInCarry.Keys.Count; i++)
        {
            tempGoodsObject = Instantiate(_UnitGoodsObjectPrefab, _ContentRootInventory.transform);
            if (i % 2 == 0)
            {
                tempGoodsObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(AnchPosXLeft, initAnchPosY + dAnchPosY * (int)((i * 0.5f)));
            }
            else
            {
                tempGoodsObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(AnchPosXRight, initAnchPosY + dAnchPosY * (int)((i * 0.5f)));
            }
            tempGoodsObject._UnitGoods = _WorldManager._PlayerUnitStarter._currGoodsInCarry.Keys.ToList()[i];
            tempGoodsObject._TextCount.text = _WorldManager._PlayerUnitStarter._currGoodsInCarry.Values.ToList()[i] + "";
            tempGoodsObject._IsChecked = false;
            tempGoodsObject._IsBelongInventory = true;

            _totalWeight += (int)(_WorldManager._PlayerUnitStarter._currGoodsInCarry.Keys.ToList()[i]._Weight
                * _WorldManager._PlayerUnitStarter._currGoodsInCarry.Values.ToList()[i]);

            if (tempGoodsObject._UnitGoods == _RememberedGoods && !_IsBuyingMode)
            {
                StartCoroutine(tempGoodsObject.DelayedTouch());
            }

            tempGoodsObject._GoodsImage.sprite = _WorldManager._PlayerUnitStarter._currGoodsInCarry.Keys.ToList()[i]._GoodsSpriteGroupEntry.Sprite;

            _DrawedGoods.Add(tempGoodsObject);
        }

        _TotalWeightText.text = _totalWeight + " kg";
        _MaxWeightText.text = "(" + _WorldManager._PlayerUnitStarter._MaxCarryWeight + " kg)";
    }

    public void ResetGoodsObjects()
    {
        for (int i = 0; i < _DrawedGoods.Count; i++)
        {
            Destroy(_DrawedGoods[i].gameObject);
        }
        _DrawedGoods.Clear();
    }



    public void EquipCheckedItem()
    {
        if (_CurrCheckedGoods != null)
        {
            if (_CurrCheckedGoods._UnitGoods._GoodsSpriteGroupEntry != null)
            {
                _WorldManager._PlayerUnitStarter.EquipItem(_CurrCheckedGoods._UnitGoods);
                _CurrCheckedGoods = null;
                KickStart();

            }
        }
    }


}
