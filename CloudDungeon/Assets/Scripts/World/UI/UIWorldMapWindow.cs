﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UIWorldMapWindow : MonoBehaviour {


    //외부참조
    WorldManager _WorldManager;
    public UnitMapObject _UnitMapObjectPrefab;

    public GameObject _MapObjectRoot;


    //시스템
    public List<UnitMapObject> _MapObjectList; // 생성된 오브젝트 맵들을 가지고 있다.


	void Start () {

    }
	
    public void KickStart()
    {
        if (_WorldManager == null)
            _WorldManager = GetComponentInParent<WorldManager>();

        InstantiateMapObjects();
    }


    public void InstantiateMapObjects()
    {
        ResetMap();

        for (int i=0; i< _WorldManager._CurrTownList.Count; i++)
        {
            if (_WorldManager._DiscoveredTownList.Contains(_WorldManager._CurrTownList[i]))
            {
                UnitMapObject tempMapObj = Instantiate(_UnitMapObjectPrefab, _MapObjectRoot.transform);
                tempMapObj._CurrUnitTown = _WorldManager._CurrTownList[i];
                tempMapObj._TextTownName.text = tempMapObj._CurrUnitTown._name;
                tempMapObj.transform.localPosition = _WorldManager._CurrTownList[i]._currUnitTile._unitTileObject.transform.localPosition * 10f;
                _MapObjectList.Add(tempMapObj);
            }
        }
    }

    public void ResetMap()
    {
        for (int i = 0; i < _MapObjectList.Count; i++)
        {
            Destroy(_MapObjectList[i].gameObject);
        }
        _MapObjectList.Clear();

    }


}
