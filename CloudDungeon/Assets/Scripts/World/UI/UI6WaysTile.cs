﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI6WaysTile : MonoBehaviour {

    WorldManager _WorldManager;
    //public string _6waysPosition;  //LT   LM    LB    RT    RM   RB

    public UnitCommandTile _currCommandTile;

    void Start () {
        _WorldManager = GetComponentInParent<WorldManager>();
        _currCommandTile = new UnitCommandTile();

    }

    public void Touch6WaysTile()
    {
        if (!_WorldManager._WorldCharacterController._IsMoving)
        {            
            if (_WorldManager._IsCommandTileOn)
            {
                if (_currCommandTile != null)
                {

                    _WorldManager.DrawCommandTile(false, false);

                    CharacterMoveThere();
                }
            }
            else
            {

                _WorldManager.DrawCommandTile(false, true);
            }
        }
    }

    void CharacterMoveThere()
    {
        if (_WorldManager._PlayerUnitStarter._state ==1)
        {
            _WorldManager._PlayerUnitStarter.MoveCharacterToHere(_currCommandTile._CurrUnitTile._unitTileObject, true);
        }
    }
}
