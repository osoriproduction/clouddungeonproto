﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMenuBtnController : MonoBehaviour {

    //외부참조 
    public GameObject _UITradePurchase;
    public GameObject _UIWorldMapWindow;
    WorldManager _WorldManager;

    public AnimatedButton _TradeBTN;
    public AnimatedButton _TownBTN;
    public AnimatedButton _PubBTN;

    public AnimatedButton _RestBTN;
    public Text _RestCostText;

    public bool _IsWindowOn; //윈도우가 켜졌는지, 꺼졌는지 체크하는 변수


    private void Start()
    {
        _WorldManager = GetComponentInParent<WorldManager>();
    }

    int _updateCount;
    private void Update()
    {
        _updateCount++;
        if(_updateCount%10 == 0)
        {
            if(_WorldManager._PlayerUnitStarter._CurrUnitTown != null)
            {
                _TradeBTN.gameObject.SetActive(true);
                _TownBTN.gameObject.SetActive(true);
                _PubBTN.gameObject.SetActive(true);
                if (_WorldManager._PlayerUnitStarter._currentMP < _WorldManager._PlayerUnitStarter._maxMP )
                {
                    _RestBTN.gameObject.SetActive(true);
                }

            }
            else
            {
                _TradeBTN.gameObject.SetActive(false);
                _TownBTN.gameObject.SetActive(false);
                _PubBTN.gameObject.SetActive(false);
                _RestBTN.gameObject.SetActive(false);

            }
        }
    }


    public void CallUITradePurchase()
    {
        _IsWindowOn = true;

        _UITradePurchase.gameObject.SetActive(true);
        _WorldManager._UITradeWindow.KickStart();

        _WorldManager._WorldCharacterController.ToggleMoveCommandTiles();
    }

    public void CloseUITradePurchase()
    {
        _IsWindowOn = false;


        _UITradePurchase.gameObject.SetActive(false);

    }

    public void CallUIWorldMap()
    {
        _IsWindowOn = true;

        _UIWorldMapWindow.gameObject.SetActive(true);
        _WorldManager._UIWorldMapWindow.KickStart();

        _WorldManager._WorldCharacterController.ToggleMoveCommandTiles();

    }

    public void CloseUIWorldMap()
    {
        _IsWindowOn = false;


        _UIWorldMapWindow.gameObject.SetActive(false);

    }


    public void CallUITownInfo()
    {
        _IsWindowOn = true;


        _WorldManager._UITownInfoWindow.gameObject.SetActive(true);
        _WorldManager._UITownInfoWindow.KickStart();

        _WorldManager._WorldCharacterController.ToggleMoveCommandTiles();

    }

    public void CloseUITownInfo()
    {
        _IsWindowOn = false;


        _WorldManager._UITownInfoWindow.gameObject.SetActive(false);

    }

    public void CallUICompanyInfo()
    {
        _IsWindowOn = true;


        _WorldManager._UICompanyInfoWindow.gameObject.SetActive(true);
        _WorldManager._UICompanyInfoWindow.KickStart();

        _WorldManager._WorldCharacterController.ToggleMoveCommandTiles();

    }

    public void CloseUICompanyInfo()
    {
        _IsWindowOn = false;


        _WorldManager._UICompanyInfoWindow.gameObject.SetActive(false);

    }


    public void CallUIPubInfo()
    {
        _IsWindowOn = true;


        _WorldManager._UIPubInfoWindow.gameObject.SetActive(true);
        _WorldManager._UIPubInfoWindow.KickStart();

        _WorldManager._WorldCharacterController.ToggleMoveCommandTiles();

    }

    public void CloseUIPubInfo()
    {
        _IsWindowOn = false;


        _WorldManager._UIPubInfoWindow.ClearPub();
        _WorldManager._UIPubInfoWindow.gameObject.SetActive(false);
        
    }

    public void CallCharWindow()
    {
        _IsWindowOn = true;


        _WorldManager._UICharWindow.gameObject.SetActive(true);
        _WorldManager._UICharWindow.KickStart();


    }

    public void CloseCharWindow()
    {
        _IsWindowOn = false;


        _WorldManager._UICharWindow.gameObject.SetActive(false);

    }

  


    public void CallUIPlayerInfo()
    {
        _IsWindowOn = true;


        _WorldManager._UIPlayerInfoWindow.gameObject.SetActive(true);
        _WorldManager._UIPlayerInfoWindow.KickStart();

        _WorldManager._WorldCharacterController.ToggleMoveCommandTiles();

    }


    public void CloseUIPlayerInfo()
    {
        _IsWindowOn = false;


        _WorldManager._UIPlayerInfoWindow.gameObject.SetActive(false);

    }


    public void AttackBTN()
    {

        _WorldManager.DrawCommandTile(false, false);

        _WorldManager._PlayerUnitStarter.Attack();

    }



}

