﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class UICompanyInfoWindow : MonoBehaviour {

    //외부 참조
    WorldManager _WorldManager;
    public GameObject _BeforeRoot;
    public GameObject _AfterRoot;
    

    //내부 참조
    public UnitCompanay _OpendCurrCompany; //현재 열려있는 컴퍼니

    public AnimatedButton _DoDivideBTN;
    public Text _TotalDividendText;
    public Text _CompanyLevelText;
    public Text _MeberListText;
    public Text _TownListText;
    
    public Text _MakeGoldText;
    public int _MakeCompanyCost; //회사 창설시 최초 필요 자본금
    public Text _WillMakeCompanyNameText;
    public string _MakeCompanyName;

    public Text _CurrCompanyNameText;  //만들고 난 후 컴퍼니의 이름 텍스트

    public void KickStart()
    {
        _WorldManager = GetComponentInParent<WorldManager>();
        _MakeCompanyCost = 5000;

        if (_WorldManager._PlayerUnitStarter._CurrCompany != null)
        {
            _BeforeRoot.SetActive(false);
            _AfterRoot.SetActive(true);

            _OpendCurrCompany = _WorldManager._PlayerUnitStarter._CurrCompany;
            RefreshDisplay();

        }
        else //가입된 컴퍼니가 없을 때는 새로운 컴퍼니 창설이 가능하다. 
        {
            SuggestMakeCompany();

            _BeforeRoot.SetActive(true);
            _AfterRoot.SetActive(false);

        }


    }


    public void SuggestMakeCompany()
    {
        _WillMakeCompanyNameText.text = "???"; //나중에 해주자.. ㅋㅋ
        _MakeGoldText.text = _MakeCompanyCost + "";

    }

    public void LetsCompoundCompany()  //컴파니 설립 버튼
    {
        if (_WorldManager._PlayerUnitStarter._Gold >= _MakeCompanyCost)
        {
            _WorldManager._PlayerUnitStarter._Gold -= _MakeCompanyCost;
            int tempNum = (int)Random.Range(1, _WorldManager._CompanyManager._ProtoCompanyList.Count);

            UnitCompanay tempCompany = _WorldManager._CompanyManager._ProtoCompanyList[tempNum];
            _WorldManager._CompanyManager._ProtoCompanyList.RemoveAt(tempNum);
            _WorldManager._CompanyManager._CurrCompanyList.Add(tempCompany);
            _WorldManager._CompanyManager.AddStarterToCompany(tempCompany, _WorldManager._PlayerUnitStarter, 3000);

            _WorldManager._PlayerUnitStarter._CurrCompany = tempCompany;

            _WorldManager._UIMenuBtnController.CloseUICompanyInfo();
            _WorldManager._UIMenuBtnController.CallUICompanyInfo();
        }
    }


    public void RefreshDisplay()
    {
        _CurrCompanyNameText.text = _OpendCurrCompany._Name;
        _CompanyLevelText.text = "Lv." + _OpendCurrCompany._Level + "  " + _OpendCurrCompany._TotalStockAmount+"/ "+_WorldManager._CompanyManager._CompanyExpTable[_OpendCurrCompany._Level ] ;

        string _context = "";

        //_OpendCurrTown._TownStockInvestmentDic....   디셔너리 정렬 후.. 한다.
        for (int i = 0; i < _OpendCurrCompany._StartersInCompanyDic.Keys.Count; i++)  //순위 표기해준다.
        {
            _context += (_OpendCurrCompany._StartersInCompanyDic.Keys.ToList()[i]._Name + " " + _OpendCurrCompany._StartersInCompanyDic.Values.ToList()[i])+" ( "+ (int)(_OpendCurrCompany._StartersInCompanyDic.Values.ToList()[i]*100/_OpendCurrCompany._TotalStockAmount) + "%"+" )" ;

            if (i % 2 == 1)
            {
                _context += "\n";
            }
            else
            {
                _context += "       ";

            }
        }
        _MeberListText.text = _context;


        string _context2 = "";
        for (int i = 0; i < _OpendCurrCompany._StockPerTownDic.Keys.Count; i++)  //순위 표기해준다.
        {
            _context2 += (_OpendCurrCompany._StockPerTownDic.Keys.ToList()[i]._name + " " + _OpendCurrCompany._StockPerTownDic.Values.ToList()[i]) + " ( " + (int)(_OpendCurrCompany._StockPerTownDic.Values.ToList()[i] * 100 / _OpendCurrCompany._StockPerTownDic.Keys.ToList()[i]._TotalInvestmentAmount ) + "%" + " )";

            if (i % 2 == 1)
            {
                _context2 += "\n";
            }
            else
            {
                _context2 += "       ";

            }
        }
        _TownListText.text = _context2;


        _TotalDividendText.text = "Total Dividends:   "+ _WorldManager._PlayerUnitStarter._CurrCompany._TotalDividens + "";

        if (_WorldManager._PlayerUnitStarter._CurrCompany._TotalDividens > 0)
        {
            _DoDivideBTN.gameObject.SetActive(true);
        }
        else
        {
            _DoDivideBTN.gameObject.SetActive(false);

        }
    }




}
