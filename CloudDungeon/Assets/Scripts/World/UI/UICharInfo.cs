﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICharInfo : MonoBehaviour {


    WorldManager _WorldManager;
    UnitStarter _UnitStarter;

    public GameObject _HPBarInfo;
    public GameObject _MPBarInfo;

    public Text _TextName;
    public Text _TextCompanyName;

    public GameObject _LvInfo;
    public Text _LvText;
    public GameObject _HPBar;
    public GameObject _MPBar;

    [HideInInspector]
    public Image _MPBarIMage;

    [HideInInspector]
    public Image _HPBarIMage;

    public UITextRoot _UITextRoot;
    public GameObject _MoveReadyIcon;

    public Color _yellowColor;  // 돈 넣기 전
    public Color _blueColor;  //돈 꽉 차서 피버시간일 때


    void Start()
    {
        //_GameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        _WorldManager = GetComponentInParent<WorldManager>();
        _UnitStarter = GetComponentInParent<UnitStarter>();

        _UITextRoot = GetComponentInChildren<UITextRoot>();
        _yellowColor = new Color(0.95f, 0.85f, 0.15f, 1);
        _blueColor = new Color(0, 0, 1, 1);
        _MPBarIMage = _MPBar.GetComponent<Image>();
        _MPBarIMage.color = _yellowColor;
        _HPBarIMage = _HPBar.GetComponent<Image>();
        if (_UnitStarter._IsMonster)
        {
            _HPBarIMage.color = Color.red;
        }
        StartCoroutine(SlowUpdate());
    }

    void Update () {
     
        if (_UnitStarter!=null)
        {
            if (_UnitStarter._CurrOnTheTileObject != null)
            {
                if (_UnitStarter._CurrOnTheTileObject._CurrUnitTile._IsDiscovered || _UnitStarter._state==2)
                {
                    _HPBarInfo.gameObject.SetActive(true);
                    _MPBarInfo.gameObject.SetActive(true);
                    _TextName.gameObject.SetActive(true);
                    _TextCompanyName.gameObject.SetActive(true);
                    _LvInfo.gameObject.SetActive(true);
                    _UITextRoot.gameObject.SetActive(true);
                }
                else
                {
                    _HPBarInfo.gameObject.SetActive(false);
                    _MPBarInfo.gameObject.SetActive(false);
                    _TextName.gameObject.SetActive(false);
                    _TextCompanyName.gameObject.SetActive(false);
                    _LvInfo.gameObject.SetActive(false);
                    _UITextRoot.SleepAllTextBox();
                    _UITextRoot.gameObject.SetActive(false);

                }

            }

            if (_UnitStarter.transform.localScale.x < 0)
            {
                this.transform.localScale = new Vector3(-1, 1, 1);
            }
            else
            {
                this.transform.localScale = new Vector3(1, 1, 1);
            }

            
        }
    }


    IEnumerator SlowUpdate()
    {
        float _WaitTime = 0.2f;


        _MPBarIMage.color = _yellowColor;

        for (; ; )
        {
            yield return new WaitForSeconds(_WaitTime);

            if (_UnitStarter != null)
            {

                _HPBar.transform.localScale = new Vector3(Mathf.Clamp(_UnitStarter._currentHP / _UnitStarter._maxHP, 0.05f, 1), 1, 1);
                _MPBar.transform.localScale = new Vector3(Mathf.Clamp(_UnitStarter._currentMP / _UnitStarter._maxMP, 0.05f, 1), 1, 1);
                if (_UnitStarter._WarLevel <= _UnitStarter._TradeLevel)
                {
                    _LvText.text = _UnitStarter._TradeLevel + "";

                }
                else
                {
                    _LvText.text = _UnitStarter._WarLevel + "";

                }

                if (_UnitStarter._CurrCompany != null)
                {
                    _TextName.text = _UnitStarter._Name;
                    _TextCompanyName.text = _UnitStarter._CurrCompany._Name;
                }
                else
                {
                    _TextName.text = _UnitStarter._Name;
                    _TextCompanyName.text = "";
                }

                if (_UnitStarter._IsMonster)
                {
                    _TextName.color = Color.red;
                }
                else
                {

                }

            }

        }

    }
}
