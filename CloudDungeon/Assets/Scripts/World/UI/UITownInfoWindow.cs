﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEngine.UI;

public class UITownInfoWindow : MonoBehaviour {

    //외부 참조
    WorldManager _WorldManager;

    //내부 참조
    public UnitTown _OpendCurrTown; //현재 열려있는 타운
    public int _CurrInvestmentAmount; //현재 투자하려고 하는 금액

    public Text _TradeLvText;
    public Text _InvestedCompanyContextText;
    public Text _CurrInvestmentAmountText;

    public GameObject _TaxAdjustRoot; //세금 조절 부. 유저가 모노폴리 길드의 지분 리더일 경우만 나온다.
    public Text _DividendText;  //세금이 모여서 저장된걸 보여준다. 
    public Text _CurrTaxRateText;  //현재 세율


    public void KickStart()
    {
        _WorldManager = GetComponentInParent<WorldManager>();

        _OpendCurrTown = _WorldManager._PlayerUnitStarter._CurrUnitTown;
        _CurrInvestmentAmount = 0;

        _TaxAdjustRoot.SetActive(false);
        if (_OpendCurrTown._MonopolyCompany != null && _WorldManager._PlayerUnitStarter._CurrCompany!=null ) 
        {
            if(_OpendCurrTown._MonopolyCompany == _WorldManager._PlayerUnitStarter._CurrCompany)
            {
                //나중에 리더임을 필터링해주는 조건을 추가해야 한다.

                _TaxAdjustRoot.SetActive(true);
                _CurrTaxRateText.text = (int)(_OpendCurrTown._TaxRate*100) + " %";
                if (_OpendCurrTown._MonopolyCompany._TaxResidualRevenuePerTownDic.Keys.Contains(_OpendCurrTown))
                {
                    _OpendCurrTown._TotalDividendAmountInTown = _OpendCurrTown._MonopolyCompany._TaxResidualRevenuePerTownDic[_OpendCurrTown];
                }
                else
                {
                    _OpendCurrTown._TotalDividendAmountInTown = 0;
                }
                _DividendText.text = _OpendCurrTown._TotalDividendAmountInTown + "";
            }
        }

        RefreshDisplay();


    }




    public void RefreshDisplay()
    {
        _TradeLvText.text = "Lv." + _OpendCurrTown._CurrTownCommerceGrade + "  " + _OpendCurrTown._TotalInvestmentAmount + " /" + _WorldManager._TownExpTable[_OpendCurrTown._CurrTownCommerceGrade];

        _CurrInvestmentAmountText.text = _CurrInvestmentAmount + "";

        string _context = "";

        //_OpendCurrTown._TownStockInvestmentDic....   디셔너리 정렬 후.. 한다.
        for (int i=0; i< _OpendCurrTown._TownStockInvestmentDic.Keys.Count; i++)  //순위 표기해준다.
        {
            _context+= (_OpendCurrTown._TownStockInvestmentDic.Keys.ToList()[i]._Name + " " + _OpendCurrTown._TownStockInvestmentDic.Values.ToList()[i] +" ( "+ (int)(_OpendCurrTown._TownStockInvestmentDic.Values.ToList()[i]*100/_OpendCurrTown._TotalInvestmentAmount)+"% )" );

            _context += "\n";
        }
        _InvestedCompanyContextText.text = _context;

    }


    public void InvestmentToTownByPlayer() //투자하는 버튼
    {
        if (_WorldManager._PlayerUnitStarter._CurrCompany != null)
        {
            if (_WorldManager._PlayerUnitStarter._Gold >= _CurrInvestmentAmount)
            {
                _WorldManager.InvestmentToTown(_OpendCurrTown, _WorldManager._PlayerUnitStarter._CurrCompany, _CurrInvestmentAmount, _WorldManager._PlayerUnitStarter);
                _CurrInvestmentAmount = 0;
            }
        }
        RefreshDisplay();

    }

    public void UIClearBTN()
    {
        _CurrInvestmentAmount = 0;
        _CurrInvestmentAmountText.text = _CurrInvestmentAmount + "";

    }

    public void UpPercentBTN(float _PlusPercent )
    {
        _WorldManager.CalculateTotalInvestmentAmount(_OpendCurrTown);

        int _UpAmount = (int)(_OpendCurrTown._TotalInvestmentAmount * _PlusPercent);

        if (_WorldManager._PlayerUnitStarter._Gold >= _CurrInvestmentAmount + _UpAmount)
        {
            _CurrInvestmentAmount += _UpAmount;
        }
        else
        {
            _CurrInvestmentAmount = _WorldManager._PlayerUnitStarter._Gold;
        }

        _CurrInvestmentAmountText.text = _CurrInvestmentAmount + "";

    }



    public void UpTaxRateBTN()
    {
        if (_OpendCurrTown._TaxRate+0.01f <= 0.5f)
        {
            _OpendCurrTown._TaxRate += 0.01f;
        }
        _CurrTaxRateText.text = (int)(_OpendCurrTown._TaxRate * 100) + " %";

    }

    public void DownTaxRateBTN()
    {
        if (_OpendCurrTown._TaxRate - 0.01f >= 0f)
        {
            _OpendCurrTown._TaxRate -= 0.01f;
        }
        _CurrTaxRateText.text = (int)(_OpendCurrTown._TaxRate * 100) + " %";

    }

}
