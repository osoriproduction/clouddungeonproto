﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;


public class UICharWindow : MonoBehaviour {

    WorldManager _WorldManager;
    public UnitStarter _CalledStarter;
   

    //left side
    public GameObject _PortraitRoot;
    public GameObject _HPBar;
    public GameObject _MPBar;
    public Text _textHP;
    public Text _textMP;
    public Text _textClass;
    public Text _textName;
    public Text _textLv;

    //deatil Side
    public GameObject _ContentInfoRoot;
    public UIUnitAttribute _UIUnitAttributePrefab;

    //Join Suggest Side
    public int _CostForJoinAttemp;  //요거 하나로 일단, 조인 제안 테스트



    private void Update()
    {
        if (_CalledStarter != null)
        {
            if (_CalledStarter._CurrUnitTown != _WorldManager._PlayerUnitStarter._CurrUnitTown)
            {
                CloseUI();
            }
        }
    }

    public void KickStart () {
        _WorldManager = GetComponentInParent<WorldManager>();
        _UIUnitAttributeList = new List<UIUnitAttribute>();

        if (_CalledStarter != null)  //얘는 메소드 밖에서 사전적으로 참조해주고 들어와야 한다.
        {
            _textName.text = _CalledStarter._Name;

            _HPBar.transform.localScale = new Vector3(_CalledStarter._currentHP / _CalledStarter._maxHP, 1, 1);
            _MPBar.transform.localScale = new Vector3(_CalledStarter._currentMP / _CalledStarter._maxMP, 1, 1);
            _textHP.text = (int)_CalledStarter._currentHP + "/ " + (int)_CalledStarter._maxHP;
            _textMP.text = (int)_CalledStarter._currentMP + "/ " + (int)_CalledStarter._maxMP;
            _textLv.text = "Lv. " + _CalledStarter._TradeLevel + "  " + (_CalledStarter._CurrTradeExp - _WorldManager._StarterManager._ExpTable[_CalledStarter._TradeLevel - 1]) + "/ " + (_WorldManager._StarterManager._ExpTable[_CalledStarter._TradeLevel] - _WorldManager._StarterManager._ExpTable[_CalledStarter._TradeLevel - 1]);

            DrawDetailnfos();

            _CostForJoinAttemp = Mathf.Max( (_CalledStarter._TradeLevel-_WorldManager._PlayerUnitStarter._TradeLevel) * 1000,500) +(int)(( _CalledStarter._StatTradeInformationStrength + _CalledStarter._StatTradeWillingness + _CalledStarter._StatTradeJudgement)* _CalledStarter._TradeLevel);  //고용에 필요한 금액

        }
        else
        {
            CloseUI();
        }
    }

    public void DrawDetailnfos()
    {
        for(int i = 0; i < _UIUnitAttributeList.Count; i++)
        {
            Destroy(_UIUnitAttributeList[i].gameObject);

        }
        _UIUnitAttributeList.Clear();

        int _posY = -100;
        DrawDetailUnitInfo("레벨", _CalledStarter._TradeLevel+"" , _posY);
        _posY += -70;

        if (_CalledStarter._CurrCompany != null)
        {
            DrawDetailUnitInfo("소속길드", _CalledStarter._CurrCompany._Name + "", _posY);
            _posY += -70;

        }
        else
        {
            DrawDetailUnitInfo("소속길드",  "없음", _posY);
            _posY += -70;
            
            DrawDetailUnitInfo("스카웃 비용", (int)_CostForJoinAttemp + "", _posY);
            _posY += -70;
        }

        _posY += -30;

        DrawDetailUnitInfo("소지골드", (int)_CalledStarter._Gold + "", _posY);
        _posY += -70;

        DrawDetailUnitInfo("재고금액", (int)CalculateCurrInventoryCost() + "", _posY);
        _posY += -70;

        _posY += -30;

        DrawDetailUnitInfo("정보력", _CalledStarter._StatTradeInformationStrength + "", _posY);
        _posY += -70;

        DrawDetailUnitInfo("의지", _CalledStarter._StatTradeWillingness + "", _posY);
        _posY += -70;

        DrawDetailUnitInfo("판단력", _CalledStarter._StatTradeJudgement + "", _posY);
        _posY += -70;

        DrawDetailUnitInfo("속도", (int)(_CalledStarter._MoveSPD*100) + "", _posY);
        _posY += -70;




    }

    public List<UIUnitAttribute> _UIUnitAttributeList; //여기 모아두었다가 나중에 한번에 정리
    public void DrawDetailUnitInfo(string _stringheader, string _stringcontext , float _posY )
    {
        UIUnitAttribute tempAttri = Instantiate(_UIUnitAttributePrefab, _ContentInfoRoot.transform);
        tempAttri.transform.localPosition = new Vector3(150, _posY, 0);
        tempAttri.TextHead.text = _stringheader;
        tempAttri.TextContext.text = _stringcontext;
        _UIUnitAttributeList.Add(tempAttri);
    }


    public void CallUI(UnitStarter _calledStarter)
    {
        _CalledStarter = _calledStarter;


        /*
        if (_CalledStarter != null)
        {

            if (_CalledStarter._classType == 1)
            {
                _textClass.text = "Fighter";
            }
            else if (_CalledStarter._classType == 2)
            {
                _textClass.text = "Archer";
            }
            else if (_CalledStarter._classType == 3)
            {
                _textClass.text = "Wizard";
            }
            else if (_CalledStarter._classType == 4)
            {
                _textClass.text = "Healer";
            }
            else if (_CalledStarter._classType == 5)
            {
                _textClass.text = "Assassin";
            }
        }*/
    }

    public void CloseUI()
    {
        _CalledStarter = null;
        this.gameObject.SetActive(false);

    }



    public int CalculateCurrInventoryCost()
    {
        int _return = 0;
        for( int i=0; i< _CalledStarter._currGoodsCostInCarry.Keys.Count; i++)
        {
            _return += _CalledStarter._currGoodsCostInCarry.ToList()[i].Value;

        }

        return _return;
    }


    public void JoinOurCompany()
    {
        if (_CalledStarter._CurrCompany == null && _WorldManager._PlayerUnitStarter._CurrCompany!=null)
        {
            if (_WorldManager._PlayerUnitStarter._Gold >= _CostForJoinAttemp)
            {
                _WorldManager._PlayerUnitStarter._Gold -= _CostForJoinAttemp;

                _CalledStarter._CurrCompany = _WorldManager._PlayerUnitStarter._CurrCompany;
                
            }

        }
    }



}
