﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitItemObject : MonoBehaviour {

    WorldManager _WorldManager;

    public UnitGoods _UnitGood;//현재 참조된 보통의 구즈

    public SpriteRenderer _SpriteReneder;
    
    //속성
    public bool _IsCoin;//코인일 경우 코인 취급한다.
    public int _CoinGold; //얼마나 들고 있는지

	void Start () {
        _WorldManager = GetComponentInParent<WorldManager>();
        
    }
	

}
