﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using HeroEditor.Common;
using HeroEditor.Common.Enums;
using Assets.HeroEditor.Common.CharacterScripts;

public class StarterObjectInPubPrefab : MonoBehaviour {

    WorldManager _WorldManager;
    public UnitStarter _UnitStarter;
    public Character _Character;
    public LayerManager _LayerManager;

    private void Start()
    {
        _WorldManager = GetComponentInParent<WorldManager>();
        _LayerManager = GetComponentInChildren<LayerManager>();
        ReOrderLayer();

        StartCoroutine(WalkAround());
    }

    public void CallThisUICharWindow()
    {
        _WorldManager._UICharWindow._CalledStarter = _UnitStarter;
        _WorldManager._UIMenuBtnController.CallCharWindow();

    }


    void ReOrderLayer()
    {
        _LayerManager.SortingOrderOffset = 5000;
        _LayerManager.SetOrderBySortingOrder();
        
    }

    IEnumerator WalkAround()
    {
        for(; ; )
        {

            yield return MoveCharacterToHereIEnumVec(new Vector3(Random.Range(-20f, 20f), 0, 0));

            yield return new WaitForSeconds(Random.Range(5, 10));

        }
    }



    IEnumerator MoveCharacterToHereIEnumVec( Vector3 _targetVec)
    {
        float _AudjustedMoveSPD = 0.5f;

        _Character.Animator.SetBool("Run", true);

        _targetVec = new Vector3(_targetVec.x + this.transform.position.x , this.transform.position.y, this.transform.position.z);

        if (_targetVec.x >= this.transform.position.x)
        {
            _Character.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        }
        else
        {
            _Character.transform.localScale = new Vector3(-0.5f, 0.5f, 0.5f);
        }

        for (int i = 0; i < 10000; i++)
        {
            yield return new WaitForEndOfFrame();

            this.transform.position += (_targetVec - this.transform.position).normalized * _AudjustedMoveSPD;
            if (DistBetweenPointR(_targetVec, this.transform.position) < 0.5f)
            {
                break;
            }
        }


        _Character.Animator.SetBool("Run", false);
        _Character.Animator.SetBool("Ready", true);
        _Character.Animator.SetBool("Angry", false);

    }


    public float DistBetweenPointR(Vector3 _target, Vector3 _refer)
    {
        return (_target.x - _refer.x) * (_target.x - _refer.x) + (_target.y - _refer.y) * (_target.y - _refer.y);
    }
}
