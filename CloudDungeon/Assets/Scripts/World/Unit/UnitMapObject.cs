﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitMapObject : MonoBehaviour {

    //외부참조
    WorldManager _WorldManager;


    //시스템
    public UnitTown _CurrUnitTown;  //실제 지니고 있는 마을
    public Text _TextTownName; //마을 이름 

	void Start () {
        _WorldManager = GetComponentInParent<WorldManager>();
    }

    public void GoAutoMove()
    {

        _WorldManager._UIWorldMapWindow.gameObject.SetActive(false);
        _WorldManager._PlayerUnitStarter._walkedWayObjectList = _WorldManager._PlayerUnitStarter.GetWayListFromWayManger(_WorldManager._PlayerUnitStarter._CurrOnTheTileObject._CurrUnitTile, _CurrUnitTown._currUnitTile); //리스트에서 검색해보고, 없으면 내가 찾아간다.
        if (_WorldManager._PlayerUnitStarter._walkedWayObjectList == null)
        {
            Debug.Log("null 임..");
            _WorldManager._PlayerUnitStarter._walkedWayObjectList = _WorldManager._PlayerUnitStarter.FindWayByAStar(_CurrUnitTown._currUnitTile);
        }
        _WorldManager._PlayerUnitStarter.SailToTheTile(_CurrUnitTown._currUnitTile); //이동 시작.

        //_WorldManager.DrawCommandTile(false, false);
    }

 
}
