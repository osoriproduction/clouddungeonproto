﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using HeroEditor.Common;
using HeroEditor.Common.Enums;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Assets.HeroEditor.Common.CharacterScripts;


public class UnitStarter : MonoBehaviour {
           
    //외부 참조
    public WorldManager _WorldManager;
    StarterManager _StarterManager;
    WayManager _WayManager;
    public Character _Character;
    public UnitTileObject[] _boundaryTiles;
    WeaponControls _WeaponControls;
    KeyCode _tabBTN;
    public GameObject _AnimationObject;
    public GameObject _Pelvis;
    public GameObject _PelvisArmor;

    public UICharInfo _UICharInfo;  //주인공 캐릭터에게만 일단..

    //시스템
    public List<UnitTileObject> _walkedWayObjectList;   //걸어갈 길
    public string targetTownTileName;  //목적지 타일 명
    public UnitTown _LastTradedTown; //마지막에 거래한 타운.
    public bool _IsPlayerChar;

    public float _CurrCarryWeight; // 현재 소지 무게
    public Dictionary<UnitGoods, int> _currGoodsInCarry;  // 들고 있는 물건들과 그 개수
    public Dictionary<UnitGoods, int> _currGoodsCostInCarry;  // 들고 있는 물건들의 구입 원가 정보

    public List<float> _EstimatedAveragePriceRateList;  //스타터가 추정한 현재 전세계의 평균 가격 리스트

    public UnitTileObject _CurrOnTheTileObject;
    public UnitTileObject _BeforeTileObject;
    public UnitTown _CurrUnitTown;

    public float _AudjustedMoveSPD; //여러 변수에 의해 조정된 이동 속도
    public int _CurrHitAvailableCount; //한번에 때릴 수 있는 카운트 수
    public int _dir;  //현재 움직이는 방향
    const float constGravity = -0.35f;  //중력 가속도

    public UnitPerson _UnitPerson;  //외모 클래스 등 기본 정보
    public float _currentHP;
    public float _currentMP;
    public bool _IsMonster;  //사냥 당할 수 있는 자코 몬스터인지
    public List<UnitStarter> _CurrHostileStarterList; //현재 적대하고 있는 스타터들

    public bool _StunATKAvailable;  //공격 시 스턴시킬 수 있는 어택 충전 여부.

    //장비 장착
    public UnitGoods[] _EquippedItems;

    // 0은 주무기   
    //1은 쉴드 혹은 보조 무기 (양손일 경우, 0 과 1에 다가지고 있음),  
    //2는 아머,   
    //3은 헬멧 
    //4는 마스크 
    //5는 글래스
    //6은 케이프
    //7은 등

    public int _EquippmentATK;  //장비 착용 공격력
    public int _EquippmentDEF; //장비 착용 방어력





    //속성

    public int _Gold;  //스타터의 소지 골드량
    public string _Name;  //스타터 이름 

    public int _CurrTradeExp;   // 스타터 트레이드  경험치
    public int _TradeLevel;   //스타터의 트레이드 레벨

    public int _CurrWarExp;  //스타터 전투 경험치
    public int _WarLevel;  //스타터 전투 레벨

    public UnitTown _NativeBornHomeTown; //고향 타운
    public UnitCompanay _CurrCompany;  //현재 속해있는 컴퍼니 . 없으면 null이다.


    public float _maxHP;    //STR
    public float _maxMP;   //CON   SP이기도 하다. 체력이므로, 왔다갔다 하면 떨어진다. 다 떨어지면 쉰다. 타운에 머무르면 회복량에 따라 찬다.
    public float _MaxCarryWeight; // 들 수 있는 최대 무게   STR

    public float _StatTradeWillingness;  //트레이드 의지력, 추진력      장사 의지    CON + WIS 
    public float _StatTradeJudgement;  //트레이드 판단력                 장사 판단력     WIS + SENSE
    public float _StatTradeInformationStrength;   //트레이드 정보력      장사 정보력  INT + SENSE

    public int _StatSTR;
    public int _StatCON;
    public int _StatDEX;
    public int _StatINT;
    public int _StatWIS;
    public int _StatSENSE;

    public float _StatPhysicsATK; //물리 공격력 ( STR +  무기 합 ) * LV
    public float _StatMagicATK; // 마법 공격력 ( INT + 마법 무기 합 ) * LV
    public float _StatPhysicsArmor; // 물리 방어력 ( DEX + 방어구 합)*LV
    public float _StatMagicArmor; //마법 방어력( WIS + 방어구 합 )*LV

    public int _StatHitAvailableCount; //한번에 때릴 수 있는 카운트 수. 장비 종류에 따라 달라짐

    public float _ATKCoolTime; //공격 쿨타임 (DEX 연관)
    public float _MoveSPD; //(DEX 연관)
    public float _SearchingRange; //인식범위같은거

    //상태 값
    public int _state; //1:idle   2:deathing  3:moving
    public string _currWork;   //"TRADE" ,"PLUNDER" 스타터의 직업이나 정체성 
    public string _actionState; //  "WAITING",  "TRADING", "PUBBING" , "ROAMING" ,  "ENGAGING"   현재 하고 있는 행위



    void Start()
    {
        _Character = GetComponentInChildren<Character>();
        _WorldManager = GetComponentInParent<WorldManager>();
        _WayManager = _WorldManager.gameObject.GetComponent<WayManager>();
        _StarterManager = _WorldManager._StarterManager;
        _WeaponControls = _Character.transform.GetComponent<WeaponControls>();
        _tabBTN = KeyCode.Mouse0;
        _Pelvis = _Character.transform.Find("Animation/Pelvis").gameObject;
        _AnimationObject = _Character.transform.Find("Animation").gameObject;
        _PelvisArmor = _Character.transform.Find("Animation/Pelvis/Pelvis[Armor]").gameObject;
        _currGoodsInCarry = new Dictionary<UnitGoods, int>();
        _currGoodsCostInCarry = new Dictionary<UnitGoods, int>();
        _EquippedItems = new UnitGoods[8] {null, null, null, null, null, null, null, null };//장비를 초기화시켜준다.  0은 주손, 1은 부손, 2는 아머, 3은 헬멧

        _state = 1;
        if (!_IsMonster)
        {
         
            _currWork = "TRADE";
     
            _actionState = "WAITING";
        }
        else
        {
            _currWork = "PLUNDER";
            _actionState = "WAITING";
        }

        //임시 속성 설정  이 아래는 나중에 다 불러오는 함수로 빼버린다.
        _TradeLevel = 1;

        if (!_IsMonster)
        {
            _WarLevel = 1;
            _Gold = 2000;
            //이하의 기본 스탯은 레벨업에 따라 바뀌지 않는다.
            _StatSTR =(int)Random.Range(7,18);   
            _StatCON = (int)Random.Range(7, 18);
            _StatDEX = (int)Random.Range(7, 18);
            _StatINT = (int)Random.Range(7, 18);
            _StatWIS = (int)Random.Range(7, 18);
            _StatSENSE = (int)Random.Range(7, 18);
            
        }
     

        DecideBySTAT();

        if (_IsMonster)
        {
            _maxMP = 99999;
        }


        //if (_IsPlayerChar)  //test
        //{
        //    BuyGoodsToCarryStock(_WorldManager._EconomyManager._ProtoGoodsList[0], 1,100);
        //    BuyGoodsToCarryStock(_WorldManager._EconomyManager._ProtoGoodsList[1], 2, 200);

        //}

        //시스템 초기화
        _StatHitAvailableCount = 1;

        _currentMP = _maxMP;
        _currentHP = _maxHP;

        StartCoroutine(Init());
        
    }

    private void Update()
    {
        if (_Character.transform.localScale.x >= 0)
        {
            _dir = 1;
        }
        else
        {
            _dir = -1;
        }
    }


    IEnumerator Init()
    {
        yield return new WaitForSeconds(0.03f);
       
        int tempNameNum = Random.Range(0, _StarterManager._StarterNamePool.Count);   //이름을 정하자.
        if (!_IsMonster)
        {
            _Name = _StarterManager._StarterNamePool[tempNameNum];
            _StarterManager._StarterNamePool.RemoveAt(tempNameNum);
            InitSetHomeTown();  //고향을 정하고 거기로 이동  임시.
            InitSetCompany(); //컴파니 소속 여부를 정한다. 임시.

        }

         _WorldManager._StarterManager._CurrStartersList.Add(this);  //현재 스타터 리스트에 이 스타터를 넣어준다.

        yield return new WaitForSeconds(0.13f);

        FindMyLocatedTileObject();  //현재 위치를 확인하고, 로직을 시작한다.

        _EstimatedAveragePriceRateList = new List<float>();
        _EstimatedAveragePriceRateList = _WorldManager._EconomyManager.CalculateAveragePriceList();
             
        AIBrainRef = AIBrain();
        if (!_IsPlayerChar)
        {
            StartCoroutine(AIBrainRef);
        }


        //스타터 바디에 컬라이더 생성
        BoxCollider _Collider =  _Character.gameObject.AddComponent<BoxCollider>();
        _Collider.size = new Vector3(100, 180, 10);
        _Collider.center = new Vector3(0, 100, 0);
        _Collider.isTrigger = true;
        _Character.gameObject.tag = "StarterBody";  //사전 정의 필수

    }


    public void DecideBySTAT()  //기초 스탯들을 참고하여 고급 수치값들을 정해준다.
    {

        _maxHP = 100 +_WarLevel * (_StatSTR + _StatCON) * 1;   //STR
        _maxMP = 30 + (_TradeLevel + _WarLevel) * _StatCON * 1;   //CON   SP이기도 하다. 체력이므로, 왔다갔다 하면 떨어진다. 다 떨어지면 쉰다. 타운에 머무르면 회복량에 따라 찬다.
        _MaxCarryWeight = 20 + (_WarLevel + _TradeLevel) * _StatSTR; // 들 수 있는 최대 무게   STR

        _StatTradeWillingness = _StatSTR * 2 + _StatWIS * 3 + _TradeLevel * 5;  //트레이드 의지력, 추진력      장사 의지    CON + WIS 
        _StatTradeJudgement = _StatWIS * 2 + _StatSENSE * 3 + _TradeLevel * 5;  //트레이드 판단력                 장사 판단력     WIS + SENSE
        _StatTradeInformationStrength = _StatINT * 3 + _StatSENSE * 2 + _TradeLevel * 5;   //트레이드 정보력      장사 정보력  INT + SENSE

        _StatPhysicsATK = (_StatSTR + _EquippmentATK) * _WarLevel; //물리 공격력 ( STR +  무기 합 ) * LV
        _StatMagicATK = (_StatINT + _EquippmentATK) * _WarLevel; // 마법 공격력 ( INT + 방어구 합 ) * LV
        _StatPhysicsArmor = (_StatDEX + _EquippmentDEF) * _WarLevel;
        _StatMagicArmor = (_StatWIS ) * _WarLevel;

        if (_Character != null)
        {

            if (_Character.WeaponType == WeaponType.Melee1H)
            {
                _StatHitAvailableCount = 1; //한번에 때릴 수 있는 카운트 수. 장비 종류에 따라 달라짐

            }
            else if (_Character.WeaponType == WeaponType.Melee2H)
            {
                _StatHitAvailableCount = 5; //한번에 때릴 수 있는 카운트 수. 장비 종류에 따라 달라짐

            }
            else if (_Character.WeaponType == WeaponType.MeleePaired)
            {
                _StatHitAvailableCount = 2; //한번에 때릴 수 있는 카운트 수. 장비 종류에 따라 달라짐

            }
        }
        else
        {
            _StatHitAvailableCount = 1;
        }
        _ATKCoolTime = 3 - _StatDEX * 0.1f; //공격 쿨타임 (DEX 연관)
        _MoveSPD = 0.5f + _StatDEX * 0.02f; //(DEX 연관)
        
    }



    public void InitSetHomeTown()
    {

        int tempHomeTownNum = (int)Random.Range(0, _WorldManager._CurrTownList.Count);

        _NativeBornHomeTown = _WorldManager._CurrTownList[tempHomeTownNum];
        _NativeBornHomeTown._NativedStarters.Add(this);

        this.transform.position = _NativeBornHomeTown._currUnitTile._unitTileObject.transform.position;
        
    }

    public void InitSetCompany()
    {
        int tempCompanyNum;
        if (Random.Range(0, 100) < 10 && !_IsPlayerChar)
        {
            tempCompanyNum = (int)Random.Range(1, _WorldManager._CompanyManager._CurrCompanyList.Count);
            _CurrCompany = _WorldManager._CompanyManager._CurrCompanyList[tempCompanyNum];   //컴퍼니도 수동으로 지정해줘야 한다.
            _WorldManager._CompanyManager.AddStarterToCompany(_CurrCompany, this, 3000);
            
        }   

    }


    public IEnumerator AIBrainRef;
    IEnumerator AIBrain()
    {
        yield return new WaitForSeconds(Random.Range(0.1f, 0.3f));

        if (_IsPlayerChar)  //유저의 경우, 출발 전에 거래창을 닫아준다. 
        {
            _WorldManager._UIMenuBtnController.CloseUITradePurchase();
            _WorldManager._UIMenuBtnController.CloseUIWorldMap();

        }

        for (; ; )
        {

            yield return new WaitForSeconds(Random.Range(0.1f, 0.3f));

            if (_currWork == "TRADE")
            {
                //-------------------휴식 행위  ----------------------------------------------
                if (_currentMP < _maxMP * 0.3f && _CurrUnitTown != null && !_IsPlayerChar)
                {
                    _actionState = "PUBBING";  //잠깐 주점에서 쉬다가 가자 ... ㅎ
                    yield return new WaitForSeconds(Random.Range(15, 30));

                    _actionState = "WAITING";   //이제 준비되었다.
                }
                

                //-------------------컴퍼니 자동 가입 로직  ----------------------------------------------
                if (Random.Range(0, 100) < 5)  
                {
                    if (_CurrCompany == null && !_IsPlayerChar && _Gold > 5000)
                    {
                        UnitCompanay _tempCom = _WorldManager._CompanyManager._CurrCompanyList[(int)Random.Range(1, _WorldManager._CompanyManager._CurrCompanyList.Count)];
                        if (_WorldManager._PlayerUnitStarter._CurrCompany != null)
                        {
                            if (_tempCom != _WorldManager._PlayerUnitStarter._CurrCompany)
                            {
                                _WorldManager._CompanyManager.AddStarterToCompany(_tempCom, this, 3000);
                                _Gold -= 3000;
                                _CurrCompany = _tempCom;
                            }
                        }
                        else
                        {
                            _WorldManager._CompanyManager.AddStarterToCompany(_tempCom, this, 3000);
                            _Gold -= 3000;
                            _CurrCompany = _tempCom;
                        }
                    }
                }

                //-------------------매매행위  ----------------------------------------------


                if (_actionState == "WAITING")  //이동 트레이딩 등의 상태를 의미한다.
                {
                    _actionState = "TRADING";

                    if (_CurrUnitTown != null&&!_isHitFlying && !_isStunned )
                    {
                        yield return StartCoroutine(FindAndSellAction()); //일단 가지고 있는 게 있으면 처분하는 행위를 한다.
                    }
                    yield return new WaitForSeconds(2);

                    if (_CurrUnitTown != null && !_isHitFlying && !_isStunned)
                    {
                        yield return StartCoroutine(FindAndBuyAction());  //현재 마을에서 구입 행위를 한다. 
                    }
                    yield return new WaitForSeconds(2);

                    if (_CurrUnitTown != null && !_isHitFlying && !_isStunned)
                    {
                        if (_CurrCompany != null)
                        {
                            if(_Gold> 5000)
                            {
                                if(Random.Range(0,2000000) < _StatTradeJudgement * _StatTradeWillingness * _StatTradeInformationStrength)  //조건 체크하여 투자 진행
                                {
                                    _WorldManager.InvestmentToTown(_CurrUnitTown, _CurrCompany, (int)((_Gold - 5000) * Random.Range(0.2f, 0.5f)), this );
                                }
                            }
                        }
                    }
                    //이제 팔 마을을 찾아서 출발해야겠지.                    
                    UnitTown tempTown = null;
                    if (_CurrUnitTown != null)
                    {
                        tempTown = FindGoodTownToTrade();
                    }

                    if (tempTown == null)
                    {
                        //Debug.Log("거래할 만한 타운이 없네.." + _WorldManager._CurrTownList.Count);
                        if (_IsPlayerChar)
                        {
                            tempTown = _WorldManager._DiscoveredTownList[Random.Range(0, _WorldManager._DiscoveredTownList.Count)];

                        }
                        else
                        {
                            tempTown = _WorldManager._CurrTownList[Random.Range(0, _WorldManager._CurrTownList.Count)];

                        }

                    }
                    else
                    {
                        //Debug.Log("거래할 마을을 찾았다.." + tempTown._name);

                    }

                    targetTownTileName = tempTown._currUnitTile._unitTileObject.name;

                    if (_CurrOnTheTileObject._CurrUnitTile != tempTown._currUnitTile)  //찾아가고자 하는 곳이 지금 여기랑 같지 않을 경우만 수행
                    {
                        _walkedWayObjectList = GetWayListFromWayManger(_CurrOnTheTileObject._CurrUnitTile, tempTown._currUnitTile); //리스트에서 검색해보고, 없으면 내가 찾아간다.
                        if (_walkedWayObjectList == null)
                        {
                            Debug.Log( _Name + "__walkedWayObjectList is null");
                            _walkedWayObjectList = FindWayByAStar(tempTown._currUnitTile);
                        }

                        yield return new WaitForSeconds(Random.Range(1.5f, 3));
                        if (_CurrUnitTown != null && !_isHitFlying && !_isStunned)
                        {
                            yield return SailToTheTileIENum(tempTown._currUnitTile); //이동 시작. 
                        }
                    }
                    else {

                    }
                    yield return new WaitForSeconds(Random.Range(0.5f, 1.2f));
  
                    _actionState = "WAITING";
                }
            }         
            else if (_currWork == "PLUNDER")
            {
                if (_actionState == "WAITING")
                {
                    //roaming시킨다.

                    yield return RoamingAroundAndPlunderingIENum();


                }              


            }

        }
    }

    public void KickStartPlayerAIBrain()  //유저 캐릭터의 aibrain을 돌린다.
    {
        _actionState = "WAITING";

        AIBrainRef = AIBrain();        
        StartCoroutine(AIBrainRef);
        
    }

    public void StopPlayerAIBrain()
    {
        StopCoroutine(AIBrainRef);
        StopCoroutine("FindAndSellAction");
        StopCoroutine("FindAndBuyAction");
        StopCoroutine("SailToTheTileIENum");
        _actionState = "WAITING";
        
    }

    // 트레이더 관련한 영역

    public IEnumerator FindAndSellAction()  //한번 현재 마을을 쫙 훑은 후에, 거기서 판매 행위를 한다. 
    {
        yield return new WaitForSeconds(0.1f);

        if (Random.Range(0, 130) < _StatTradeInformationStrength)  //일정 정보력 이상이면, 평균 가격을 새로 갱신해서 가져온다. 안되면 예전꺼 그냥 써야지 무.
        {
            _EstimatedAveragePriceRateList = new List<float>();
            _EstimatedAveragePriceRateList = _WorldManager._EconomyManager.CalculateAveragePriceList();
        }

        float tempUnitRevenueRate = 0;   // 시세차이가 이득인지
        UnitGoods _chosenGoods = null;
        
        while (Random.Range(0, 100) < _StatTradeWillingness &&Random.Range(0,100)<95  )  //어떤 조건 하에 활동을 반복할 것인가.
        {
            int SellPrice;
            int SellCount;

            for (int i = 0; i < _currGoodsInCarry.Keys.Count; i++)  //내 포켓의 상품들을 하나씩 돌면서, 시세 차이를 구한 후, 이득이 나면 일단 다 팔아버리자.  나중에 고도화하고
            {
                tempUnitRevenueRate = _currGoodsInCarry.Keys.ToList()[i]._Price * (_CurrUnitTown._CurrPriceRateList[_currGoodsInCarry.Keys.ToList()[i]._Group] - _EstimatedAveragePriceRateList[_currGoodsInCarry.Keys.ToList()[i]._Group]);
                if (tempUnitRevenueRate > 0)
                {
                    _chosenGoods = _currGoodsInCarry.Keys.ToList()[i];
                    
                    SellPrice = (int)(_chosenGoods._Price * _CurrUnitTown._CurrPriceRateList[_chosenGoods._Group]);
                                        
                    SellCount = _currGoodsInCarry[_chosenGoods];  //내가 팔려고 하는게 내 가방에 것 만큼이다. 
                    _Gold += SellPrice * SellCount;
                    
                    _CurrTradeExp += (SellPrice - _currGoodsCostInCarry[_chosenGoods]) * SellCount;   //경험치 획득 행위 (이득 발생시만 처리)
                    SellGoodsFromCarryStock(_chosenGoods, SellCount);
                    
                    _CurrUnitTown._CurrPriceRateList[_chosenGoods._Group]   //구입한 만큼 해당 마을의 물가가 변동한다.
                        = _CurrUnitTown._CurrPriceRateList[_chosenGoods._Group]
                        * (1 - (SellCount * SellPrice) / (_CurrUnitTown._CurrTownCommerceGrade * 100000f));
                        
                }
            } 
        }

        _StarterManager.LevelDeciderForTrade(this);
    }


    public IEnumerator FindAndBuyAction()  //한번 현재 마을을 쫙 훑은 후에, 거기서 구입 행위를 한다. 
    {
        yield return new WaitForSeconds(0.1f);

        float _CompanyDiscountRate=0;  //이건 구입시 할인 율. 컴퍼니 독점일때..

        if (_CurrCompany != null)
        {
            if(_CurrCompany == _CurrUnitTown._MonopolyCompany)
            {
                _CompanyDiscountRate = _CurrUnitTown._TaxRate;
            }
        }


        if (Random.Range(0, 130) < _StatTradeInformationStrength)  //일정 정보력 이상이면, 평균 가격을 새로 갱신해서 가져온다. 안되면 예전꺼 그냥 써야지 무.
        {
            _EstimatedAveragePriceRateList = new List<float>();
            _EstimatedAveragePriceRateList = _WorldManager._EconomyManager.CalculateAveragePriceList();
        }
        float mostHighUnitRevenue = 0;   //제일 싼 시세 레이트
        int mostHighUnitRevenueGoodsGroupNum = 999;    //제일 구입할법한 상품 그룹 번호
        float tempUnitRevenue = 0;   // 기준가격*시세차이/무게
        UnitGoods _chosenGoods = null;

        while (_Gold > 0 && Random.Range(0, 100) < _StatTradeWillingness && Random.Range(0, 100) < 95)  //어떤 조건 하에 조사 및 구입 활동을 반복할 것인가. 일단 골드만..
        {
            foreach (var aGoods in _CurrUnitTown._CurrGoodsDic)   //마을에 있는 상품들을 하나씩 돌면서, 시세 차이를 구한 후,  제일 이득이 나는것을 계산한다. 
            {             
                tempUnitRevenue = aGoods.Key._Price 
                    * (_EstimatedAveragePriceRateList[aGoods.Key._Group] - (_CurrUnitTown._CurrPriceRateList[aGoods.Key._Group]+_CurrUnitTown._TaxRate -_CompanyDiscountRate - _WorldManager._EconomyManager._BuyPriceDiscountAdjustRate  ) ) 
                    / aGoods.Key._Weight;

                if (tempUnitRevenue > mostHighUnitRevenue)
                {
                    mostHighUnitRevenue = tempUnitRevenue;
                    mostHighUnitRevenueGoodsGroupNum = aGoods.Key._Group;
                    _chosenGoods = aGoods.Key;
                }
            }

            if (_chosenGoods != null && _CurrUnitTown._CurrGoodsDic.ContainsKey(_chosenGoods))
            {                
                int PurchasePrice = (int)(_chosenGoods._Price * (_CurrUnitTown._CurrPriceRateList[_chosenGoods._Group]+_CurrUnitTown._TaxRate - _CompanyDiscountRate - _WorldManager._EconomyManager._BuyPriceDiscountAdjustRate) );
                int PurchaseCount = Mathf.Min( _Gold / PurchasePrice, _CurrUnitTown._CurrGoodsDic[_chosenGoods] , (int)((_MaxCarryWeight-_CurrCarryWeight)/_chosenGoods._Weight ));
                // 골드기준, 해당 마을 잔여재고 기준, 무게 기준

                if (PurchaseCount > 0)
                {
                    _Gold -= PurchasePrice * PurchaseCount;
                    if (_CurrUnitTown._MonopolyCompany != null)
                    {
                        if (_CurrUnitTown._MonopolyCompany != _CurrCompany)  //현재 독점 기업과 나의 기업이 다르다면...
                        {
                            _WorldManager._CompanyManager.InsertTaxDividendToEachTownDic(_CurrUnitTown._MonopolyCompany, _CurrUnitTown, (int)(PurchasePrice * PurchaseCount * _CurrUnitTown._TaxRate));
                        }
                    }
                    BuyGoodsToCarryStock(_chosenGoods, PurchaseCount, PurchasePrice);
                    if (_CurrUnitTown._CurrGoodsDic[_chosenGoods] - PurchaseCount <= 0)
                    {
                        _CurrUnitTown._CurrGoodsDic[_chosenGoods] = 0;  //개수를 0으로 만들고, 
                        _CurrUnitTown._CurrGoodsDic.Remove(_chosenGoods);  //해당하는 품목을 빼준다.
                    }
                    else
                    {
                        _CurrUnitTown._CurrGoodsDic[_chosenGoods] -= PurchaseCount;   //구입한 만큼 마을에서 해당 개수를 빼준다.
                    }
                    _CurrUnitTown._CurrPriceRateList[_chosenGoods._Group]   //구입한 만큼 해당 마을의 물가가 변동한다.
                        = _CurrUnitTown._CurrPriceRateList[_chosenGoods._Group]
                        * (1 + (PurchaseCount * PurchasePrice) / (_CurrUnitTown._CurrTownCommerceGrade * 100000f));

                }
            }
        }

    }

    public void SellGoodsFromCarryStock(UnitGoods _sellGoods, int _sellCount)  //현재 캐릭터의 저장고에서 해당 물품을 빼준다.
    {
        if (_currGoodsInCarry.ContainsKey(_sellGoods))
        {
            if (_sellCount < _currGoodsInCarry[_sellGoods])  //내가 팔려고 하는게 가지고 있는것보다 적다.
            {
                _currGoodsInCarry[_sellGoods] -= _sellCount;

            }
            else  //내가 팔려고 하는게 가지고 있는것의 개수와 같거나, 더 많아. 그러면..  물론 더 많은 경우는 안나오게 앞에서 예외처리해주자. 골드랑 연계되어 있으니...
            {
                _currGoodsCostInCarry.Remove(_sellGoods);
                _currGoodsInCarry.Remove(_sellGoods);
                    
            }
        }

        _CurrCarryWeight -= _sellGoods._Weight * _sellCount;
               
    }

   public void BuyGoodsToCarryStock(UnitGoods _addGoods, int _addCount, int _addPrice)  //현재 캐릭터의 저장고에 해당 물품, 개수를 넣어준다.
    {
        if (_currGoodsInCarry.ContainsKey(_addGoods))
        {
            _currGoodsInCarry[_addGoods] += _addCount;
        }
        else
        {
            _currGoodsInCarry.Add(_addGoods, _addCount);
        }

        if (_currGoodsCostInCarry.ContainsKey(_addGoods))
        {
            _currGoodsCostInCarry[_addGoods] = (int)( (_currGoodsCostInCarry[_addGoods] * _currGoodsInCarry[_addGoods] + _addPrice * _addCount) / (_currGoodsInCarry[_addGoods] + _addCount) );

        }
        else
        {
            _currGoodsCostInCarry.Add(_addGoods, _addPrice);
        }

        _CurrCarryWeight += _addGoods._Weight * _addCount;

        //테스트 코드. 조건을 단 후에 입힌다.
        if (!_IsPlayerChar)
        {
            if (Random.Range(0, 100) < 20)  //나중에는 여기에 현재 장비 능력치 비교해서 갈아주는 걸로 하자.
            {
                EquipItem(_addGoods);
            }
        }
        

    }

    float _PriceAdjustor; //판매하는 곳에서는 반값에 사주는 것 반영.
    public UnitTown FindGoodTownToTrade()  //어느 마을이 제일 수익이 크려나? 판단하는 로직
    {
        FindMyLocatedTileObject();

        UnitTown _return = null;
        UnitTown _tempTown = new UnitTown() ;

        float _expectedRevenue = 0;

        float _MaxRevenuePerDist = 0;
        float _expectedRevenuePerDist = 0;

        List<UnitTown> _SearchingTownList = null;
        if ( _IsPlayerChar){ //플레이어의 경우 현재 발견한 마을로만 제한해준다.

            _SearchingTownList = _WorldManager._DiscoveredTownList;
        }
        else
        {
            _SearchingTownList = _WorldManager._CurrTownList;

        }

        if (_currGoodsInCarry.Count > 0)
        {
            for (int i = 0; i < _SearchingTownList.Count; i++)  //이거는 전체 인공지능 대상으로 하므로 현재 타운을 넣어준다.
            {
                if (i==0||( Random.Range(0, 100) < _StatTradeInformationStrength))  //정보력이 닿는 만큼 넣는다. 이거는 다음에 좀 더 괜찮은 로직으로 변경가능할듯. 
                {
                    _tempTown = _SearchingTownList[i];
                    _expectedRevenue = 0;

                    for (int j = 0; j < _currGoodsInCarry.Keys.Count; j++)
                    {
                        _PriceAdjustor = 1;
                        if (_tempTown._TownRegularlyGoods.Contains(_currGoodsInCarry.Keys.ToList()[j]))  //현재 상품을 해당 마을에서 가지고 있으면 반값처리. 
                        {
                            _PriceAdjustor = 0.5f;
                        }

                        //현재 가지고 있는 물품의 가격과 시세및 수량을 계산해서 임시 수익을 계산한다.
                        _expectedRevenue += _currGoodsInCarry.Keys.ToList()[j]._Price * _currGoodsInCarry.Values.ToList()[j] * _tempTown._CurrPriceRateList[_currGoodsInCarry.Keys.ToList()[j]._Group]* _PriceAdjustor;
                    }
                    //_expectedRevenuePerDist = _expectedRevenue / DistBetweenPointR(this.transform.position, _tempTown._currUnitTile._unitTileObject.transform.position);  // 거리에 비례한 수익을 계산해준다.
                    _expectedRevenuePerDist = _expectedRevenue;  //우선 거리 비용은 생략해주자.

                    //Debug.Log(" _expectedRevenue  " + _expectedRevenue);
                    //Debug.Log(" _expectedRevenuePerDist  " + _expectedRevenuePerDist);

                    if (_expectedRevenuePerDist > _MaxRevenuePerDist && Random.Range(0, 100) < _StatTradeJudgement)  //판단을 해서 지금보다 더 좋은 기회면 넣는다.
                    {
                        _MaxRevenuePerDist = _expectedRevenuePerDist;
                        _return = _tempTown;
                    }
                }
            }
        }
        else
        {
            //Debug.Log("들고있는 재고가 없음.");
        }
        return _return;
    }



    //내가 지금 있는 타운을 찾아주는 것
    public void FindMyLocatedTileObject()
    {
        for (int i = 0; i < _WorldManager._CurrTileObjectList.Count; i++)   
        {
            if (DistBetweenPointR(this.transform.position, _WorldManager._CurrTileObjectList[i].transform.position) < 30)
            {
               _CurrOnTheTileObject = _WorldManager._CurrTileObjectList[i];
               

                break;
            }
        }

        if (_CurrOnTheTileObject._CurrUnitTile._currUnitTown != null)  //타일에 마을이 존재한다면 현재 마을로 지정해준다.
        {
            _CurrUnitTown = _CurrOnTheTileObject._CurrUnitTile._currUnitTown;

            if (_IsPlayerChar)  //플레이어 캐릭터일 경우, 현재 도착한 마을을 알고 있는 마을로 넣어준다.
            {
                if (_CurrUnitTown != null)
                {
                    if (!_WorldManager._DiscoveredTownList.Contains(_CurrUnitTown))
                    {
                        _WorldManager._DiscoveredTownList.Add(_CurrUnitTown);
                    }
                }

            }
        }
        else
        {
            _CurrUnitTown = null;
        }
             

    }

    // 특정 타일로 이동하는 기능
    public void SailToTheTile(UnitTile _targetUnitTile)
    {
        StartCoroutine(SailToTheTileIENum(_targetUnitTile));

    }

    IEnumerator SailToTheTileIENum(UnitTile _targetUnitTile)
    {
        if (_IsPlayerChar)  //출발전에 거래창을 닫아준다. 
        {
            _WorldManager._UIMenuBtnController.CloseUITradePurchase();
            _WorldManager._UIMenuBtnController.CloseUIWorldMap();
            _WorldManager.DrawCommandTile(false, false);
        }

        int _wayIndex = 0;
        yield return new WaitForSeconds(0.5f);

        for (; ; )
        {
            if (_wayIndex >= _walkedWayObjectList.Count)
            {
                break;
            }

            if (!_isStunned && !_isHitFlying)  //날라가거나, 스턴 걸렸을 때는 타일로의 이동을 보류한다.
            {

                if (_state == 1)
                {
                    if (_walkedWayObjectList[_wayIndex] != null)  //전에 있던 타일이 아닐 경우. 
                    {
                        _BeforeTileObject = _CurrOnTheTileObject;   //현재 있던 타일이 과거 타일이 되고, 
                        _CurrOnTheTileObject = _walkedWayObjectList[_wayIndex];  //갈 타일을 현재 타일로 저장해준다.
                        if (_walkedWayObjectList[_wayIndex]._CurrUnitTile._currUnitTown != null)  //옮긴 타일에 마을이 존재한다면 현재 마을로 지정해준다.
                        {
                            _CurrUnitTown = _walkedWayObjectList[_wayIndex]._CurrUnitTile._currUnitTown;

                        }
                        else
                        {
                            _CurrUnitTown = null;
                        }

                        yield return StartCoroutine(MoveCharacterToHereIEnum(_walkedWayObjectList[_wayIndex], false));
                        _wayIndex++;


                    }
                }
            }
            else
            {
                yield return new WaitForSeconds(3.03f);

            }
            yield return new WaitForSeconds(0.03f);
        }
    }

    IEnumerator SailToTheTileIENumJustOneBlock(UnitTile _targetUnitTile)
    {
        if (_IsPlayerChar)  //출발전에 거래창을 닫아준다. 
        {
            _WorldManager._UIMenuBtnController.CloseUITradePurchase();
            _WorldManager._UIMenuBtnController.CloseUIWorldMap();
            _WorldManager.DrawCommandTile(false, false);
        }

        int _wayIndex = 0;
        yield return new WaitForSeconds(0.01f);

        for (int i=0;i<1 ;i++ )
        {
            if (_wayIndex >= _walkedWayObjectList.Count)
            {
                break;
            }

            if (_isStunned || _isHitFlying)  //날라가거나, 스턴 걸렸을 때는 타일로의 이동을 취소해준다.
            {
                break;
            }

            if (_state == 1)
            {
                if (_walkedWayObjectList[_wayIndex] != null)  //전에 있던 타일이 아닐 경우. 
                {
                    _BeforeTileObject = _CurrOnTheTileObject;   //현재 있던 타일이 과거 타일이 되고, 
                    _CurrOnTheTileObject = _walkedWayObjectList[_wayIndex];  //갈 타일을 현재 타일로 저장해준다.
                    if (_walkedWayObjectList[_wayIndex]._CurrUnitTile._currUnitTown != null)  //옮긴 타일에 마을이 존재한다면 현재 마을로 지정해준다.
                    {
                        _CurrUnitTown = _walkedWayObjectList[_wayIndex]._CurrUnitTile._currUnitTown;

                    }
                    else
                    {
                        _CurrUnitTown = null;
                    }

                    yield return StartCoroutine(MoveCharacterToHereIEnum(_walkedWayObjectList[_wayIndex], false));
                    _wayIndex++;


                }
            }
            yield return new WaitForSeconds(0.03f);
        }
    }




    //-------------------전투 메소드 관련 영역 ----------------------------------------------



    public void Attack()
    {
        if (_CurrUnitTown == null && !_isStunned && !_isHitFlying )
        {
            if (_currentMP - 1 >= 0)
            {
                _currentMP -= 1;
                if (_WeaponControls != null)
                {
                    if (MoveLeftOrRightRef != null)
                    {
                        StopCoroutine(MoveLeftOrRightRef);
                    }
                    MoveLeftOrRightRef = MoveLeftOrRight();
                    StartCoroutine(MoveLeftOrRightRef);


                    switch (_WeaponControls.Character.WeaponType)
                    {
                        case WeaponType.Melee1H:
                        case WeaponType.Melee2H:
                        case WeaponType.MeleePaired:
                            // _WeaponControls.Character.Animator.SetTrigger(Time.frameCount % 2 == 0 ? "Slash" : "Jab");
                            _WeaponControls.Character.Animator.SetTrigger("Slash");

                            break;

                        case WeaponType.Bow:
                            _WeaponControls.Character.BowShooting.ChargeButtonDown = Input.GetKeyDown(_tabBTN);
                            _WeaponControls.Character.BowShooting.ChargeButtonUp = Input.GetKeyUp(_tabBTN);
                            break;
                    }
                }
            }
        }
    }

    IEnumerator MoveLeftOrRightRef;
    IEnumerator MoveLeftOrRight()
    {
        _UICharInfo.transform.localPosition = new Vector3(0, 131.77f, 0);
        _Character.transform.localPosition = new Vector3(0, -50, 0);


        if (Random.Range(0,100)<50)
        {
            _Character.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);

            _Character.transform.Translate(new Vector3(-4, 0, 0));
            _UICharInfo.transform.Translate(new Vector3(-4, 0, 0));

            yield return new WaitForSeconds(0.45f);
            _Character.transform.Translate(new Vector3(4, 0, 0));
            _UICharInfo.transform.Translate(new Vector3(4, 0, 0));
        }
        else
        {
            _Character.transform.localScale = new Vector3(-0.5f, 0.5f, 0.5f);

            _Character.transform.Translate(new Vector3(4, 0, 0));
            _UICharInfo.transform.Translate(new Vector3(4, 0, 0));

            yield return new WaitForSeconds(0.45f);
            _Character.transform.Translate(new Vector3(-4, 0, 0));
            _UICharInfo.transform.Translate(new Vector3(-4, 0, 0));
        }


    }


    public void GotDMG(int _dmg, UnitStarter _attacker)
    {
        
        float _hitDir = -1;
        if (_attacker._Character.transform.position.x > _Character.transform.position.x)
        {
            _hitDir = -1;
        }
        else
        {
            _hitDir = 1;
        }


        Vector3 _dVel = -(_attacker._Character.transform.position - _Character.transform.position).normalized * Random.Range(0.4f, 0.6f) + new Vector3(0, Random.Range(2.2f, 2.8f), 0);

        bool _isBackStabAttack = false;  //뒤에서 찌르면.. 색은 노란색
        bool _isCritical = false;  // 크리티컬은 글씨 크기를 조정한다.
        bool _isEvaded = false; //회피했음

        //if (Random.Range(0, 100) < _attacker._criticalPoss * 100)  //크리티컬 터짐
        //{
        //    _isCritical = true;
        //    _dmg = _dmg + (int)(_dmg * _attacker._criticalRatio);
        //}
        //if (_dir == _attacker._dir) //백스탭
        //{
        //    _isUltimateAttack = true;
        //    if (_GeneDatas[8]._expressed)
        //    {
        //        _dmg = _dmg * 4;

        //    }
        //    else
        //    {
        //        _dmg = _dmg * 2;

        //    }
        //}
             

        if (_state != 2)
        {
            if (_isEvaded)  //현재는 이베이드가 없으므로 발동은 안될것 같다.
            {                
                if (_UICharInfo != null)
                {
                    _UICharInfo._UITextRoot.ShowMSG("Evaded!", 0.01f, new Color(1, 0.8f, 1, 1), 80, new Vector3(Random.Range(-0.1f, 0.1f), 1.4f, 0), false, 30, new Vector3(0, 0, 0));
                    return;
                }
            }
            if (_UICharInfo._UITextRoot.gameObject.activeSelf) // uitextroot가 활성화 되어 있을 때만
            {
                if (_UICharInfo != null && _dmg > 0)
                {
                    if (true)   //맞은쪽을 넣어주는 분기. 현재는 상관
                    {
                        if (_dmg < _maxHP * 0.1f)
                        {
                            _UICharInfo._UITextRoot.ShowMSG(_dmg + "", 0.01f, new Color(1, 0.1f, 0.1f, 1), 100, new Vector3(Random.Range(0.8f, 1.2f) * _hitDir, Random.Range(1.2f, 1.6f), 0), false, 30, new Vector3(0, -0.1f, 0));

                        }
                        else if (_dmg < _maxHP * 0.3f)
                        {
                            _UICharInfo._UITextRoot.ShowMSG(_dmg + "", 0.01f, new Color(1, 0.1f, 0.1f, 1), 120, new Vector3(Random.Range(0.8f, 1.2f) * _hitDir, Random.Range(1.2f, 1.6f), 0), false, 30, new Vector3(0, -0.1f, 0));

                        }
                        else
                        {
                            _UICharInfo._UITextRoot.ShowMSG(_dmg + "", 0.01f, new Color(1, 0.1f, 0.1f, 1), 150, new Vector3(Random.Range(0.8f, 1.2f) * _hitDir, Random.Range(1.2f, 1.6f), 0), false, 30, new Vector3(0, -0.1f, 0));

                        }
                    }
                    else   //맞은 쪽이 상대방
                    {
                        if (_isBackStabAttack)    //백스탭 공격이다.
                        {
                            if (_isCritical)
                            {
                                _UICharInfo._UITextRoot.ShowMSG(_dmg + "!!", 0.01f, new Color(1f, 0.94f, 0.55f, 1), 150, new Vector3(Random.Range(0.9f, 1.8f) * _hitDir, 3.0f, 0), false, 60, new Vector3(0, -0.1f, 0));

                            }
                            else
                            {
                                _UICharInfo._UITextRoot.ShowMSG(_dmg + "!!", 0.01f, new Color(1f, 0.94f, 0.55f, 1), 100, new Vector3(Random.Range(0.9f, 1.8f) * _hitDir, 3.0f, 0), false, 60, new Vector3(0, -0.1f, 0));

                            }
                        }
                        else  //그냥 일반 공격이다.
                        {
                            if (_isCritical)
                            {
                                _UICharInfo._UITextRoot.ShowMSG(_dmg + "", 0.01f, new Color(1, 1f, 1f, 1), 150, new Vector3(Random.Range(0.8f, 1.8f) * _hitDir, Random.Range(1.5f, 2.3f), 0), false, 60, new Vector3(0, -0.1f, 0));

                            }
                            else
                            {
                                _UICharInfo._UITextRoot.ShowMSG(_dmg + "", 0.01f, new Color(1, 1f, 1f, 1), 100, new Vector3(Random.Range(0.8f, 1.8f) * _hitDir, Random.Range(1.5f, 2.3f), 0), false, 60, new Vector3(0, -0.1f, 0));

                            }
                        }
                    }
                }
            }

            if (_currentHP - _dmg > 0)  //입은 피해가 죽음에 이를 정도는 아닌 경우
            {
                _currentHP -= _dmg;

                HitEffectIENumRef = HitEffectIENum(_attacker);
                StartCoroutine(HitEffectIENumRef);
                
            }
            else  //입은 피해가 죽음에 이를 경우
            {
                _state = 2;
                StopCoroutine(AIBrainRef);
                
                _currentHP = 0;
                HitEffectIENumRef = HitEffectIENum(_attacker);
                StartCoroutine(HitEffectIENumRef);

                //if (!bIsArmy)
                //{
                //    AwakeAndDropItems();
                //}
                            

            }            
        }
    }




    public bool _isStunned;  //땅에 드러 누웠다.
    public bool _isHitFlying;  
    IEnumerator HitEffectIENumRef;
    IEnumerator HitEffectIENum(UnitStarter _attacker)
    {
        bool _WillBeFlying = false; //레벨 차이 혹은 기타 여러 스탯 차이에 의해 날라갈지 여부 판별식

        float _endY = _Character.transform.position.y; //떨어질 때 고정되는 절대적인 위치 값
        float _velocityX = 0;
        float _velocityY = 0;
        float _flyIntensity = 0;

        _AnimationObject.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));
        _Character.transform.localPosition = new Vector3(0, -50, 0);

        _flyIntensity = 0.7f; //이제 얘는 상수로 쓰인다. 죽을 경우에만 찔러주자.
        _velocityX = _flyIntensity * 0.5f * _attacker._dir;
        _velocityY = _flyIntensity * 1.8f;

        if (!_attacker._isHitFlying && !_attacker._isStunned)  //공격자가 정상 상태여야 내가 넘어진다.
        {
            if (_attacker._WarLevel > _WarLevel)
            {
                if (Random.Range(0, 100) < 99)
                {
                    _WillBeFlying = true;

                }
            }else if (_attacker._WarLevel == _WarLevel)
            {
                if (Random.Range(0, 100) < 23)
                {
                    _WillBeFlying = true;

                }
            }
            else
            {
                if (Random.Range(0, 100) < 0)
                {

                    _WillBeFlying = true;

                }
            }
        }
        if (_state != 2)  //안 죽는다.   
        {
            if ( _attacker._StunATKAvailable&& _WillBeFlying && !_isStunned)  //날라가는 상황에 처했다. 때린 애가 아쳐는 아니다.
            {
                _attacker._StunATKAvailable = false;
                _isStunned = true;
                _isHitFlying = true;
                _Character.Animator.SetInteger("Dead", 1);

                _Character.Animator.SetBool("Run", false);
                _Character.Animator.SetBool("Ready", true);
                _Character.Animator.SetBool("Angry", false);

                yield return new WaitForSeconds(0.1f);

                if (_attacker.transform.localPosition.x > this.transform.localPosition.x)
                {
                    _Character.Animator.SetBool("DieFront", true);
                }
                else
                {
                    _Character.Animator.SetBool("DieBack", true);
                }

                for (int i = 0; i < 1000; i++)
                {
                    if (_Character.transform.position.y < _endY)
                    {
                        _Character.transform.position = new Vector3(_Character.transform.position.x, _endY, _Character.transform.position.z);
                        break;
                    }
                    _Character.transform.Translate(new Vector3(_velocityX, _velocityY, 0));
                    _velocityY += constGravity;
                    yield return new WaitForEndOfFrame();
                }

                for (int i = 0; i < 100; i++)
                {
                    if (!_isStunned)
                    {
                        Debug.Log(" stunned breaked");
                        break;
                    }
                    yield return new WaitForSeconds(0.01f);  // 날라간 후에 몇 초간 땅에 눕는다.

                }

                if (_state != 2)
                {
                    _Character.Animator.SetInteger("Dead", 0);
                    _Character.Animator.SetBool("DieBack", false);
                    _Character.Animator.SetBool("DieFront", false);
                    _Character.Animator.SetBool("Run", false);
                    _isHitFlying = false;
                    _isStunned = false;
                    _Character.transform.localPosition = new Vector3(0, -50, 0);
                }
            }
            else if (!_isStunned)  //날라가지는 않는다.
            {
                _Character.transform.Translate(new Vector3(-1f, 0, 0) * _dir);
                _Character.transform.Rotate(new Vector3(0, 0, 3) * _dir);
                yield return new WaitForSeconds(0.05f);

                _Character.transform.localPosition = new Vector3(0, -50, 0);
                _Character.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));

                _Character.Animator.SetInteger("Dead", 1);
                yield return new WaitForSeconds(1);

                _Character.Animator.SetInteger("Dead", 0);

            }
        }
        else  //죽는 상황이다.
        {
            _Character.Animator.SetInteger("Dead", 1);
            _isStunned = true;

            //아래: 죽을 때 다 꺼준다. 
            _UICharInfo._HPBarInfo.gameObject.SetActive(false);
            _UICharInfo._MPBarInfo.gameObject.SetActive(false);
            _UICharInfo._LvInfo.gameObject.SetActive(false);
            _Character.GetComponent<BoxCollider>().enabled = false;

            if (_flyIntensity >= 1 )  //날라가는 상황에 처했다. 때린 애가 아쳐는 아니다.
            {
                _isHitFlying = true;

                _Character.Animator.SetBool("Run", false);
                _Character.Animator.SetBool("Ready", true);
                _Character.Animator.SetBool("Angry", false);

                yield return new WaitForSeconds(0.1f);

                if (_attacker.transform.localPosition.x > this.transform.localPosition.x)
                {
                    _Character.Animator.SetBool("DieFront", true);
                }
                else
                {
                    _Character.Animator.SetBool("DieBack", true);
                }

                for (int i = 0; i < 1000; i++)
                {
                    if (_Character.transform.position.y < _endY)
                    {
                        _Character.transform.position = new Vector3(_Character.transform.position.x, _endY, _Character.transform.position.z);

                        break;
                    }
                    _Character.transform.Translate(new Vector3(_velocityX, _velocityY, 0));
                    _velocityY += constGravity;
                    yield return new WaitForEndOfFrame();

                }

                _isHitFlying = false;
            }
            else  //날라가지 않고 조용히 죽는다.
            {
                _Character.Animator.SetBool("Run", false);
                _Character.Animator.SetBool("Ready", true);
                _Character.Animator.SetBool("Angry", false);

                yield return new WaitForSeconds(0.1f);

                if (_attacker.transform.localPosition.x > this.transform.localPosition.x)
                {
                    _Character.Animator.SetBool("DieFront", true);
                }
                else
                {
                    _Character.Animator.SetBool("DieBack", true);
                }
            }
            

            _attacker._CurrWarExp += (int)(200* (_TradeLevel+_WarLevel));
            _attacker._Gold += _Gold;
            _StarterManager.LevelDeciderForFight(_attacker);                 

            _StarterManager._CurrStartersList.Remove(this);
            _UICharInfo.gameObject.SetActive(false);
            yield return new WaitForSeconds(5);

            if (_IsPlayerChar)
            {

                SceneManager.LoadScene("WorldMap"); //죽으면 일단 씬 재로드 해주자.
                Destroy(this.gameObject);

            }
            else
            {
                Destroy(this.gameObject);

            }

        }
    }





    //public void GotHeal(int _heal, float _delaySec, UnitCharacter _healer, bool _fever)
    //{
    //    if (_state != 2)
    //    {
    //        if (_healer.bIsArmy == this.bIsArmy)
    //        {
    //            if (_fever)
    //            {
    //                _CharInfo._UITextRoot.ShowMSG(_heal + "", _delaySec, new Color(0.1f, 1f, 0.1f, 1), 120, new Vector3(Random.Range(-0.2f, 0.2f), 1.2f, 0), false, 30, new Vector3(0, 0, 0));

    //            }
    //            else
    //            {
    //                _CharInfo._UITextRoot.ShowMSG(_heal + "", _delaySec, new Color(0.1f, 1f, 0.1f, 1), 80, new Vector3(Random.Range(-0.2f, 0.2f), 0.8f, 0), false, 30, new Vector3(0, 0, 0));
    //            }
    //        }

    //        if (_currentHP + _heal < _maxHP)
    //        {
    //            _currentHP += _heal;

    //        }
    //        else
    //        {
    //            _currentHP = _maxHP;

    //        }

    //    }
    //}










    //--------------------탐험 메소드 관련 영역 ----------------------------------------------








    //--------------------길찾기 메소드 관련 영역 -------------------------------------------




    public List<UnitTileObject> GetWayListFromWayManger(UnitTile _startUnitTile, UnitTile _targetUnitTile)   // 웨이매니져에서 이미 정해진 길을 가져온다.
    {
        //Debug.Log("GetWayListFromWayManger has been called: " + _startUnitTile._Num +" to "+ _targetUnitTile._Num);

        List<UnitTileObject> _return = null;

        for(int i=0; i < _WayManager._AllWaysList.Count; i++)
        {
            if(_startUnitTile._unitTileObject == _WayManager._AllWaysList[i][0])
            {
                for (int j = 0; j < _WayManager._AllWaysList.Count; j++)
                {
                    if (_targetUnitTile._unitTileObject == _WayManager._AllWaysList[i][_WayManager._AllWaysList[i].Count - 1])
                    {
                        _return = _WayManager._AllWaysList[i];

                    }
                }
            }
        }
        return _return;
    }

    

    public List<UnitHex> _closedHexList;
    public List<UnitHex> _openedHexList;
    public UnitHex _currHex;   
    public UnitHex _lastHex;   // 도착 헥스 직전의 헥스다.
    public UnitHex _destinationHex;
    public UnitHex _initiationHex;

    bool IsClosedHexListHaveThisTileObject(UnitTileObject _targetTileObject)
    {
        bool _return = false;

        for (int i = 0; i < _closedHexList.Count; i++)
        {
            if (_closedHexList[i]._unitTileObject == _targetTileObject)
            {
                _return = true;
                break;
            }
        }
        return _return;
    }

    bool IsOpendHexListHaveThisTileObject(UnitTileObject _targetTileObject)
    {
        bool _return = false;

        for (int i = 0; i < _openedHexList.Count; i++)
        {
            if (_openedHexList[i]._unitTileObject == _targetTileObject)
            {
                _return = true;
                break;
            }
        }
        return _return;
    }
    
    UnitHex GetHexFromObjectTile(UnitTileObject _targetTileObject)
    {
        UnitHex _return = null;

        for (int i = 0; i < _openedHexList.Count; i++)
        {
            if (_openedHexList[i]._unitTileObject == _targetTileObject)
            {
                _return = _openedHexList[i];
                break;
            }
        }
        if(_return == null)
        {
            for (int i = 0; i < _closedHexList.Count; i++)
            {
                if (_closedHexList[i]._unitTileObject == _targetTileObject)
                {
                    _return = _closedHexList[i];
                    break;
                }
            }
        }

        return _return;
    }
    
    UnitHex GetHexOfCheapestGFromOpenList()
    {
        UnitHex _return = null;
        int _minG = 999999;
        
        for(int i = 0; i < _openedHexList.Count; i++)
        {
            if(_openedHexList[i]._G < _minG)
            {
                _return = _openedHexList[i];
                _minG = _openedHexList[i]._G;
            }
        }
        
        return _return;
    }
    
    public List<UnitTileObject> FindWayByAStar(UnitTile _targetUnitTile)
    {
        List<UnitTileObject> _returnWayList = new List<UnitTileObject>(); ;  //이게 결국 최종 길이지 뭐.
        List<UnitHex> _returnHexList = new List<UnitHex>();  //이건 헥스 기준.. 이걸로 받은 후, 웨이 리스트로 다시 뿌려줄까?
        UnitHex _SearchingHex;

        _closedHexList = new List<UnitHex>();
        _openedHexList = new List<UnitHex>();
        _currHex = new UnitHex();
        _lastHex = new UnitHex();
        _initiationHex = new UnitHex();
        _destinationHex = new UnitHex();

        

        //도착 타일 지정
        _destinationHex._unitTileObject = _targetUnitTile._unitTileObject;
        _destinationHex._H = 0;
        _destinationHex._G = 0;
        _destinationHex._F = 0;



        for (int i = 0; i < _WorldManager._CurrTileObjectList.Count; i++)   //시작 타일 지정
        {
            if (DistBetweenPointR(this.transform.position, _WorldManager._CurrTileObjectList[i].transform.position) < 30)
            {
                _currHex._unitTileObject = _WorldManager._CurrTileObjectList[i];
                _currHex._G = 0;
                _currHex._H = DistBetweenPointHEX(_currHex._unitTileObject, _destinationHex._unitTileObject);
                _closedHexList.Add(_currHex);
                _initiationHex = _currHex;
                break;
            }
        }


        //맨 처음 한번 열어주기.   

        for (int i = 0; i < _WorldManager._CurrTileObjectList.Count; i++)
        {
            if (DistBetweenPointHEX(_WorldManager._CurrTileObjectList[i], _initiationHex._unitTileObject) == 1 && !_WorldManager._CurrTileObjectList[i]._CurrUnitTile._IsMoveUnAvailable)
            {
                UnitHex _tempHex = new UnitHex();
                _tempHex._unitTileObject = _WorldManager._CurrTileObjectList[i];
                _tempHex._G = 1;
                _tempHex._H = DistBetweenPointHEX(_WorldManager._CurrTileObjectList[i], _destinationHex._unitTileObject);
                _tempHex._F = _tempHex._G + _tempHex._H;
                _tempHex._Parent = _initiationHex;

                _openedHexList.Add(_tempHex);

            }
        }
        
        //무한 반복 시작
        bool _arrived = false;
        for (int h = 0; h < 99999; h++)
        {
            if (_arrived)  //길 찾음
            {
                //Debug.Log("Path Finded...at " + h + " I am" + this.name);

                _returnHexList.Add(_destinationHex);
                _returnHexList.Add(_lastHex);
                _currHex = _lastHex;

                for (; ; )
                {
                    if (_currHex._Parent == _initiationHex )
                    {
                        break;
                    }
                    _currHex = _currHex._Parent;
                    _returnHexList.Add(_currHex);
                    
                }

                for(int l = _returnHexList.Count; l >0   ; l--)  //거꾸로 길을 넣어주는 것.
                {
                    _returnWayList.Add(_returnHexList[l-1]._unitTileObject);                    
                }


                break;
            }                             
            _SearchingHex = GetHexOfCheapestGFromOpenList();
            //한 개의 제일 싼 오픈 헥스를 가져와서 주변부를 탐색해준다.
            if (_SearchingHex == null)
            {
                Debug.Log("no way until "+ h + " I am" + this.name);
                SceneManager.LoadScene("WorldMap");
               //_WorldManager._CurrTownList.Remove(_targetUnitTile._currUnitTown);  //길이 없는 마을은 일단 리스트에서 제거해준다. 나중에는 드로우 시에 처리해줘야할듯
                break;
            }
            for (int j = 0; j < _WorldManager._CurrTileObjectList.Count; j++)
            {
                if (DistBetweenPointHEX(_WorldManager._CurrTileObjectList[j], _SearchingHex._unitTileObject) == 1 && !_WorldManager._CurrTileObjectList[j]._CurrUnitTile._IsMoveUnAvailable && !IsClosedHexListHaveThisTileObject(_WorldManager._CurrTileObjectList[j]))
                {

                    if (_destinationHex._unitTileObject == _WorldManager._CurrTileObjectList[j]) //도착함. 
                    {
                        _arrived = true;
                        _lastHex = _SearchingHex;
                        break;
                    }
                    else if (!IsOpendHexListHaveThisTileObject(_WorldManager._CurrTileObjectList[j]))  //해당 타일이 오픈에 있지 않은 새로 조사하는 타일이다.
                    {
                        UnitHex _tempHex = new UnitHex();
                        _tempHex._unitTileObject = _WorldManager._CurrTileObjectList[j];
                        _tempHex._Parent = _SearchingHex;
                        _tempHex._G = _SearchingHex._G + 1;
                        _tempHex._H = DistBetweenPointHEX(_WorldManager._CurrTileObjectList[j], _destinationHex._unitTileObject);
                        _tempHex._F = _tempHex._G + _tempHex._H;
                        _openedHexList.Add(_tempHex);
                    }
                    else  //이미한번 조사했던 타일이다. 여기서는 판정 해서 퍼렌트 바꿔줘야 함.
                    {
                        UnitHex _tempHex = GetHexFromObjectTile(_WorldManager._CurrTileObjectList[j]);

                        if (_SearchingHex._G + 1 < _tempHex._G)
                        {
                            _tempHex._Parent = _SearchingHex;
                            _tempHex._G = _SearchingHex._G + 1;
                            _tempHex._H = DistBetweenPointHEX(_WorldManager._CurrTileObjectList[j], _destinationHex._unitTileObject);
                            _tempHex._F = _tempHex._G + _tempHex._H;

                        }
                    }
                }
            }
            _openedHexList.Remove(_SearchingHex);
            _closedHexList.Add(_SearchingHex);

        }
        return _returnWayList;

    }
    
    int CountParentG(UnitHex _targetHex)
    {        
        int _return=1;
        UnitHex _currHex = _targetHex;

        for (; ; )
        {
            if (_targetHex._Parent != null && _targetHex._Parent != _initiationHex)
            {
                _currHex = _currHex._Parent;
                _return++;
            }
            else
            {
                break;
            }
        }
        
        return _return;
    }

    public float DistBetweenPointR(Vector3 _target, Vector3 _refer)
    {
        return (_target.x - _refer.x) * (_target.x - _refer.x) + (_target.y - _refer.y) * (_target.y - _refer.y);

    }

    public int DistBetweenPointHEX(UnitTileObject _targetTileObject, UnitTileObject _referTileObject)
    {
        int _return = 0;

        _return = Mathf.Abs( (int)((_targetTileObject.transform.localPosition.x - _referTileObject.transform.localPosition.x) *0.4f)) + Mathf.Abs((int)((_targetTileObject.transform.localPosition.y - _referTileObject.transform.localPosition.y) *0.5f));

        return _return;

    }



    //---------------------------------캐릭터 이동 및 공격 로밍등 관련 메소드 -----------------------------
   
        
        
        //이동 명령어
    public void MoveCharacterToHere(UnitTileObject _targetTileObject, bool IsMovingPlayer)
    {
        if (IsMovingPlayer)
        _WorldManager.DrawCommandTile(false, false);  //움직일 때는 이동 타일을 꺼준다.
        StartCoroutine(MoveCharacterToHereIEnum(_targetTileObject, IsMovingPlayer));
    }

    float _consumedMP;
    IEnumerator MoveCharacterToHereIEnum(UnitTileObject _targetTileObject, bool IsMovingPlayer)
    {
        if (!_isStunned && !_isHitFlying)
        {
            if (_IsPlayerChar)
            {
                _WorldManager._WorldCharacterController._IsMoving = true;
            }
            else
            {
                _Character.transform.localPosition = new Vector3(0, -50, 0);

            }
            _consumedMP = _CurrCarryWeight * 0.005f + 1f;
            if (_currentMP - _consumedMP >= 0)
            {
                _currentMP -= _consumedMP;
                _AudjustedMoveSPD = _MoveSPD;
            }
            else
            {
                _currentMP = 0;
                _AudjustedMoveSPD = _MoveSPD * 0.3f;

                if (_currentHP - (int)(_maxHP*0.05f) >= 0)
                {
                    _currentHP -= (int)(_maxHP * 0.05f);
                }
                else
                {
                    _currentHP = 0;

                    //쓰러져서 죽는 것을 형상화
                    _Character.Animator.SetBool("Run", false);
                    _Character.Animator.SetBool("Ready", true);
                    _Character.Animator.SetBool("Angry", false);

                    yield return new WaitForSeconds(0.1f);

                    _Character.Animator.SetBool("DieFront", true);



                }
            }


            _Character.Animator.SetBool("Run", true);
            _state = 3;

            Vector3 _targetAdjustedVector = new Vector3(_targetTileObject.transform.position.x, _targetTileObject.transform.position.y, this.transform.position.z);

            if (_targetTileObject.transform.position.x >= this.transform.position.x)
            {
                _Character.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            }
            else
            {
                _Character.transform.localScale = new Vector3(-0.5f, 0.5f, 0.5f);
            }

            for (int i = 0; i < 10000; i++)
            {
                yield return new WaitForEndOfFrame();

                this.transform.position += (_targetAdjustedVector - this.transform.position).normalized * _AudjustedMoveSPD;
                if (DistBetweenPointR(_targetAdjustedVector, this.transform.position) < 0.50f)
                {
                    //Debug.Log("breaked");
                    break;
                }
            }

            _CurrOnTheTileObject = _targetTileObject;  //현재 타일 위치 인식해준다.
            if (Random.Range(0, 100) < 23 && _currentMP>0) 
            {
                _StunATKAvailable = true;
                _CurrHitAvailableCount = _StatHitAvailableCount;
             
            }
            if (_currentMP == 0)
            {
                _StunATKAvailable = false;
                _CurrHitAvailableCount = 0;

            }
            if (!_StunATKAvailable)  //스턴 어택 충전상태가 아닐 경우.
            {
                _CurrHitAvailableCount = 0; //이동 시에는 히트 카운트 0으로 초기화해준다.단,,,,,
            }
            else
            {
                _CurrHitAvailableCount = _StatHitAvailableCount; 
            }

            if (_targetTileObject._CurrUnitTile._currUnitTown != null)  //이동하려는 장소가 타운일 경우, 무기를 집어넣어라.
            {
                _CurrHitAvailableCount = 0;
            }

            if (_IsPlayerChar)
            {
                _WorldManager._PlayerUnitStarter.FindMyLocatedTileObject();
                _WorldManager._WorldCharacterController._IsMoving = false;

                //_WorldManager.DiscoveringTilesForUnFog(_targetTileObject._CurrUnitTile);
                if (IsMovingPlayer)
                {
                    _WorldManager._WorldCharacterController.ToggleMoveCommandTiles();
                }
            }

            _Character.Animator.SetBool("Run", false);
            _Character.Animator.SetBool("Ready", true);
            _Character.Animator.SetBool("Angry", false);

            _state = 1;

        }
    }




    //로밍
    UnitTile _roamingTargetTile;
    public List<UnitStarter> _recognizedEnemyList; //인식한 적 리스트들
    public UnitStarter _currEnemy;  //현재 타겟한 적

    IEnumerator RoamingAroundAndPlunderingIENum()
    {
        _roamingTargetTile = new UnitTile() ;
        _recognizedEnemyList = new List<UnitStarter>();
        _currEnemy = new UnitStarter();

        for (; ; )
        {
            if (_state == 2)
            {
                break;
            }
            yield return new WaitForSeconds(0.15f);

            if (_currEnemy == null)  //현재 적이 없을 경우,
            {
                if(_currentHP+_currentHP*0.03f <= _maxHP)
                {
                    _currentHP += _currentHP * 0.03f;
                }
                else
                {
                    _currentHP = _maxHP;
                }

                _recognizedEnemyList.Clear();
                _recognizedEnemyList = _StarterManager.FindNearStarters(this, 500, true);
               // Debug.Log(_Name + " 은 적을 찾는 중...");

                if (_recognizedEnemyList != null && _recognizedEnemyList.Count>0) //주변에서 발견된 적이 있을 경우
                {
                    //Debug.Log(_Name + " 은 적을 찾아서 하나 랜덤으로 입력해줄 예정. 총 개수는 " + _recognizedEnemyList.Count);

                    _currEnemy = _recognizedEnemyList[(int)Random.Range(0, _recognizedEnemyList.Count)]; //적을 인식해준다.


                }
                else  //주변에 적이 눈싯고 찾아봐도 없다.
                {
                    //Debug.Log(_Name + " 은 아무리 찾아도 적이 없음...");

                    for (int i = 0; i < 3; i++)  //로밍 시작
                    {

                        _roamingTargetTile = _WorldManager.GetNearTiles(_CurrOnTheTileObject._CurrUnitTile, this)[(int)Random.Range(0, 6)];

                        if (_roamingTargetTile != null)
                        {
                            // Debug.Log(_Name + " 은 타일 한개 이동 시작함...");
                            if (_roamingTargetTile._currUnitTown == null)
                            {
                                yield return MoveCharacterToHereIEnum(_roamingTargetTile._unitTileObject, false);
                            }
                            yield return new WaitForSeconds(Random.Range(0.2f, 2f));

                        }
                    }
                }
            }
            else  //현재 타겟해 놓은 적이 있을 경우
            {
                if (DistBetweenPointR(this.transform.position, _currEnemy.transform.position) < 60)
                {
                    //Debug.Log(_Name + " 은 공격을 시작한다...");

                    Attack();
                    yield return new WaitForSeconds(_ATKCoolTime*0.5f+0.1f);
                }
                else if (DistBetweenPointR(this.transform.position, _currEnemy.transform.position) > 2000) //거리가 너무 먼 경우 대상을 놓치게 해주자.
                {
                    //Debug.Log(_Name + " 은 대상을 놓쳤습니다..");

                    _currEnemy = null;

                }
                else if (_currEnemy._CurrUnitTown!=null)
                {
                    //Debug.Log(_Name + " 은 대상이 안전지대에 있어서 놓쳤습니다..");

                    _currEnemy = null;
                }
                else
                {
                   // Debug.Log(_Name + " 은 적에게로 다가간다...");
                           
                    if (!_isHitFlying && !_isStunned)
                    {
                        UnitTile _tempTile = _WorldManager.GetNearTileToApproachTile(_currEnemy._CurrOnTheTileObject._CurrUnitTile, this);
                        if (_tempTile._unitTileObject != null)
                        {

                            if (_tempTile._currUnitTown == null)
                            {

                                yield return MoveCharacterToHereIEnum(_tempTile._unitTileObject, false);
                            }
                        }
                        else
                        {
                           // Debug.Log(_Name + " 은 대상을 놓쳤습니다..");

                            _currEnemy = null;
                        }
                    }
                }
            }
        }

    }

    //---------------------------아이템 장착 메소드------------------------------------------

    public void EquipItem(UnitGoods _UnitGoods)  //장비를 장착해주는 메소드. 
    {
        SellGoodsFromCarryStock(_UnitGoods, 1);

        // 0은 주무기   
        //1은 쉴드 혹은 보조 무기 (양손일 경우, 0 과 1에 다가지고 있음),  
        //2는 아머,   
        //3은 헬멧 
        //4는 마스크 
        //5는 글래스
        //6은 케이프
        //7은 등



        if (_UnitGoods._EquipmentType == UnitGoods.EquipmentType.MeleeWeapon1H)  //트레일 가져와야 한다.
        {
            _EquippedItems[0] = _UnitGoods;
            _Character.WeaponType = WeaponType.Melee1H;
            _Character.PrimaryMeleeWeapon = _UnitGoods._GoodsSpriteGroupEntry.Sprite;
            _Character.PrimaryMeleeWeaponRenderer.sprite = _Character.PrimaryMeleeWeapon;

            _Character.PrimaryMeleeWeaponTrailRenderer.sprite = _UnitGoods._SubGoodsSpriteGroupEntry.Sprite;
            _EquippmentATK = _UnitGoods._EquipmentStat;

        }
        else if (_UnitGoods._EquipmentType == UnitGoods.EquipmentType.MeleeWeapon2H)
        {
            _EquippedItems[0] = _UnitGoods;
            _Character.WeaponType = WeaponType.Melee2H;
            _Character.PrimaryMeleeWeapon = _UnitGoods._GoodsSpriteGroupEntry.Sprite;
            _Character.PrimaryMeleeWeaponRenderer.sprite = _Character.PrimaryMeleeWeapon;
            _Character.PrimaryMeleeWeaponTrailRenderer.sprite = _UnitGoods._SubGoodsSpriteGroupEntry.Sprite;
            _EquippmentATK = _UnitGoods._EquipmentStat;


        }
        else if (_UnitGoods._EquipmentType == UnitGoods.EquipmentType.Bow)
        {
            _EquippedItems[0] = _UnitGoods;
            _Character.WeaponType = WeaponType.Bow;

            _Character.Bow = _UnitGoods._GoodsSpriteGroupEntry.Sprites;
            _Character.BowRenderers[0].sprite = _Character.Bow[3];
            _Character.BowRenderers[1].sprite = _Character.Bow[2];
            _Character.BowRenderers[2].sprite = _Character.Bow[2];

            _Character.BowRenderers[3].sprite = _Character.Bow[3];
            _Character.BowRenderers[4].sprite = _Character.Bow[2];
            _Character.BowRenderers[5].sprite = _Character.Bow[2];
            _EquippmentATK = _UnitGoods._EquipmentStat;


        }
        else if (_UnitGoods._EquipmentType == UnitGoods.EquipmentType.Shield)
        {
            _EquippedItems[1] = _UnitGoods;
            _Character.WeaponType = WeaponType.Melee1H;

            _Character.Shield = _UnitGoods._GoodsSpriteGroupEntry.Sprite;
            _Character.ShieldRenderer.sprite = _Character.Shield;

        }
        else if (_UnitGoods._EquipmentType == UnitGoods.EquipmentType.Armor)
        {
            _EquippedItems[2] = _UnitGoods;
            _Character.Armor = _UnitGoods._GoodsSpriteGroupEntry.Sprites;

            _Character.ArmorRenderers[0].sprite = _Character.Armor[0];
            _Character.ArmorRenderers[1].sprite = _Character.Armor[0];
            _Character.ArmorRenderers[2].sprite = _Character.Armor[1];
            _Character.ArmorRenderers[3].sprite = _Character.Armor[1];
            _Character.ArmorRenderers[4].sprite = _Character.Armor[2];
            _Character.ArmorRenderers[5].sprite = _Character.Armor[2];
            _Character.ArmorRenderers[6].sprite = _Character.Armor[2];
            _Character.ArmorRenderers[7].sprite = _Character.Armor[2];
            _Character.ArmorRenderers[8].sprite = _Character.Armor[3];
            _Character.ArmorRenderers[9].sprite = _Character.Armor[3];

            _Character.ArmorRenderers[10].sprite = _Character.Armor[3];
            _Character.ArmorRenderers[11].sprite = _Character.Armor[4];
            _Character.ArmorRenderers[12].sprite = _Character.Armor[4];
            _Character.ArmorRenderers[13].sprite = _Character.Armor[5];
            _Character.ArmorRenderers[14].sprite = _Character.Armor[5];
            _Character.ArmorRenderers[15].sprite = _Character.Armor[5];
            _Character.ArmorRenderers[16].sprite = _Character.Armor[6];
            _Character.ArmorRenderers[17].sprite = _Character.Armor[6];
            _Character.ArmorRenderers[18].sprite = _Character.Armor[7];
            _Character.ArmorRenderers[19].sprite = _Character.Armor[7];

            _Character.ArmorRenderers[20].sprite = _Character.Armor[8];
            _Character.ArmorRenderers[21].sprite = _Character.Armor[9];
            _Character.ArmorRenderers[22].sprite = _Character.Armor[9];
            _Character.ArmorRenderers[23].sprite = _Character.Armor[10];
            _Character.ArmorRenderers[24].sprite = _Character.Armor[11];

        }
        else if (_UnitGoods._EquipmentType == UnitGoods.EquipmentType.Helmet)
        {
            _EquippedItems[3] = _UnitGoods;
            _Character.Helmet = _UnitGoods._GoodsSpriteGroupEntry.Sprite;
            _Character.HelmetRenderer.sprite = _Character.Helmet;

            if (_UnitGoods._SubGoodsSpriteGroupEntry != null)
            {
                _Character.HairMask.sprite = _UnitGoods._SubGoodsSpriteGroupEntry.Sprite;
            }
        }
        else if (_UnitGoods._EquipmentType == UnitGoods.EquipmentType.Mask)
        {
            _EquippedItems[4] = _UnitGoods;
            _Character.Mask = _UnitGoods._GoodsSpriteGroupEntry.Sprite;
            _Character.MaskRenderer.sprite = _Character.Mask;

        }
        else if (_UnitGoods._EquipmentType == UnitGoods.EquipmentType.Glasses)
        {
            _EquippedItems[5] = _UnitGoods;
            _Character.Glasses = _UnitGoods._GoodsSpriteGroupEntry.Sprite;
            _Character.GlassesRenderer.sprite = _Character.Glasses;

        }
        else if (_UnitGoods._EquipmentType == UnitGoods.EquipmentType.Cape)
        {
            _EquippedItems[6] = _UnitGoods;
            _Character.Cape = _UnitGoods._GoodsSpriteGroupEntry.Sprite;
            _Character.CapeRenderer.sprite = _Character.Cape;

        }
        else if (_UnitGoods._EquipmentType == UnitGoods.EquipmentType.Back)
        {
            _EquippedItems[7] = _UnitGoods;
            _Character.Back = _UnitGoods._GoodsSpriteGroupEntry.Sprite;
            _Character.BackRenderer.sprite = _Character.Back;

        }


        //능력치 정산
        if (_EquippedItems[0] != null)
        {
            _EquippmentATK = _EquippedItems[0]._EquipmentStat;
        }
        _EquippmentDEF = 0;

        for (int i = 1; i < 8; i++)
        {
            if (_EquippedItems[i] != null)
            {
                _EquippmentDEF += _EquippedItems[i]._EquipmentStat;
            }
        }
        DecideBySTAT();  //능력치 계산해서 제공



    }
}
