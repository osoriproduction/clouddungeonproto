﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitGoodsObject : MonoBehaviour {

    //외부 참조
    WorldManager _WorldManager;

    public UnitGoods _UnitGoods;
    public Text _TextCount; //수량
    public Image _GoodsImage;  //물건 이미지

    public GameObject _Checked;

    //시스템
    public bool _IsChecked;
    public bool _IsBelongInventory;  //이게 참이면, 인벤토리 굿즈이다.

	void Start () {
        _WorldManager = GetComponentInParent<WorldManager>();
        _Checked.SetActive(false);
    }
     

    private void Update()
    {
        if (_WorldManager._UITradeWindow.gameObject.activeSelf)
        {
            if (_WorldManager._UITradeWindow._CurrCheckedGoods != this)
            {
                _IsChecked = false;
                _Checked.SetActive(false);
            }
        }
        else if (_WorldManager._UIPlayerInfoWindow.gameObject.activeSelf)
        {
            if (_WorldManager._UIPlayerInfoWindow._CurrCheckedGoods != this)
            {
                _IsChecked = false;
                _Checked.SetActive(false);
            }
        }
    }

    public IEnumerator DelayedTouch()
    {
        yield return new WaitForSeconds(0.1f);
        Touch();
    }

    public void Touch()
    {
        if (_WorldManager._UITradeWindow.gameObject.activeSelf)
        {
            if (_IsBelongInventory)  //이게 무조건 먼저 나와야 한다.
            {
                _WorldManager._UITradeWindow._IsBuyingMode = false;
            }
            else
            {
                _WorldManager._UITradeWindow._IsBuyingMode = true;

            }
            CheckAction();
        }
        else if(_WorldManager._UIPlayerInfoWindow.gameObject.activeSelf)
        {
            if (_IsBelongInventory)  //이게 무조건 먼저 나와야 한다.
            {
                _WorldManager._UIPlayerInfoWindow._IsBuyingMode = false;
            }
            else
            {
                _WorldManager._UIPlayerInfoWindow._IsBuyingMode = true;

            }
            CheckActionForPlayerInfo();
        }
    }


    public void CheckAction()
    {
        if (!_IsChecked)
        {
            _WorldManager._UITradeWindow._CurrCheckedGoods = this;
            _WorldManager._UITradeWindow._RememberedGoods = _UnitGoods;
            _IsChecked = true;
            _Checked.SetActive(true);
            _WorldManager._UITradeWindow.DrawCheckedGoodsInfo();
        }
        else
        {
            _WorldManager._UITradeWindow._CurrCheckedGoods = null;
            _IsChecked = false;
            _Checked.SetActive(false);
            _WorldManager._UITradeWindow.DrawCheckedGoodsInfo();

        }
    }

    public void CheckActionForPlayerInfo()
    {
        if (!_IsChecked)
        {
            Debug.Log("checked");
            _WorldManager._UIPlayerInfoWindow._CurrCheckedGoods = this;
            _WorldManager._UIPlayerInfoWindow._RememberedGoods = _UnitGoods;
            _IsChecked = true;
            _Checked.SetActive(true);
            _WorldManager._UIPlayerInfoWindow.DrawCheckedGoodsInfo();
        }
        else
        {
            _WorldManager._UIPlayerInfoWindow._CurrCheckedGoods = null;
            _IsChecked = false;
            _Checked.SetActive(false);
            _WorldManager._UIPlayerInfoWindow.DrawCheckedGoodsInfo();

        }
    }
}
