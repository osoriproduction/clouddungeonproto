﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitCommandTile : MonoBehaviour {

    public UnitTile _CurrUnitTile;  //현재 품고 있는 원형 타일 정보

    public GameObject _MoveSelectBtnObject;
    public GameObject _CanNotMoveBtnObject;
    public GameObject _MainTouchBTNObject;

    WorldManager _WorldManager;

    public bool _IsSelected; //일단 현재 선택된 타일들

    
    
    // Use this for initialization
    void Start () {
        _WorldManager = GameObject.Find("WorldManager").GetComponent<WorldManager>();

	}
	


    public void MainTouchBTN()
    {
        //Debug.Log("this btn was touched :" + _CurrUnitTile._Num);
        //Debug.Log("_SelectedTileList.Count :" + _WorldManager._SelectedTileList.Count);

        if (_WorldManager._SelectedTileList.Count<3 && _CurrUnitTile._IsBoughtLand && _CurrUnitTile._IsDiscovered)
        {
            if (_WorldManager._SelectedTileList.Count == 0 )
            {
                _IsSelected = true;
                _WorldManager._SelectedTileList.Add(_CurrUnitTile);
                SetColorByIsSelected();
            }
            else
            {
                if (_CurrUnitTile._TileType == _WorldManager._SelectedTileList[0]._TileType
                    && _CurrUnitTile._TileGrade == _WorldManager._SelectedTileList[0]._TileGrade
                    && !_WorldManager._SelectedTileList.Contains(_CurrUnitTile)                   
                &&DistBetweenPointR( _CurrUnitTile._unitTileObject.transform.position, _WorldManager._SelectedTileList[_WorldManager._SelectedTileList.Count-1]._unitTileObject.transform.position)<500
                )
                {
                    _IsSelected = true;
                    _WorldManager._SelectedTileList.Add(_CurrUnitTile);
                    SetColorByIsSelected();

                }
            }
        }
      
    }

    public void SetColorByIsSelected()
    {
        if (_IsSelected)
        {
            _MoveSelectBtnObject.SetActive(true);
        }
        else
        {
            _MoveSelectBtnObject.SetActive(false);

        }
    }


    //기능
    public float DistBetweenPointR(Vector3 _target, Vector3 _refer)
    {
        return (_target.x - _refer.x) * (_target.x - _refer.x) + (_target.y - _refer.y) * (_target.y - _refer.y);

    }

}
