﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitTileObject : MonoBehaviour {

    public UnitTile _CurrUnitTile;

    public Text _TileGradeText;
    public Text _MonoPolyCompanyText;

    
    public void DisplayTileGradeText()  //타일 그레이드 보여주는 명령어
    {
        if (_CurrUnitTile._TileGrade >= 1)
        {
            _TileGradeText.gameObject.SetActive(true);

            if (_CurrUnitTile._TileType == 1100)
            {
                _TileGradeText.color = Color.white;
            }
            else if(_CurrUnitTile._TileType == 2100) 
            {
                _TileGradeText.color = Color.blue;

            }
            else if (_CurrUnitTile._TileType == 3100)
            {
                _TileGradeText.color = Color.cyan;

            }
            else if (_CurrUnitTile._TileType == 4000)
            {
                _TileGradeText.color = Color.grey;

            }
            _TileGradeText.text = "+"+_CurrUnitTile._TileGrade + "";
        }
        else
        {

            _TileGradeText.gameObject.SetActive(false);


        }
    }

}
