﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.HeroEditor.Common.CharacterScripts;
using HeroEditor.Common.Enums;



public class TouchInputManager : MonoBehaviour
{

    public WorldManager _WorldManager;

    Vector3 startPos, deltaPos, nowPos;
    const float dragAccuracy = 5;
    
    float iDragSPD;

    public bool _IsOnTileTouch;  //지금 타일 터치를 하고 있는 상태이다. 마우스로는 클릭한 상태.

    public Camera _MainCamera;
    RaycastHit hit;
    Ray ray;
    int pointerID;       

   

    void Awake()
    {

        iDragSPD = -0.35f;


        _WorldManager = GetComponent<WorldManager>();

#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBPLAYER
        pointerID = -1;
#elif UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_IPHONE
		pointerID = 0;
#endif

    }

    Touch tempTouchs;
    int _holdCount;
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();

        }
                
        
        //if (pointerID != -1)  //Android  화면 드래그 로직
        //{

        //    tempTouchs = Input.GetTouch(0);


        //    if (Input.touchCount == 1)
        //    {
        //        if (true)
        //        {                    
                   
        //            if (tempTouchs.phase == TouchPhase.Began)
        //            {
        //                _holdCount = 0;
        //                startPos = tempTouchs.position;
        //                nowPos = startPos;
        //                deltaPos = nowPos - startPos;

        //            }
        //            else if (tempTouchs.phase == TouchPhase.Moved || tempTouchs.phase == TouchPhase.Stationary)
        //            {
        //                _holdCount++;

        //                if (_holdCount > 7)
        //                {
        //                    nowPos = tempTouchs.position;
        //                    deltaPos = nowPos - startPos;

        //                    if (deltaPos.sqrMagnitude > dragAccuracy)
        //                    {
        //                        _MainCamera.transform.Translate(new Vector3(-0.6f * deltaPos.x, 0, 0));
        //                        startPos = nowPos;
        //                    }
        //                }
        //            }
        //            if (tempTouchs.phase == TouchPhase.Ended)
        //            {
        //                _holdCount = 0;
        //            }
        //        }
        //    }
        //    else if (Input.touchCount == 2)
        //    {
            
        //    }
        //}

    }
    

    
    public float DistBetweenR(Vector3 target, Vector3 refer)
    {

        float tempReturnDist = (target.x - refer.x) * (target.x - refer.x)
            + (target.y - refer.y) * (target.y - refer.y)
            + (target.z - refer.z) * (target.z - refer.z);


        return tempReturnDist;


    }





}