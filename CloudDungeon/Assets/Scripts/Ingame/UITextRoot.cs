﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITextRoot : MonoBehaviour {


    WorldManager _WorldManager;
    UnitStarter _UnitStarter;

    public Text _TextPrefab;

    public List<Text> TextBoxes;


    void Start () {

        //_GameManager = GameObject.Find("GameManager").GetComponent<GameManager>();        
        //_UnitCharacter = GetComponentInParent<UnitCharacter>();

        _WorldManager = GetComponentInParent<WorldManager>();
        _UnitStarter = GetComponentInParent<UnitStarter>();

        StartCoroutine(Init());
    }

    IEnumerator Init()
    {
        yield return new WaitForSeconds(0.02f);

        for (int i = 0; i < 100; i++)
        {
            Text temptext = Instantiate(_TextPrefab, this.transform);
            temptext.gameObject.SetActive(false);
            TextBoxes.Add(temptext);
        }


    }
    public void SleepAllTextBox()
    {
        for (int i = 0; i < TextBoxes.Count; i++)
        {
            if (TextBoxes[i] != null)
            {
                TextBoxes[i].gameObject.SetActive(false);

            }
        }
    }


    public void ShowMSG(string _msg, float _delaySec, Color _color, int _size,Vector3 _force, bool _dontdown, int _playFrame, Vector3 _downForce)
    {
        
        Text _currText;
        for(int i=0; i < TextBoxes.Count; i++)
        {
            if (TextBoxes[i] != null)
            {
                if (!TextBoxes[i].gameObject.activeSelf)
                {

                    _currText = TextBoxes[i];
                    _currText.text = _msg;
                    _currText.color = _color;
                    _currText.fontSize = _size;
                    if (this.gameObject.activeSelf)  //아마도 코루틴은 오브젝트가 켜져야지 작동하는듯? 다른 함수는 그냥 작동하나보다. 
                    {
                        StartCoroutine(ShowMSGIEnumWithForce(_currText, _delaySec, _force, _dontdown, _playFrame, _downForce));
                    }
                    break;

                }
            }
        }
    }
        
    IEnumerator ShowMSGIEnumWithForce(Text _text, float _delaySec,Vector3 _velInit, bool _dontdown, int _playFrame, Vector3 _downForce)
    {
        _text.gameObject.SetActive(true);
        _text.color = new Color(_text.color.r, _text.color.g, _text.color.b, 0);

        yield return new WaitForSeconds(_delaySec);
        _text.color = new Color(_text.color.r, _text.color.g, _text.color.b, 1);

        _text.gameObject.transform.localPosition = new Vector3(0, 0, 0);

        Vector3 _velocity = _velInit;

        for (int i = 0; i < _playFrame; i++)
        {
            if (i < 0)
            {
                _text.gameObject.transform.Translate(_velocity);
            }
            else
            {
                _text.gameObject.transform.Translate(_velocity);
                if (_dontdown)
                {
                    _velocity = new Vector3(_velocity.x, _velocity.y, _velocity.z);

                }
                else
                {
                    _velocity = new Vector3(_velocity.x, _velocity.y, _velocity.z) + _downForce;
                }
            }
            yield return new WaitForEndOfFrame();
        }

        _text.gameObject.SetActive(false);
    }



}
