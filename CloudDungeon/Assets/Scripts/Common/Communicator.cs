﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Communicator : MonoBehaviour {
    
    private static Communicator instance = null; //정적 변수
    public static Communicator Instance //인스턴스 접근 프로퍼티
    {  
        get
        {
            return instance;
        }
    }

    public int _currentScene; // 현재 유저가 머물고 있는 씬 이름.  0 월드맵. 타일 넘버와 같은 값을 가진다.
                                       // 타일 넘버  +100 00250: 100번 라인의 x: 2.5     -100 00250: 100번 라인의 x:-2.5







    void Start () {

        if (instance)  //싱글톤 시동
        {
            DestroyImmediate(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);

    }
	



}
