// Copyright (C) 2015-2017 ricimi - All rights reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement.
// A Copy of the Asset Store EULA is available at http://unity3d.com/company/legal/as_terms.

using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

// This class is based on the official source code for Unity's UI Button (which can
// be found here: https://bitbucket.org/Unity-Technologies/ui), but adds a delay before
// calling the button's on-clicked event. The reason for doing this lies in the fact
// that the demo buttons are mostly used to open popups o trigger transitions to new scenes,
// and it gives a nicer visual feeling to wait for the button animation to be played
// for a bit before executing those actions (as opposed to interrupting said animation).
public class ButtonForTile : UIBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerUpHandler, IPointerDownHandler
{


    [Serializable]
    public class ButtonClickedEvent : UnityEvent { }

    [SerializeField]
    private ButtonClickedEvent m_OnClick = new ButtonClickedEvent();

    public WorldManager _WorldManager;
    private Animator m_animator;
    public UnitCommandTile _UnitCommandTile;

    private bool m_pointerInside = false;
    private bool m_pointerPressed = false;


    override protected void Start()
    {
        base.Start();
        m_animator = GetComponent<Animator>();
        _WorldManager = this.transform.GetComponentInParent<WorldManager>();
    }

    public ButtonClickedEvent onClick
    {
        get { return m_OnClick; }
        set { m_OnClick = value; }
    }

    public virtual void OnPointerEnter(PointerEventData eventData)
    {
        if (eventData.button != PointerEventData.InputButton.Left)
            return;

        m_pointerInside = true;

        if (_WorldManager._TouchInputManager._IsOnTileTouch && !m_pointerPressed)
            m_OnClick.Invoke();

        if (m_pointerPressed)
            Press();
    }

    public virtual void OnPointerExit(PointerEventData eventData)
    {
        if (eventData.button != PointerEventData.InputButton.Left)
            return;

        m_pointerInside = false;
      
        Unpress();
    }

    public virtual void OnPointerUp(PointerEventData eventData)
    {
        if (eventData.button != PointerEventData.InputButton.Left)
            return;

        if (_UnitCommandTile._CurrUnitTile._IsBoughtLand && !_UnitCommandTile._CurrUnitTile._IsDiscovered)  //샀는데, 미발견한 땅일 경우 
        {
            _UnitCommandTile._CurrUnitTile._IsDiscovered = true;
            _WorldManager.DrawFogTile(false);//타일 포그 상태를 다시 그려준다.

        }
        else if (!_UnitCommandTile._CurrUnitTile._IsBoughtLand)  //아직 사지 않은 땅일 경우
        {
            if(_UnitCommandTile._CurrUnitTile._TilePriceType == 1000)
            {
                if(_WorldManager._PlayerWood >= _UnitCommandTile._CurrUnitTile._TilePrice)
                {
                    _WorldManager._PlayerWood -= _UnitCommandTile._CurrUnitTile._TilePrice;
                    _UnitCommandTile._CurrUnitTile._IsBoughtLand = true;
                    _UnitCommandTile._CurrUnitTile._IsDiscovered = true;
                    _WorldManager._PlayerTileList.Add(_UnitCommandTile._CurrUnitTile);
                    _WorldManager.DrawFogTile(false);//타일 포그 상태를 다시 그려준다.

                }
            }
            else if (_UnitCommandTile._CurrUnitTile._TilePriceType == 2000)
            {
                if (_WorldManager._PlayerGold >= _UnitCommandTile._CurrUnitTile._TilePrice)
                {
                    _WorldManager._PlayerGold -= _UnitCommandTile._CurrUnitTile._TilePrice;
                    _UnitCommandTile._CurrUnitTile._IsBoughtLand = true;
                    _UnitCommandTile._CurrUnitTile._IsDiscovered = true;
                    _WorldManager._PlayerTileList.Add(_UnitCommandTile._CurrUnitTile);
                    _WorldManager.DrawFogTile(false);//타일 포그 상태를 다시 그려준다.

                }

            }
            else if (_UnitCommandTile._CurrUnitTile._TilePriceType == 3000)
            {
                if (_WorldManager._PlayerFood >= _UnitCommandTile._CurrUnitTile._TilePrice)
                {
                    _WorldManager._PlayerFood -= _UnitCommandTile._CurrUnitTile._TilePrice;
                    _UnitCommandTile._CurrUnitTile._IsBoughtLand = true;
                    _UnitCommandTile._CurrUnitTile._IsDiscovered = true;
                    _WorldManager._PlayerTileList.Add(_UnitCommandTile._CurrUnitTile);
                    _WorldManager.DrawFogTile(false);//타일 포그 상태를 다시 그려준다.

                }
            }

        }

        m_pointerPressed = false;
        _WorldManager._TouchInputManager._IsOnTileTouch = false;
        _WorldManager.CheckReleasingTiles(); //모든 타일들의 isSelected를 해방시켜준다.
               
        Unpress();
    }

    public virtual void OnPointerDown(PointerEventData eventData)
    {
        if (eventData.button != PointerEventData.InputButton.Left)
            return;

        _WorldManager._TouchInputManager._IsOnTileTouch = true;

        if (_WorldManager._TouchInputManager._IsOnTileTouch)
            m_OnClick.Invoke();


        m_pointerPressed = true;
        Press();


    }

    private void Press()
    {
        if (!IsActive())
            return;

    }

    private void Unpress()
    {
        if (!IsActive())
            return;

      
    }

    public void OnPressedAnimationFinished()
    {
        m_OnClick.Invoke();
    }

    public void ResetToNormalState()
    {
        m_animator.Play("Normal");
    }
}
